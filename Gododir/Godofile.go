package main

import (
	"fmt"
	"log"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	do "gopkg.in/godo.v2"
)

func tasks(p *do.Project) {
	do.Env = ``

	p.Task("default", do.S{"run"}, nil)

	// Task to setup everything.
	p.Task("install", nil, func(c *do.Context) {
		for _, cmd := range []string{
			// Create some of the needed directories.
			"mkdir -p static/js/",

			// Install MySQL.
			"sudo apt-get update",
			"sudo apt-get install -y mysql-server",

			// Install protoc.
			"wget https://github.com/google/protobuf/releases/download/v3.0.0-beta-2/protoc-3.0.0-beta-2-linux-x86_64.zip",
			"rm -rf google protoc-temp bin/protoc",
			"unzip -o -d protoc-temp protoc-3.0.0-beta-2-linux-x86_64.zip",
			"mv protoc-temp/protoc bin/",
			"mv protoc-temp/google .",
			"rm -rf protoc-temp",
			"go get -u github.com/golang/protobuf/{proto,protoc-gen-go}",

			// Install Node.js.
			"npm --version || curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -",
			"sudo apt-get install -y nodejs python-virtualenv",
			// TODO: Upgrade tsd to Typings.
			"sudo npm install -g protobufjs typescript proto2typescript tsd",

			// Build protos (needed to use go get).
			"godo protos",

			// Install Go dependencies.
			"go get -u sandengine/...",

			// Install Python dependencies.
			"sudo apt-get install -y libjpeg-dev",
			"cd tools && virtualenv . && source bin/activate && pip install -r requirements.txt",

			// Install TypeScript dependencies.
			"godo clientdeps",
		} {
			log.Printf("exec %s", cmd)
			c.Bash(cmd)
		}
	})

	// Task to get all the dependencies of the client app.
	p.Task("clientdeps", nil, func(c *do.Context) {
		c.Bash("source env.sh && " +
			"mkdir -p client/sandengine/lib && " +
			"cd client/sandengine/lib && " +
			"tsd install jquery es6-promise sockjs pubsub protobufjs " +
			"underscore backbone mocha chai --save")
	})

	// Task to compile all the protos.
	p.Task("protos?", nil, func(c *do.Context) {
		c.Bash("PATH=$PATH:bin/ protoc " +
			"--go_out=src/ " +
			"--python_out=tools/ " +
			"src/sandengine/proto/*.proto " +
			"--proto_path=src/ ")

		c.Bash("PATH=$PATH:bin/ " +
			"pbjs src/sandengine/proto/*.proto " +
			"--path=src/ --min " +
			"--source=proto --target=js > " +
			"static/js/proto.js && " +
			"pbjs src/sandengine/proto/*.proto " +
			"--path=src/ " +
			"--source=proto --target=json > " +
			"static/js/proto.json && " +
			"proto2typescript --file static/js/proto.json " +
			"--properties=false > " +
			"client/sandengine/proto.d.ts")

		bytebuffer := `/// <reference path=\"lib/bytebuffer.d.ts\" />`

		c.Bash("echo \"" + bytebuffer + "\" | cat - client/sandengine/proto.d.ts " +
			"> tmp && mv tmp client/sandengine/proto.d.ts")
	}).Src("src/sandengine/proto/*.proto")

	// Task to compile the TypeScript code. This does not include clientdeps as
	// a dependency because it is slow.
	p.Task("client?", do.P{"protos"}, func(c *do.Context) {
		c.Bash("tsc " +
			"--out static/js/engine.js " +
			"client/sandengine/*.ts")
		c.Bash("tsc " +
			"--out static/js/test.js " +
			"client/sandengine/test/*.ts")
		c.Bash("tsc " +
			"--out static/js/sandbuilder.js " +
			"client/sandbuilder/*.ts")
	}).Src("client/sandengine/*.ts", "client/sandbuilder/*.ts")

	// Task to compile the Go server.
	p.Task("build?", do.S{"protos"}, func(c *do.Context) {
		c.Run("GOOS=linux GOARCH=amd64 go build -o bin/sandbuilder sandbuilder")
	}).Src("src/sandbuilder/**/*.go", "src/sandengine/**/*.go")

	// Task to run the main server.
	p.Task("run", do.S{"build", "client", "assets"}, func(c *do.Context) {
		c.Run("./bin/sandbuilder")
	}).Src("templates/*.tmpl.html")

	// Task to remove the databases.
	p.Task("cleardbs", nil, func(c *do.Context) {
		c.Bash("rm -f *.db")

		// Drop all the DBs in MySQL.
		db, err := gorm.Open("mysql", "sandbuilder:sandbuilder@/sandbuilder")
		if err != nil {
			log.Fatalf("Could not open database: %s", err)
		}

		db.Exec(`DROP TABLE entities;`)
		db.Exec(`DROP TABLE players;`)
	})

	// Task to create manual testing data.
	p.Task("testdata", do.S{"loadstatic", "build"}, func(c *do.Context) {
		c.Bash("./bin/sandbuilder test_data")
	})

	// Task to load static data into the system.
	p.Task("loadstatic", do.S{"cleardbs", "build"}, func(c *do.Context) {
		c.Bash("./bin/sandbuilder load_static")
	})

	// Task to compile the assets.json file.
	p.Task("assets", do.S{"protos"}, func(c *do.Context) {
		c.Bash("cd tools && " +
			"source bin/activate && " +
			"python compile_assets.py")
	})

	// Task to create all terrain generation, etc.
	p.Task("generate", do.S{"assets"}, func(c *do.Context) {
		c.Bash("mkdir -p data/regions/ data/tilesets")
		c.Bash("cd tools && " +
			"source bin/activate && " +
			"python generate_all.py")
	})

	// Task to generate new stuff, load it, and run the server.
	p.Task("genrun", do.S{"generate", "testdata", "run"}, func(c *do.Context) {
		// We don't actually have to do anything.
	})

	packages := []string{"", "/server", "/builtins", "/builtins/regions", "/db"}

	// Task to run all the tests.
	p.Task("test", nil, func(c *do.Context) {
		for _, p := range packages {
			c.Bash(fmt.Sprintf("go test sandengine%s", p))
		}
	})

	// Task to generate test coverage.
	p.Task("cover", do.S{"build", "client"}, func(c *do.Context) {
		for _, p := range packages {
			outfile := strings.Replace(p, "/", "-", -1)
			c.Bash(fmt.Sprintf("go test -coverprofile=coverage%s.out sandengine%s", outfile, p))
			c.Bash(fmt.Sprintf("go tool cover -html=coverage%s.out", outfile))
		}
	})

	// Task to push a built binary to production.
	p.Task("push", do.S{"build", "client"}, func(c *do.Context) {
		root := "rob@www.sandbuildergame.com:/home/rob/sandengine"
		c.Bash(fmt.Sprintf("scp bin/sandbuilder %s", root))
		c.Bash(fmt.Sprintf("scp -r static %s", root))
		c.Bash(fmt.Sprintf("scp -r templates %s", root))
		c.Bash(fmt.Sprintf("scp -r data %s", root))
	})
}

func main() {
	do.Godo(tasks)
}
