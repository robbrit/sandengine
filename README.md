# Installation

    cd /path/to/sandengine
    # Run the install script:
    ./install.sh

# Generating Assets

    source env.sh
    godo generate
    godo loadstatic

To create some pre-made players:

    source env.sh
    godo testdata

# Building

    source env.sh
    godo build

# Adding a new Entity

1. Add it to entity_factory.go.
2. Add it to entity_factory.ts.
3. Create an image for it.
4. Add it to assets.py.
5. If it's a static entity: godo generate, godo testdata.
6. If it has behaviour, add it in main.go
