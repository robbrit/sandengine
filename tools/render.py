import base64
import glob
from PIL import Image
from sandengine.proto import region_pb2, tileset_pb2

tilesets = {}

def render(region):
    global tilesets

    width = region.width
    height = region.height

    ts = tilesets[region.tiles.tileset]
    im = Image.new("RGBA", (width * ts.tile_width, height * ts.tile_height))

    ts_im = Image.open("../static/%s.png" % ts.image_resource)

    x, y = 0, 0
    for tile in r.tiles.tiles:
        tx, ty = (ts.tile_width * tile.subtype_num,
                ts.tile_height * 2 * tile.group_num)
        tile_im = ts_im.crop((tx, ty,
            tx + ts.tile_width, ty + ts.tile_height))

        im.paste(tile_im, (x * ts.tile_width, y * ts.tile_height))

        # Render the borders
        i = 1
        neighbourTypes = {}

        for dx, dy in [[-1, 0], [0, -1], [1, 0], [0, 1]]:
            tx = x + dx
            ty = y + dy
            if tx < 0 or ty < 0 or tx >= width or ty >= height:
                continue

            group = r.tiles.tiles[ty * width + tx].group_num

            neighbourTypes[group] = neighbourTypes.get(group, 0)
            neighbourTypes[group] |= i
            i <<= 1

        for group, offset in neighbourTypes.items():
            if tile.group_num >= group:
                continue
            # render the edge on me
            tile_im = ts_im.crop((
                    ts.tile_width * (offset - 1),
                    ts.tile_height * (2 * group - 1),
                    ts.tile_width * offset,
                    ts.tile_height * (2 * group)))

            im.paste(tile_im, (x * ts.tile_width, y * ts.tile_height), tile_im)

        x += 1
        if x == width:
            x = 0
            y += 1

    im.save("../data/regions/%s.png" % region.name)

for path in glob.glob("../data/tilesets/*.tileset"):
    with open(path, "r") as f:
        t = tileset_pb2.Tileset()
        t.ParseFromString(base64.b64decode(f.read()))
        tilesets[t.name] = t

for path in glob.glob("../data/regions/*.region"):
    with open(path, "r") as f:
        r = region_pb2.Region()
        r.ParseFromString(base64.b64decode(f.read()))
        render(r)
