import base64
from generate import tilesets, outside, assets
from sandengine.proto import region_pb2

forest = tilesets.Tileset("forest", tilesets.ForestGroups)

with open("../data/tilesets/forest.tileset", "w") as f:
    f.write(base64.b64encode(forest.to_proto().SerializeToString()))

r1 = outside.Outside("1", forest, 200, 200, 0, 0)
r2 = outside.Outside("2", forest, 200, 200, 0, 1)

r1.set_neighbour("right", r2)
r2.set_neighbour("left", r1)
r1.smooth()
r2.smooth()

with open("../data/regions/1.region", "w") as f:
    f.write(base64.b64encode(r1.to_proto().SerializeToString()))
with open("../data/regions/2.region", "w") as f:
    f.write(base64.b64encode(r2.to_proto().SerializeToString()))

with open("../data/regions/static", "w") as f:
    se = region_pb2.StaticEntities()
    for r in [r1, r2]:
        se.regions.extend([r.static_entities.to_proto()])
    f.write(base64.b64encode(se.SerializeToString()))
