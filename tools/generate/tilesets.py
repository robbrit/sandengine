import random
from sandengine.proto import tileset_pb2

ForestGroups = [
    {
        "name": "dirt",
        "entity_chances": {
            "boulder": 1.0,
            "dead_tree": 0.5,
        },
        "subtypes": [{}, {}],
    },
    {
        "name": "grass",
        "entity_chances": {
            "boulder": 0.1,
            "bush": 0.5,
            "tree": 3.0,
            "pine_tree": 0.2,
            "chicken": 0.15,
            "cotton_tree": 0.3,
            #"wolf": 0.05,
            "fern": 0.005,
        },
        "subtypes": [{}, {}, {}],
    },
    {
        "name": "trees",
        "entity_chances": {
            "boulder": 0.1,
            "tree": 2.0,
            "pine_tree": 3.0,
            #"wolf": 0.08,
            "fern": 1.0,
        },
    },
    {
        "name": "water",
        "entity_chances": {
            "lilypad": 2,
        },
        "subtypes": [
            {"passable": False},
            {"passable": False},
        ],
    }
]

class Tileset(object):
    def __init__(self, name, groups):
        self.name = name
        self.groups = groups

    def to_proto(self):
        ts = tileset_pb2.Tileset()
        ts.name = self.name
        ts.image_resource = "tilesets/%s" % self.name
        ts.tile_width = 40
        ts.tile_height = 40

        for group in self.groups:
            g = ts.groups.add()
            g.name = group["name"]

            for i, subtype in enumerate(group.get("subtypes", [{}])):
                st = g.subtypes.add()
                st.offset = i
                st.passable = subtype.get("passable", True)

            for ent, pct in group.get("entity_chances", []).items():
                ec = g.entity_chances.add()
                ec.entity_type = ent
                ec.probability = pct / 100.0

        return ts

    def num_groups(self):
        """Gets the number of groups in this tileset."""
        return len(self.groups)

    def num_subtypes(self, group):
        """Gets the number of subtypes for a group."""
        return len(self.groups[group].get("subtypes", [{}]))

    def random_subtype(self, group):
        """Gets a random subtype for a group."""
        return random.randint(0, max(0, self.num_subtypes(group) - 1)) 

    def is_passable(self, tile):
        """Checks if a tile type is passable."""
        group, subtype = tile
        subtype = self.groups[group].get("subtypes", [{}])[subtype]
        return subtype.get("passable", True)

    def random_passable(self, passable=True):
        """Gets a random passable tile group and subtype."""
        passables = set()
        for group, data in self.groups:
            for i, subgroup in enumerate(group.get("subtypes", [])):
                if subgroup.get("passable", True) == passable:
                    passables.add(group)

        group = random.choice(passables)
        return (group, self.random_subtype(group))
