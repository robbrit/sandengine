import base64
import random
from generate.static_entities import StaticEntities
from sandengine.proto import region_pb2


class Region(object):
    def __init__(self, name, tileset, width, height):
        self.name = name
        self.width = width
        self.height = height
        self.tileset = tileset
        self.tiles = []

        self.neighbours = {}

        self.generate_tiles()
        self.generate_static_entities()

    def set_neighbour(self, which, region):
        self.neighbours[which] = region

    def generate_tiles(self):
        raise NotImplementedError()

    def to_proto(self):
        r = region_pb2.Region()
        r.name = self.name
        r.width = self.width
        r.height = self.height

        r.tiles.tileset = self.tileset.name
        for tile in self.tiles:
            t = r.tiles.tiles.add()
            t.group_num = tile[0]
            t.subtype_num = tile[1]

        if self.neighbours.get("left"):
            r.neighbours.left = self.neighbours["left"].name
        if self.neighbours.get("up"):
            r.neighbours.up = self.neighbours["up"].name
        if self.neighbours.get("right"):
            r.neighbours.right = self.neighbours["right"].name
        if self.neighbours.get("down"):
            r.neighbours.down = self.neighbours["down"].name

        return r

    def group_for(self, group):
        """Get the group info for a group index."""
        return self.tileset.groups[group]

    def smooth(self):
        """Smooth our west and north borders with our neighbours."""
        west = self.neighbours.get("left")
        if west:
            for y in range(self.height):
                my_tile = self.tile_at(0, y)
                their_tile = west.tile_at(west.width - 1, y)

                self.smooth_tile(0, y, my_tile, their_tile, west)

        north = self.neighbours.get("up")

        if north:
            for x in range(self.width):
                my_tile = self.tile_at(x, 0)
                their_tile = north.tile_at(x, north.height - 1)

                self.smooth_tile(x, 0, my_tile, their_tile, north)

    def smooth_tile(self, x, y, my_tile, their_tile, them):
        """Merge my tile with their tile."""
        if self.is_passable(my_tile) and not them.is_passable(their_tile):
            # Their tile is not passable, but mine is. Set mine to something
            # that isn't passable.
            if them.tileset.name != self.tileset.name:
                # I can't use theirs, so just generate a random one.
                self.set_tile_at(x, y, self.tileset.random_passable(False))
            else:
                # Just clone the tile from the other side.
                self.set_tile_at(x, y, their_tile)
        elif not self.is_passable(my_tile) and them.is_passable(their_tile):
            # Ours is not passable, but theirs is. Set mine to something that
            # is passable.
            if them.tileset.name != self.tileset.name:
                # I can't use theirs, so just generate a random one.
                self.set_tile_at(x, y, self.tileset.random_passable())
            else:
                # Just clone the tile from the other side.
                self.set_tile_at(x, y, their_tile)

    def tile_at(self, x, y):
        """Get the value at x, y."""
        return self.tiles[x + y * self.width]

    def set_tile_at(self, x, y, tile, tiles=None):
        """Set the tile at x, y to value."""
        if tiles is None:
            tiles = self.tiles

        tiles[x + y * self.width] = tile

    def generate_static_entities(self):
        """Get a list of static entities for this region."""
        entities = []

        x, y = 0, 0

        for tile in self.tiles:
            group = self.group_for(tile[0])

            # choose using roulette wheel
            selection = random.random()

            ents = group.get("entity_chances", [])

            for entity_type, probability in ents.items():
                probability /= 100.0
                if selection < probability:
                    print "Adding a %s" % entity_type
                    entities.append({
                        "entity_type": entity_type,
                        "location": (x, y),
                        "properties": self.props_for_entity(entity_type),
                        "privates": self.privs_for_entity(entity_type)
                    })
                    break

                selection -= probability

            x += 1

            if x == self.width:
                x = 0
                y += 1

        self.static_entities = StaticEntities(self, entities)

    def props_for_entity(self, entity_type):
        return {}

    def privs_for_entity(self, entity_type):
        return {}

    def is_passable(self, tile):
        return self.tileset.is_passable(tile)
