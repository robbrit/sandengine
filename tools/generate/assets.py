import base64
import json
from sandengine.proto import sprite_pb2

def to_sprite_string(proto):
    return "<proto(Sprite):%s>" % base64.b64encode(proto.SerializeToString())

Sprites = [
    # Boulder
    {
        "name": "boulder",
        "width": 50,
        "x_offset": -5,
    },
    # Bush
    {"name": "bush"},
    # Chicken
    {
        "name": "chicken"
    },
    # Cotton Tree
    {
        "name": "cotton_tree",
        "width": 50,
        "height": 60,
        "animations": 2,
        "x_offset": -5,
        "y_offset": -20,
    },
    # Dead Tree
    {
        "name": "dead_tree",
        "width": 50,
        "height": 60,
        "x_offset": -5,
        "y_offset": -20,
    },
    # Fern
    {
        "name": "fern",
        "width": 50,
        "x_offset": -5,
    },
    # Lilypad
    {"name": "lilypad"},
    # Loot
    {"name": "loot"},
    # Pine Tree
    {
        "name": "pine_tree",
        "animations": 3,
        "height": 120,
        "y_offset": -80,
    },
    # Player
    {"name": "player"},
    # Tree
    {
        "name": "tree",
        "width": 50,
        "height": 70,
        "animations": 2,
        "x_offset": -5,
        "y_offset": -30,
    },
]

def Run():
    entities = {}

    for sprite in Sprites:
        p = sprite_pb2.Sprite()
        p.image_url = "/sprites/%s.png" % sprite["name"]
        p.width = sprite.get("width", 40)
        p.height = sprite.get("height", 40)
        p.x_offset = sprite.get("x_offset", 0)
        p.y_offset = sprite.get("y_offset", 0)

        numAnimations = sprite.get("animations", 1)
        for i in range(numAnimations):
            anim = p.animations.add()
            anim.axis = sprite_pb2.DIR4
            anim.frames = 1
            anim.offset = i * p.width

        entities[sprite["name"]] = to_sprite_string(p)

    with open("../static/assets.json", "w") as f:
        f.write(json.dumps({
            "tilesets": {
                "cave": "/tilesets/cave.png",
                "forest": "/tilesets/forest.png",
            },
            "sounds": {
                "boulderfall": "/sounds/boulderfall.ogg",
                "chicken": "/sounds/chicken.ogg",
                "metalchop": "/sounds/metalchop.ogg",
                "swing": "/sounds/swing.ogg",
                "treefall": "/sounds/treefall.ogg",
                "woodchop": "/sounds/woodchop.ogg",
            },
            "entities": entities
        }, indent=2))

if __name__ == "__main__":
    Run()
