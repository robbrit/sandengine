import random
import time
from math import log
from noise import snoise2

from .regions import Region


class Outside(Region):
    def __init__(self, name, tileset, width, height, offset_x, offset_y):
        self.offset_x = offset_x
        self.offset_y = offset_y
        self.freq = 64.0
        self.octaves = 4
        Region.__init__(self, name, tileset, width, height)

    def noise(self, offset_x, offset_y, x, y):
        """ Generate the tile type for a point.

        This algorithm is a little bit complicated, but it gives us some nice
        noisy terrain without having it look chaotic. It also avoids a
        contouring problem that we had with an earlier noise algorithm which
        would result in a contoured effect based on the group number. This
        new algorithm means that any terrain type can be next to any other
        terrain type.
        """

        # The tile type is numeric, ranging on [0, self.tileset["num_groups"])
        # So we are trying to generate a binary number that represents the
        # type that we want.
        num_bits = log(self.tileset.num_groups()) / log(2.0)
        current_bit = 0
        value = 0

        # For each bit, if the Perlin noise at that spot is positive, then the
        # bit is a one. If it is negative, it is a zero.
        while current_bit < num_bits:
            if snoise2(
                (offset_x + x + current_bit * self.width * 2) / self.freq,
                (offset_y + y + current_bit * self.height * 2) / self.freq,
                self.octaves
            ) >= 0.0:
                value |= (1 << current_bit)

            current_bit += 1

        # If num_groups is not a power of two, we may end up outside the range
        # so mod things.
        return value % self.tileset.num_groups()

    def generate_tiles(self):
        """Generate the tiles for this region using simplex noise."""
        groups = [
            self.noise(self.offset_x, self.offset_y, x, y)
            for x in range(self.width) for y in range(self.height)
        ]

        # Convert the noise to group names
        for group in groups:
            subtype = self.tileset.random_subtype(group)
            self.tiles.append((group, subtype))

    def props_for_entity(self, entity_type):
        if entity_type in ["tree", "cotton_tree"]:
            # Give it an age.
            return {
                "birthtime": int(time.time()) - random.randint(
                    # Between 5 and 40 days old.
                    5 * 86400,
                    40 * 86400
                )
            }
        elif entity_type in ["pine_tree"]:
            # Give it an age.
            return {
                "birthtime": int(time.time()) - random.randint(
                    # Between 5 and 100 days old.
                    5 * 86400,
                    100 * 86400
                )
            }

        return Region.props_for_entity(self, entity_type)

    def privs_for_entity(self, entity_type):
        if entity_type in ["chicken"]:
            return {
                "ai": True
            }

        return Region.privs_for_entity(self, entity_type)
