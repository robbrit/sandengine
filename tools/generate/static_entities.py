import json
from sandengine.proto import region_pb2

class StaticEntities(object):
    def __init__(self, region, entities):
        self.region = region
        self.entities = entities

    def to_proto(self):
        ents = region_pb2.StaticEntities.ForRegion()
        ents.region = self.region.name

        for ent in self.entities:
            se = ents.static_entities.add()
            se.id = ""
            se.entity_type = ent["entity_type"]
            se.location.x = ent["location"][0]
            se.location.y = ent["location"][1]
            se.location.region = self.region.name
            se.properties = json.dumps(ent["properties"])
            se.privates = json.dumps(ent["privates"])

        return ents
