#!/bin/bash

mkdir -p bin
source env.sh
# Install Godo and the imported libs from Sandengine's Godo script.
go get -u gopkg.in/godo.v2/cmd/godo
go get -u github.com/go-sql-driver/mysql
go get -u github.com/jinzhu/gorm
godo install
