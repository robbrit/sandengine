/// <reference path="lib/typings/es6-promise/es6-promise.d.ts" />
/// <reference path="lib/typings/pubsubjs/pubsub.d.ts" />

/// <reference path="player.ts" />
/// <reference path="proto.d.ts" />
/// <reference path="region.ts" />
/// <reference path="resources.ts" />
/// <reference path="socket.ts" />
/// <reference path="sound.ts" />
/// <reference path="ui/ui.ts" />
/// <reference path="util.ts" />

enum GameState {
  // The game hasn't been initialized or anything yet.
  init,
  // The game is showing lobby UI (login, signup, etc.)
  lobby,
  // The game is showing the player select UI.
  selectPlayer,
  // The game is loading.
  loading,
  // The game is actually running.
  running
};

var AuthKey = "authKey";
var PlayerProto = "playerProto";
var MaxSoundDist = 30;

class Game {
  ui: UI;
  authKey: string;
  state: GameState;
  debugMode: boolean;
  socket: Socket;
  resources: ResourceManager;
  player: Player;
  lastCycle: number;
  currentRegion: Region;
  effectHandlers: any;
  entityFactory: EntityFactory;

  constructor() {
    PubSub.clearAllSubscriptions();

    this.ui = new UI(this);
    this.state = GameState.init;

    this.effectHandlers = {
      "EntityList": this.handleEntityList,
      "UpdateEntity": this.handleUpdateEntity,
      "UpdatePrivate": this.handleUpdatePrivate,
      "RemoveEntity": this.handleRemoveEntity,
      "ChangeRegion": this.handleChangeRegion,
      "PlaySound": this.handlePlaySound
    };

    // Bind some PubSub events.
    PubSub.subscribe("auth.success", (name, authKey: string) => {
      this.saveAuthKey(authKey);
      this.state = GameState.selectPlayer;
      this.ui.showSelectPlayer();
    });
    PubSub.subscribe("auth.failure", () => {
      this.stop();
    });
    PubSub.subscribe("auth.selectPlayer", (name, player: sandengine.Player) => {
      this.savePlayerProto(player);
      this.loadPlayer();
      this.start();
    });
    PubSub.subscribe("keyboard.move", (name, d: sandengine.Direction) => {
      this.attemptMove(d);
    });
    PubSub.subscribe("keyboard.interact", (name) => {
      this.attemptInteract();
    });
    PubSub.subscribe("effects", (name, effects: sandengine.Effects) => {
      this.processEffects(effects);
    });
    PubSub.subscribe("keyboard.inventory", () => {
      this.ui.toggleInventory();
    });
  }

  // Gets the player.
  getPlayer(): Player { return this.player; }
  // Gets the resource manager.
  getResourceManager(): ResourceManager { return this.resources; }
  // Gets the UI.
  getUI(): UI { return this.ui; }

  // Sets the entity factory.
  setEntityFactory(factory: EntityFactory) { this.entityFactory = factory; }

  // run causes the game to actually start running.
  run() {
    // First, figure out what state we're in.
    Proto.load().then(() => {
      return this.ui.load();
    }).then(() => {
      return this.loadAuth();
    }).then((good: boolean) => {
      if (good) {
        // We have a valid auth key.
        if (this.loadPlayer()) {
          // We have a valid auth key and have selected a player, start the game.
          this.start();
        } else {
          // We haven't chosen a player, so show the UI to choose one.
          this.state = GameState.selectPlayer;
          this.ui.showSelectPlayer();
        }
      } else {
        // We don't have a valid auth key, so show the login form.
        this.state = GameState.lobby;
        this.ui.showLoginForm();
      }
    }).catch((err) => {
      console.log("Error:", err);
    });
  }

  // stop stops the game and sends us back to the lobby.
  stop() {
    this.resetSession();
    this.state = GameState.lobby;
    this.ui.showLoginForm();
  }

  // saveAuthKey saves a new auth key we got from the server.
  saveAuthKey(authKey: string) {
    this.authKey = authKey;
    localStorage.setItem(AuthKey, authKey);
    this.ui.authKey = authKey;
  }

  // loadAuth loads our session key from localStorage and verifies whether
  // it is valid or not.
  loadAuth() : Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      if (!window.localStorage[AuthKey]) {
        // We haven"t logged in yet, so the auth is not valid.
        resolve(false);
        return;
      }

      console.log("Validating auth...");

      this.saveAuthKey(window.localStorage[AuthKey]);

      var req: sandengine.SimpleRequest = Proto.create("SimpleRequest");
      req.authKey = this.authKey;

      // Validate the auth key.
      Proto.get("ValidAuthResponse", "/valid_auth", req).then((resp: sandengine.ValidAuthResponse) => {
        if (!resp.ok) {
          console.log("fail");
          this.resetSession();
        } else {
          console.log("done");
        }

        resolve(resp.ok);
      });
    });
  }

  // resetSession clears any session info we may have stored locally.
  resetSession() {
    window.localStorage.removeItem(AuthKey);
    window.localStorage.removeItem(PlayerProto);
    this.authKey = null;
    this.player = null;
  }

  savePlayerProto(player: sandengine.Player) {
    localStorage[PlayerProto] = player.toBase64();
  }

  // loadPlayer loads the player from localStorage.
  loadPlayer(): boolean {
    if (localStorage[PlayerProto]) {
      this.player = new Player(Proto.parse("Player", localStorage[PlayerProto]));
      return true;
    }
    return false;
  }

  // start actually runs the game.
  start() {
    this.ui.showCanvas();

    this.state = GameState.loading;
    this.ui.drawLoading();

    // Do all sorts of initialization stuff
    console.log("Connecting to websocket server...");
    Socket.connect(this.authKey, this.player.getName()).then(() => {
      console.log("Connected, loading resources...");
      this.resources = new ResourceManager(this.authKey);
      return this.resources.load();
    }).then(() => {
      console.log("Resources loaded, joining region...");
      this.loadRegion();
    }).catch((err) => {
      console.log("Error in loading:", err);
      this.stop();
    });
  }

  loadRegion() {
    if (this.state != GameState.loading) {
      this.state = GameState.loading;
      this.ui.drawLoading();
    }

    // First, send the JoinGameRequest which will have us join the region.
    new Promise<sandengine.JoinGameResponse>((resolve, reject) => {
      var handler = (unused, response: sandengine.JoinGameResponse) => {
        // TODO: handle failure.
        PubSub.unsubscribe(handler);
        resolve(response);
      };
      // Send a join game request to the server, which will then send back
      // the region proto we're in, and an effects proto that says what happened.
      var join = <sandengine.JoinGameRequest>Proto.create("JoinGameRequest");
      join.setPlayerName(this.player.getName());
      PubSub.subscribe("join.success", handler);
      console.log("Attempting to join region");
      Socket.send(join);
    }).then((resp: sandengine.JoinGameResponse) => {
      console.log("Region joined, starting...");
      this.currentRegion = new Region(resp.region, new Tileset(resp.tileset));
      // The effects here will contain some information about the region that
      // needs to be processed before we attempt to run the game.
      this.processEffects(resp.effects);
      this.ui.hud.redraw();
      this.startGameLoop();
    }, (resp) => {
      console.log("error:", resp);
    });
  }

  // Processes all the effects that are passed.
  processEffects(effects: sandengine.Effects) {
    _.each(effects.effects, (rawEffect: sandengine.Effect) => {
      if (this.effectHandlers[rawEffect.getName()]) {
        this.effectHandlers[rawEffect.getName()].call(this,
          Proto.parseRaw(rawEffect.getName(), rawEffect.getData()));
      } else {
        console.log("Unknown effect type " + rawEffect.getName());
      }
    });
  }

  // Handles an EntityList effect.
  handleEntityList(effect: sandengine.EntityList) {
    this.currentRegion.setEntities(_.map(effect.getEntities(), (ent) => {
      return this.entityFactory.create(ent);
    }));

    if (this.state != GameState.running) {
      this.startGameLoop();
    }
  }

  // Handles an UpdateEntity effect.
  handleUpdateEntity(effect: sandengine.UpdateEntity) {
    var proto = effect.getEntity();
    var ent = this.currentRegion.getEntity(proto.getId());
    if (ent) {
      ent.update(proto);
    } else {
      this.currentRegion.addEntity(this.entityFactory.create(proto));
    }
  }

  // Handles an UpdatePrivate effect.
  handleUpdatePrivate(effect: sandengine.UpdatePrivate) {
    var ent = this.currentRegion.getEntity(effect.getEntityId());
    if (ent) {
      ent.updatePrivates(JSON.parse(effect.getPrivates()));
      this.ui.hud.redraw();
    }
  }

  // Handles a RemoveEntity effect.
  handleRemoveEntity(effect: sandengine.RemoveEntity) {
    this.currentRegion.removeEntity(effect.getEntityId());
  }

  // Handles a ChangeRegion effect.
  handleChangeRegion(effect: sandengine.ChangeRegion) {
    // This is us, gotta load the new region.
    this.currentRegion = null;
    this.loadRegion();
  }

  // Handles a PlaySound effect.
  handlePlaySound(effect: sandengine.PlaySound) {
    var sound = this.resources.get<Sound>("sounds/" + effect.sound);
    var player = this.playerEntity();

    if (sound && player) {
      var dx = effect.location.x - player.getLocation().x;
      var dy = effect.location.y - player.getLocation().y;

      var dist = Math.sqrt(dx * dx + dy * dy);
      var vol = Math.max(0, MaxSoundDist - dist);

      if (vol == 0) {
        return;
      }

      sound.play(vol / MaxSoundDist);
    }
  }

  startGameLoop() {
    this.ui.doneLoading();
    this.state = GameState.running;
    this.lastCycle = new Date().getTime();
    window.requestAnimationFrame(() => { this.cycle() });
  }

  cycle() {
    if (this.state != GameState.running) {
      // This happens if we switch out of the running state in between cycles.
      return;
    }

    var now = new Date().getTime(),
      dt = (now - this.lastCycle) / 1000;

    this.lastCycle = now;

    this.ui.cycle(now, dt);

    var playerEntity = this.playerEntity();
    if (!playerEntity) {
      console.log("no player entity found, resetting");
      this.stop();
      return;
    }
    this.ui.draw(this.currentRegion, this.resources, playerEntity, dt);
    window.requestAnimationFrame(() => { this.cycle() });
  }

  // attempts to move the player in the specified direction.
  attemptMove(d: sandengine.Direction) {
    if (this.state != GameState.running) {
      return;
    }

    var move = <sandengine.MoveCmd>Proto.create("MoveCmd");
    move.setDirection(d);
    Socket.send(move);
  }

  // attempts to interact with whatever is in front of the user.
  attemptInteract() {
    if (this.state != GameState.running) {
      return;
    }

    Socket.send(<sandengine.InteractCmd>Proto.create("InteractCmd"));
  }

  // fetches the player's entity.
  playerEntity(): Entity {
    if (!this.currentRegion) {
      return null;
    }
    return this.currentRegion.getEntity(this.player.getEntityId());
  }
}
