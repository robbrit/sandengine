/// <reference path="lib/typings/es6-promise/es6-promise.d.ts" />
/// <reference path="lib/typings/pubsubjs/pubsub.d.ts" />

/// <reference path="proto.d.ts" />
/// <reference path="util.ts" />

// Wraps a number of Websocket-related communications.
// Events fired:
// - auth.failure - sent when an identify request fails.
class Socket {
  static InitialReconnectDelay = 500;
  static ReconnectMultiplier = 2;

  static authKey: string;
  static reconnectTimer: number;
  static socket: WebSocket;
  static shouldReconnect: boolean;
  static delay: number = Socket.InitialReconnectDelay;

  static serverName() : string {
    var protocol = window.location.protocol;

    return (protocol == "http:" ? "ws" : "wss") +
      "://" + window.location.hostname +
      (window.location.port == "" ? "" : ":" + window.location.port) +
      "/ws";
  }

  static connect(authKey: string, playerName: string): Promise<void> {
    return new Promise<void>(function(resolve, reject) {
      if (authKey == null) {
        reject("no auth key");
        return;
      }

      if (Socket.socket != null) {
        // We're already connected.
        resolve();
      }

      var resolved = false;
      var authenticated = false;

      Socket.authKey = authKey;
      Socket.socket = new WebSocket(Socket.serverName());
      Socket.shouldReconnect = false;

      Socket.socket.onopen = function() {
        console.log("Websocket connected.");
        Socket.delay = Socket.InitialReconnectDelay;

        // Identify this socket
        Socket.send(Proto.create("IdentifyRequest"));
      };

      Socket.socket.onmessage = function(raw) {
        var msg: sandengine.ServerMessage =
          Proto.parse("ServerMessage", raw.data);

        if (msg.errors.length > 0) {
          // We had some sort of error.
          if (msg.identify !== undefined) {
            PubSub.publish("auth.failure", null);
            Socket.shouldReconnect = false;
            Socket.socket.close();
          }
          console.log("Error on websocket message: " + msg.errors.join(", "));
        } else if (msg.identify) {
          // This is the identification response.
          authenticated = true;
          Socket.shouldReconnect = true;
          if (!resolved) {
            resolved = true;
            resolve();
          }
        } else if (authenticated) {
          // We're authenticated already, so send out this message.
          Socket.fanout(msg);
        } else {
          console.log("Message received while unauthenticated:");
          console.log(msg);
        }
      };

      Socket.socket.onclose = function() {
        Socket.socket = null;
        authenticated = false;

        if (!resolved) {
          // This will happen if we can't connect for some reason
          resolved = true;
          reject("unable to connect");
        }
        console.log("Websocket closed.");

        // attempt reconnect
        Socket.attemptReconnect(authKey, playerName);
      };
    });
  }

  // Send this message to the appropriate queue.
  static fanout(msg: sandengine.ServerMessage) {
    // TODO: implement this for each type of client message other than the
    // IdentifyResponse.
    if (msg.effects) {
      // We have some effects, send them all out.
      PubSub.publish("effects", msg.effects);
    } else if (msg.join) {
      PubSub.publish("join.success", msg.join);
    }
  }

  // Attempt to reconnect to the server.
  static attemptReconnect(authKey: string, playerName: string) {
    if (Socket.reconnectTimer != 0) {
      window.clearTimeout(Socket.reconnectTimer);
    }

    Socket.reconnectTimer = window.setTimeout(function() {
      Socket.delay *= Socket.ReconnectMultiplier;
      Socket.connect(authKey, playerName);
    }, Socket.delay);
  }

  // Send a proto to the server.
  static send(msg: sandengine.ProtoBufModel) {
    var type = msg.toString().replace(".sandengine.", "");

    var client: sandengine.ClientRequest = Proto.create("ClientRequest");
    client.authKey = Socket.authKey;

    switch (type) {
    case "IdentifyRequest":
      client.identify = <sandengine.IdentifyRequest>msg;
      break;
    case "JoinGameRequest":
      client.join = <sandengine.JoinGameRequest>msg;
      break;
    default:
      // Default: assume it is a command.
      var cmd = <sandengine.Command>Proto.create("Command");
      cmd.setName(type);
      cmd.setData((<any>msg).encode());
      client.command = cmd;
    }

    Socket.socket.send(client.toBase64());
  }
}
