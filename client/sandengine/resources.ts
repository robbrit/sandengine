/// <reference path="lib/typings/es6-promise/es6-promise.d.ts" />
/// <reference path="lib/typings/jquery/jquery.d.ts" />
/// <reference path="lib/typings/underscore/underscore.d.ts" />

/// <reference path="proto.d.ts" />
/// <reference path="sound.ts" />
/// <reference path="util.ts" />
/// <reference path="ui/sprite.ts" />

class ResourceManager {
  static protoRegex = /^<proto\((\w+)\):(.+)>$/;

  assetPrefix: string;
  resources: any;

  constructor(private authKey: string) {
    // TODO: Should probably be set by the config.
    this.assetPrefix = "/static";
    this.resources = {};
  }

  process(assets: any, target: any): Array<Promise<any>> {
    var promises: Array<Promise<any>> = [];

    _.each(assets, (value, key: string) => {
      if (_.isObject(value)) {
        target[key] = {};
        promises = promises.concat(this.process(value, target[key]));
      } else if (_.isString(value)) {
        // Check for various resource types and load them
        if (this.isImage(value)) {
          promises.push(this.loadImage(target, key, value));
        } else if (this.isAudio(value)) {
          promises.push(this.loadAudio(target, key, value));
        } else if (this.isProto(value)) {
          promises.push(this.loadProto(target, key, value));
        } else {
          target[key] = value;
        }
      } else {
        target[key] = value;
      }
    });

    return promises;
  }

  isImage(url: string): boolean {
    return url.match(/\.(png|jpg)$/) != null;
  }

  isAudio(url: string): boolean {
    return url.match(/\.ogg$/) != null;
  }

  isProto(value: string): boolean {
    return value.match(ResourceManager.protoRegex) != null;
  }

  loadImage(target: any, key: string, url: string): Promise<any> {
    return new Promise((resolve, reject) => {
      console.log("Loaded image " + url);
      var img = new Image();
      img.onload = () => { resolve(img); };
      img.onerror = () => { resolve(img); };
      img.src = this.assetPrefix + url;
      target[key] = img;
    });
  }

  loadAudio(target: any, key: string, url: string): Promise<any> {
    var sound = new Sound();
    return sound.load(this.assetPrefix + url).then(() => {
      console.log("Loaded sound " + url);
      target[key] = sound;
    });
  }

  loadProto(target: any, key: string, value: string): Promise<any> {
    var match = value.match(ResourceManager.protoRegex);
    if (!match) {
      return Promise.resolve("");
    }

    var protoType = match[1];
    var protoData = match[2];

    // There are some special cases for protos: some of them have data that
    // needs to be fetched.
    switch (protoType) {
    case "Sprite":
      var sprite = <sandengine.Sprite>Proto.parse(protoType, protoData);
      return this.loadImage({}, "", sprite.getImageUrl()).then((img: HTMLImageElement) => {
        var data = new SpriteData(sprite, img);
        target[key] = data;
      });
    default:
      var proto = Proto.parse(protoType, protoData);
      target[key] = proto;
      return Promise.resolve(proto);
    }
  }

  load(): Promise<boolean> {
    return new Promise(function(resolve, reject) {
      $.getJSON("/assets", resolve, reject);
    }).then((resourcePaths: any) => {
      return Promise.all(this.process(resourcePaths, this.resources));
    }).then(() => true);
  }

  get<T>(what: string): T {
    var current = this.resources;
    var nodes = what.split("/");

    while (nodes.length > 0) {
      current = current[nodes[0]];

      if (!current) {
        return null;
      }

      nodes.shift();
    }

    return <T>current;
  }
}
