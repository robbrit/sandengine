/// <reference path="../proto.d.ts" />
/// <reference path="../tileset.ts" />

// Convert a world location into a pair of screen coordinates.
function w2s(loc: sandengine.Location, ts: Tileset): number[] {
  return [
    loc.x * ts.getTileWidth(),
    loc.y * ts.getTileHeight()
  ];
}
