/// <reference path="../lib/typings/backbone/backbone.d.ts" />
/// <reference path="../lib/typings/pubsubjs/pubsub.d.ts" />

/// <reference path="../proto.d.ts" />
/// <reference path="../util.ts" />

// A view for selecting a single player.
//
// Events fired:
// auth.selectPlayer(sandengine.Player) - fired when this player has been selected.
class PlayerOption extends Backbone.View<Backbone.Model> {
  static template: (data: any) => string =
    _.template($("#player-option-view").text());

  player: sandengine.Player;

  constructor(player: sandengine.Player) {
    super();

    this.player = player;
  }

  render(): Backbone.View<Backbone.Model> {
    this.$el
      .addClass("player-row")
      .html(PlayerOption.template({
        player: this.player
      }));
    return this;
  }

  choose() {
    PubSub.publish("auth.selectPlayer", this.player);
  }

  events(): Backbone.EventsHash {
    return {
      "click": "choose"
    };
  }
}

// Creates a view for choosing whether to select a player, or create a new player.
//
// Events fired:
// auth.selectPlayer(sandengine.Player) - fired when a player has been created.
class PlayerSelect extends Backbone.View<Backbone.Model> {
  template: (data: any) => string;

  constructor(private authKey: string) {
    super()

    this.template = _.template($("#player-select").text());
  }

  render(): Backbone.View<Backbone.Model> {
    this.$el.html(this.template({}));

    this.$el.find(".new-player-form").submit((e) => {
      e.preventDefault();
      e.stopPropagation();
      this.createPlayer();
    });

    var req: sandengine.SimpleRequest = Proto.create("SimpleRequest");
    req.authKey = this.authKey;

    this.$el.find(".no-players").hide();
    Proto.get("PlayerListResponse", "/players", req).then((players: sandengine.PlayerListResponse) => {
      var list = this.$el.find(".player-list");
      list.empty();

      if (players.players.length == 0) {
        this.$el.find(".no-players").show();
      } else {
        _.chain(players.players)
          .map((player) => new PlayerOption(player).render())
          .each((view) => view.$el.appendTo(list));
      }
    });
    return this;
  }

  createPlayer() {
    var req: sandengine.CreatePlayerRequest = Proto.create("CreatePlayerRequest");
    req.authKey = this.authKey;
    req.name = this.$el.find(".name").val();

    this.$el.find("input").prop("disabled", true);

    Proto.post("CreatePlayerResponse", "/players", req).then((resp: sandengine.CreatePlayerResponse) => {
      if (resp.errors.length == 0) {
        PubSub.publish("auth.selectPlayer", resp.player);
      } else {
        this.$el.find("input").prop("disabled", false);
        this.$el.find(".alert").remove();

        var form = this.$el.find(".new-player-form");
        _.each(resp.errors, (err) => {
          $("<div>")
            .text(err)
            .addClass("alert alert-danger")
            .appendTo(form);
        });
      }
    });
  }
}
