/// <reference path="../lib/typings/pubsubjs/pubsub.d.ts" />
/// <reference path="../lib/typings/underscore/underscore.d.ts" />

/// <reference path="../proto.d.ts" />

var Keys = {
  enter: "13",
  shift: "16",
  ctrl: "17",
  escape: "27",
  space: "32",
  left: "37",
  up: "38",
  right: "39",
  down: "40",
  0: "48",
  1: "49",
  2: "50",
  3: "51",
  4: "52",
  5: "53",
  6: "54",
  7: "55",
  8: "56",
  9: "57",
  a: "65",
  d: "68",
  i: "73",
  s: "83",
  w: "87"
};

function keyToDirection(key): sandengine.Direction {
  switch (key) {
  case Keys.left:
  case Keys.a:
    return sandengine.Direction.LEFT;
  case Keys.up:
  case Keys.w:
    return sandengine.Direction.UP;
  case Keys.right:
  case Keys.d:
    return sandengine.Direction.RIGHT;
  case Keys.down:
  case Keys.s:
    return sandengine.Direction.DOWN;
  }

  return sandengine.Direction.UNKNOWN;
}

enum KeyboardState {
  off,
  running,
  inventory
}

// This class fires a few events when in the running state:
// - keyboard.move(sandengine.Direction)
class Keyboard {
  state: KeyboardState;
  keyboardStates: any;
  lastKeyboardCheck: number;
  keyboardRepeat: number;

  constructor() {
    this.state = KeyboardState.off;
    this.keyboardStates = {};
    this.lastKeyboardCheck = 0;
    this.keyboardRepeat = 100;

    window.addEventListener("keyup", (e: KeyboardEvent) => this.handleKeyUp(e));
    window.addEventListener("keydown", (e: KeyboardEvent) => this.handleKeyDown(e));
  }

  handleKeyUp(ev: KeyboardEvent) {
    this.keyboardStates[ev.keyCode] = false;

    // These actions do not have key repeat
    switch (ev.keyCode.toString()) {
    case Keys.escape:
      switch (this.state) {
      case KeyboardState.inventory:
        this.state = KeyboardState.running;
        PubSub.publish("keyboard.inventory", null);
        break;
      }
      break;
    case Keys.i:
      switch (this.state) {
      case KeyboardState.running:
        this.state = KeyboardState.inventory;
        PubSub.publish("keyboard.inventory", null);
        break;
      case KeyboardState.inventory:
        this.state = KeyboardState.running;
        PubSub.publish("keyboard.inventory", null);
        break;
      default:
        // Nothing to do in other states.
        break;
      }
      break;
    }
  }
  
  handleKeyDown(ev: KeyboardEvent) {
    this.keyboardStates[ev.keyCode] = true;
  }

  // Checks to see if any keys are down, and takes an appropriate action if needed.
  checkKeys() {
    var shiftKey = this.keyboardStates[Keys.shift];

    _.each(this.keyboardStates, (state, key: string) => {
      if (!state) {
        return;
      }

      switch (key) {
      case Keys.left:
      case Keys.right:
      case Keys.down:
      case Keys.up:
      case Keys.a:
      case Keys.s:
      case Keys.d:
      case Keys.w:
        if (this.state === KeyboardState.running) {
          var action = shiftKey ? "turn" : "move";
          var direction = keyToDirection(key);

          if (direction != sandengine.Direction.UNKNOWN) {
            PubSub.publish("keyboard.move", direction);
          }
        }
        break;
      case Keys.space:
        if (this.state === KeyboardState.running) {
          PubSub.publish("keyboard.interact", null);
        }
        break;
      }
    });
  };

  setState(state: KeyboardState) { this.state = state; }

  move(d: sandengine.Direction) {
    PubSub.publish("keyboard.move", d);
  }

  cycle(now: number, dt: number) {
    // Check for key repeat.
    if (now - this.lastKeyboardCheck >= this.keyboardRepeat) {
      this.lastKeyboardCheck = now;
      this.checkKeys();
    }
  }
}
