/// <reference path="../lib/typings/es6-promise/es6-promise.d.ts" />
/// <reference path="../lib/typings/jquery/jquery.d.ts" />
/// <reference path="../lib/typings/pubsubjs/pubsub.d.ts" />

/// <reference path="canvas.ts" />
/// <reference path="keyboard.ts" />
/// <reference path="player_select.ts" />
/// <reference path="../entity.ts" />
/// <reference path="../game.ts" />
/// <reference path="../region.ts" />
/// <reference path="../resources.ts" />
/// <reference path="../util.ts" />

interface HUD {
  show(entity: Entity);
  toggleInventory();
  hide();
  load(): Promise<void>;
  redraw();
}

// UI wraps anything that relates to UI.
//
// Events triggered:
// auth.success(authKey: string) - fired when we have a successful authentication.
class UI {
  static ErrorMessages = {
    "not found": "Invalid username or password."
  };

  game: Game;
  canvas: HTMLCanvasElement;
  renderer: CanvasRenderer;
  authKey: string;
  keyboard: Keyboard;
  hud: HUD;

  constructor(game: Game) {
    this.game = game;
    this.canvas = <HTMLCanvasElement>$("canvas").get(0);
    this.renderer = new CanvasRenderer(this.canvas);
    this.keyboard = new Keyboard();

    window.addEventListener("resize", () => this.fitCanvas());
    $("#signupLink").click(() => this.showSignupForm());
    $("#loginLink").click(() => this.showLoginForm());

    $(".login-form").submit((e) => this.attemptLogin(e));
    $(".signup-form").submit((e) => this.attemptSignup(e));
  }

  load(): Promise<void> {
    if (this.hud) {
      return this.hud.load();
    }
    return Promise.resolve<void>();
  }

  setHUD(hud: HUD) {
    this.hud = hud;
  }

  // drawLoading draws a loading screen on the canvas.
  drawLoading() {
    var text = "Loading...";

    var ctx = this.canvas.getContext("2d");

    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

    ctx.fillStyle = "white";
    ctx.font = "36px sans serif";
    var tm = ctx.measureText(text);
    ctx.fillText(
      text,
      this.canvas.width / 2 - tm.width / 2,
      this.canvas.height / 2
    );
  }

  // doneLoading tells the UI that the game has finished loading and is ready
  // to start drawing.
  doneLoading() {
    if (this.hud) {
      this.hud.show(this.game.playerEntity());
    }
  }

  // clearForms clears out all the data in all the forms.
  clearForms() {
    $("form input.form-field").val("");
    $("form .alert").remove();
  }

  // showLoginForm shows the login form.
  showLoginForm() {
    this.reset();
    $(".login-form").show();
    $("#loginUsername").focus();
  }

  // showLoginForm shows the signup form.
  showSignupForm() {
    this.reset();
    $(".signup-form").show();
    $("#signupUsername").focus();
  }

  // showCanvas hides all the forms and shows the canvas.
  showCanvas() {
    this.reset();
    $(".canvas").show();
    this.fitCanvas();
    this.keyboard.setState(KeyboardState.running);
  }

  toggleInventory() {
    if (this.hud) {
      this.hud.toggleInventory();
    }
  }

  hideHUD() {
    if (this.hud) {
      this.hud.hide();
    }
  }

  // showSelectPlayer shows the player select screen.
  showSelectPlayer() {
    this.reset();

    var playerSelect = new PlayerSelect(this.authKey);
    playerSelect.render();
    $(".player-select")
      .empty()
      .append(playerSelect.$el)
      .show();
  }

  // reset clears all the forms and hides everything.
  reset() {
    this.clearForms();
    $(".panel").hide();
    $(".canvas").hide();
    this.hideHUD();
    this.keyboard.setState(KeyboardState.off);
  }

  // fitCanvas re-adjusts the canvas to fit the display.
  fitCanvas() {
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
  }

  // attemptLogin attempts to log the player in.
  // This fires a auth.success message on a successful login.
  attemptLogin(e: Event) {
    e.stopPropagation();
    e.preventDefault();

    var req: sandengine.AuthRequest = Proto.create("AuthRequest");

    req.username = $("#loginUsername").val();
    req.password = $("#loginPassword").val();

    Proto.post("AuthResponse", "/login", req).then((resp: sandengine.AuthResponse) => {
      if (resp.errors.length == 0) {
        this.clearForms();
        console.log("Auth successful, starting game.");
        PubSub.publish("auth.success", resp.authKey);
      } else {
        $("form .alert").remove();

        resp.errors.forEach((err) => {
          $("<div>")
            .text(UI.ErrorMessages[err])
            .addClass("alert alert-danger")
            .appendTo($(".login-form"));
        })
      }
    });
  }

  // attemptSignup attempts to sign the player up.
  // On success, this will log them in and fire a auth.success event.
  attemptSignup(e: Event) {
    e.stopPropagation();

    var req: sandengine.SignupRequest = Proto.create("SignupRequest");

    req.username = $("#signupUsername").val();
    req.email = $("#signupEmail").val();
    req.password = $("#signupPassword").val();

    // TODO: client-side form validation

    Proto.post("SignupResponse", "/signup", req).then((resp: sandengine.SignupResponse) => {
      if (resp.errors.length == 0) {
        this.clearForms();
        this.showCanvas();

        console.log("Signup successful, starting game.");

        PubSub.publish("auth.success", resp.authKey);
      } else {
        $("form .alert").remove();

        $("<div>")
          .addClass("alert alert-danger")
          .html(resp.errors.join("<br />"))
          .appendTo($(".signupForm"));
      }
    });
  }

  // draw renders a region onto the canvas centred on a particular player.
  // TODO: Would be cool to have a camera object that can move around.
  draw(region: Region, resources: ResourceManager, player: Entity, dt: number) {
    this.renderer.draw(region, resources, player, dt);
  }

  // cycle processes any information that needs to be processed on this cycle.
  cycle(now: number, dt: number) {
    this.keyboard.cycle(now, dt);
    // delete old messages
    /*while (this.messages.length > 0 &&
        this.messages[this.messages.length - 1].timestamp <
          now - this.messageEndFade) {
      this.messages.pop();
    }*/
  }
}
