/// <reference path="render_utils.ts" />
/// <reference path="../geometry.ts" />
/// <reference path="../player.ts" />
/// <reference path="../region.ts" />
/// <reference path="../resources.ts" />

class CanvasRenderer {
  needsTerrainUpdate: boolean;
  terrainCanvas: HTMLCanvasElement;

  constructor(private canvas: HTMLCanvasElement) {
    this.needsTerrainUpdate = true;
    this.terrainCanvas = <HTMLCanvasElement>document.createElement("canvas");
  }

  draw(region: Region, resources: ResourceManager, player: Entity, dt: number) {
    var ctx: CanvasRenderingContext2D = this.canvas.getContext("2d");

    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    this.drawTerrain(ctx, region, resources, player, dt);
    this.drawEntities(ctx, region, player, dt);
    this.drawDaylight(ctx, dt);
  }

  drawTerrain(ctx: CanvasRenderingContext2D, region: Region,
      resources: ResourceManager, player: Entity, dt: number) {
    var ts = region.getTileset();
    if (this.needsTerrainUpdate) {
      this.terrainCanvas.width = region.getWidth() * ts.getTileWidth();
      this.terrainCanvas.height = region.getHeight() * ts.getTileHeight();

      var offscreenCtx = this.terrainCanvas.getContext("2d");

      var img = <HTMLImageElement>resources.get(ts.getImageResource());

      for (var x = 0; x < region.getWidth(); x++) {
        for (var y = 0; y < region.getHeight(); y++) {
          var tile = region.getTileAt(x, y);

          offscreenCtx.drawImage(
            img,
            ts.getTileWidth() * ts.getTileSubtype(tile).getOffset(),
            ts.getTileHeight() * 2 * tile.getGroupNum(),
            ts.getTileWidth(),
            ts.getTileHeight(),
            x * ts.getTileWidth(),
            y * ts.getTileHeight(),
            ts.getTileWidth(),
            ts.getTileHeight()
          );

          this.drawTileBorders(offscreenCtx, resources, region, x, y);
        }
      }

      this.needsTerrainUpdate = false;
    }

    var coords = this.viewportTopLeft(ctx, player, ts);

    ctx.drawImage(
      this.terrainCanvas,
      coords[0],
      coords[1],
      ctx.canvas.width,
      ctx.canvas.height,
      0,
      0,
      ctx.canvas.width,
      ctx.canvas.height
    );
  }

  drawTileBorders(ctx: CanvasRenderingContext2D, resources: ResourceManager,
      region: Region, x: number, y: number) {
    var i = 1;
    var group: number;
    var neighbourTypes: any = {};
    var mainTile = region.getTileAt(x, y);

    _.each(Direction.offsets, (dir) => {
      var tile = region.getTileAt(x + dir[0], y + dir[1]);

      if (tile) {
        group = tile.getGroupNum();

        neighbourTypes[group] = neighbourTypes[group] || 0;
        neighbourTypes[group] |= i;
      }
      i <<= 1;
    });

    var ts = region.getTileset();
    var img = <HTMLImageElement>resources.get(ts.getImageResource());
    _.each(neighbourTypes, (tile: number, group: number) => {
      if (mainTile.getGroupNum() < group) {
        // render the edge on me
        ctx.drawImage(
          img,
          ts.getTileWidth() * (tile - 1),
          ts.getTileHeight() * (2 * group - 1),
          ts.getTileWidth(),
          ts.getTileHeight(),
          x * ts.getTileWidth(),
          y * ts.getTileHeight(),
          ts.getTileWidth(),
          ts.getTileHeight()
        );
      }
    });
  }

  drawEntities(ctx: CanvasRenderingContext2D, region: Region, player: Entity, dt: number) {
    var offset = this.viewportTopLeft(ctx, player, region.getTileset());
    // TODO: Use a fancy data structure to keep entities sorted.
    var entities = this.sortByY(region.getEntities());
    _.each(entities, (ent) => {
      ent.draw(ctx, region, dt, offset);
    });
  }

  // Just for fun, shade the world based on the time of day.
  drawDaylight(ctx: CanvasRenderingContext2D, dt: number) {
    var alpha = (this.timeOfDay() * 0.5).toFixed(3);
    ctx.fillStyle = "rgba(0, 0, 0, " + alpha + ")";
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  }

  timeOfDay(): number {
    var time = new Date().getTime(),
      msPerDay = 3600 * 1000;

    return Math.sin((time % msPerDay) / msPerDay * 2 * Math.PI) / 2 + 0.5;
  }

  // Get the top-left of the viewport within the terrain canvas, based on where
  // the player is and the tileset we are looking at.
  viewportTopLeft(ctx: CanvasRenderingContext2D, player: Entity, ts: Tileset): number[] {
    var coords = w2s(player.getLocation(), ts);
    var x = coords[0] - ctx.canvas.width / 2;
    var y = coords[1] - ctx.canvas.height / 2;

    // clamp coordinates
    x = Math.max(0,
        Math.min(this.terrainCanvas.width - ctx.canvas.width, x));
    y = Math.max(0,
        Math.min(this.terrainCanvas.height - ctx.canvas.height, y));

    return [x, y];
  }

  // Sorts a list of entities by their Y coordinate.
  sortByY(entities: Array<Entity>): Array<Entity>{
    return _.sortBy(entities, (ent: Entity) => ent.getLocation().getY());
  }
}
