/// <reference path="../geometry.ts" />
/// <reference path="../proto.d.ts" />

class SpriteData {
  constructor(private proto: sandengine.Sprite, private image: HTMLImageElement) {}

  getProto(): sandengine.Sprite { return this.proto; }
  getImage(): HTMLImageElement { return this.image; }
}

class Sprite {
  anim: number;

  constructor(private data: SpriteData) {
    this.anim = data.getProto().getDefaultAnim() || 0;
  }

  current(): sandengine.Animation {
    return this.data.getProto().getAnimations()[this.anim];
  }

  setAnimation(anim: number) {
    this.anim = anim;
  }

  draw(ctx: CanvasRenderingContext2D, facing: sandengine.Direction, x: number, y: number, dt: number) {
    // directional convention:
    // ortho: left, up, right, down
    // diag: tl, t, tr, l, r, bl, b, br
    var frame: number = 0;
    if (this.current().getAxis() != sandengine.SpriteAxis.STATIC) {
      frame = facing;
    }

    ctx.drawImage(
      this.data.getImage(),
      this.getWidth() * frame + (this.current().getOffset() || 0),
      0,
      this.getWidth(),
      this.getHeight(),
      x + (this.data.getProto().getXOffset() || 0),
      y + (this.data.getProto().getYOffset() || 0),
      this.getWidth(),
      this.getHeight()
    );
  }

  getWidth(): number { return this.data.getProto().getWidth() || 0; }
  getHeight(): number { return this.data.getProto().getHeight() || 0; }
}
