/// <reference path="proto.d.ts" />
/// <reference path="region.ts" />
/// <reference path="resources.ts" />
/// <reference path="ui/render_utils.ts" />
/// <reference path="ui/sprite.ts" />

class Entity {
  sprite: Sprite;
  properties: any;
  privates: any;

  constructor(private proto: sandengine.Entity, resources: ResourceManager) {
    var data = <SpriteData>resources.get("entities/" + proto.getEntityType());
    if (data) {
      // This entity has a sprite.
      this.sprite = new Sprite(data);
    }

    this.loadProperties();
    this.privates = {};
  }

  loadProperties() {
    this.properties = JSON.parse(this.proto.properties);
  }

  getId(): string { return this.proto.getId(); }
  getLocation(): sandengine.Location { return this.proto.getLocation(); }
  getAttr<T>(name: string): T {
    return <T>(this.properties[name] || "");
  }
  getPrattr<T>(name: string): T {
    return <T>(this.privates[name] || "");
  }

  update(proto: sandengine.Entity) {
    this.proto = proto;
    this.loadProperties();
  }

  updatePrivates(privs: any) {
    this.privates = privs;
  }

  draw(ctx: CanvasRenderingContext2D, region: Region, dt: number, offset: number[]) {
    var coords = w2s(this.getLocation(), region.getTileset());

    // Draw our sprite.
    // TODO: does it make it faster to skip drawing the sprite if it's not on
    // the screen?
    this.sprite.draw(ctx, this.getLocation().getOrientation(),
        coords[0] - offset[0],
        coords[1] - offset[1], dt);
  }
}

interface EntityFactory {
  create(proto: sandengine.Entity): Entity;
}
