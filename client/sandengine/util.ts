/// <reference path="lib/typings/es6-promise/es6-promise.d.ts" />
/// <reference path="lib/typings/jquery/jquery.d.ts" />
/// <reference path="lib/typings/protobufjs/protobufjs.d.ts" />

/// <reference path="proto.d.ts" />

var PB = window["dcodeIO"].ProtoBuf;

class Proto {
  static sandengine: any;

  static load(): Promise<boolean> {
    return new Promise<boolean>(function(resolve, reject) {
      $.get("/static/js/proto.json").then(function(json) {
        Proto.sandengine = PB.loadJson(json).build().sandengine;
        resolve();
      }, reject);
    });
  }

  static create(type: string): any {
    return new Proto.sandengine[type];
  }

  static parse(type: string, data: string): any {
    // There appears to be an edge case: if data is an empty string then this
    // is a valid proto, but the ProtoBuf lib doesn't like it.
    return data ? Proto.sandengine[type].decode64(data) : Proto.create(type);
  }

  static parseRaw(type: string, data: ByteBuffer): any {
    // There appears to be an edge case: if data is an empty string then this
    // is a valid proto, but the ProtoBuf lib doesn't like it.
    return data ? Proto.sandengine[type].decode(data) : Proto.create(type);
  }

  static get(type: string, url: string, args?: any): Promise<any> {
    return Proto.fetch(type, url, "GET", args);
  }

  static post(type: string, url: string, args?: any): Promise<any> {
    return Proto.fetch(type, url, "POST", args);
  }

  static fetch(type: string, url: string, method: any, args: any): Promise<any> {
    return new Promise<any>(function(resolve, reject) {
      $.ajax({
        contentType: "application/x-google-protobuf",
        data: args ? args.encode64() : undefined,
        dataType: "application/x-google-protobuf",
        error: (xhr) => {
          // The server sends back error codes for HTTP compliance, but the
          // interesting stuff is actually part of the proto.
          resolve(Proto.parse(type, xhr.responseText));
        },
        method: method,
        success: (data) => {
          resolve(Proto.parse(type, data));
        },
        complete: (xhr) => {
          resolve(Proto.parse(type, xhr.responseText));
        },
        url: url
      });
    });
  }
}
