class Direction {
  static offsets = [
    [-1, 0],
    [0, -1],
    [1, 0],
    [0, 1]
  ];
}
