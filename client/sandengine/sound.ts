/// <reference path="lib/typings/es6-promise/es6-promise.d.ts" />

class Sound {
  // We cache a number of copies of the audio node since the browser can only
  // play one at a time.
  numSamples: number;
  currentSample: number;
  sounds: Array<HTMLAudioElement>;

  loaded: boolean;

  constructor() {
    this.numSamples = 5;
    this.currentSample = 0;
    this.sounds = [];
    this.loaded = false;
  }

  load(url: string): Promise<boolean> {
    return new Promise<HTMLAudioElement>((resolve, reject) => {
      var sound = new Audio();
      sound.addEventListener("canplay", () => { resolve(sound); });
      sound.src = url;
    }).then((sound) => {
      this.sounds[0] = sound;

      for (var i = 1; i < this.numSamples; i++) {
        this.sounds[i] = <HTMLAudioElement>sound.cloneNode();
      }

      this.loaded = true;
      return true;
    });
  }

  play(volume: number) {
    if (volume <= 0.0) {
      return;
    }
    
    if (!this.loaded) {
      return;
    }

    var sound = this.sounds[this.currentSample];
    sound.volume = volume;
    sound.play();

    this.currentSample = (this.currentSample + 1) % this.numSamples;
  }
}
