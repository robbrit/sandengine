/// <reference path="lib/typings/es6-promise/es6-promise.d.ts" />

/// <reference path="entity.ts" />
/// <reference path="proto.d.ts" />
/// <reference path="resources.ts" />
/// <reference path="tileset.ts" />
/// <reference path="util.ts" />

class Region {
  entities: any;

  constructor(private proto: sandengine.Region, private tileset: Tileset) {
    this.entities = {};
  }
  getName(): string { return this.proto.name; }
  getWidth(): number { return this.proto.width; }
  getHeight(): number { return this.proto.height; }
  getTileset(): Tileset { return this.tileset; }

  getTileAt(x: number, y: number): sandengine.TileData.Tile {
    if (x < 0 || y < 0 || x >= this.getWidth() || y >= this.getHeight()) {
      return null;
    }
    var tiles = this.proto.getTiles();
    return tiles.getTiles()[y * this.getWidth() + x];
  }

  getEntities(): Array<Entity> { return _.values(this.entities); }
  setEntities(entities: Array<Entity>) {
    this.entities = {};
    _.each(entities, (ent: Entity) => {
      this.entities[ent.getId()] = ent;
    });
  }
  getEntity(id: string): Entity {
    return this.entities[id];
  }
  addEntity(ent: Entity) {
    // TODO: What if the entity is already there?
    this.entities[ent.getId()] = ent;
  }
  removeEntity(id: string) {
    if (this.entities[id]) {
      delete this.entities[id];
    }
  }
}
