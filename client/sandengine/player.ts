/// <reference path="proto.d.ts" />

class Player {
  constructor(private proto: sandengine.Player) {}

  getName(): string { return this.proto.name; }
  getEntityId(): string { return this.proto.getEntityId(); }
}
