/// <reference path="lib/typings/es6-promise/es6-promise.d.ts" />

/// <reference path="proto.d.ts" />
/// <reference path="util.ts" />

class Tileset {
  constructor(private proto: sandengine.Tileset) {}
  getTileWidth(): number { return this.proto.tile_width; }
  getTileHeight(): number { return this.proto.tile_height; }
  getImageResource(): string { return this.proto.image_resource; }

  getTileGroup(tile: sandengine.TileData.Tile): sandengine.TileGroup {
    return this.proto.getGroups()[tile.group_num || 0];
  }

  getTileSubtype(tile: sandengine.TileData.Tile): sandengine.TileSubtype {
    var group = this.getTileGroup(tile);
    return group.getSubtypes()[tile.subtype_num || 0];
  }
}
