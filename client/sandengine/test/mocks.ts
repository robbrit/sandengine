/// <reference path="../proto.d.ts" />
/// <reference path="../region.ts" />
/// <reference path="../tileset.ts" />

function fakeRegion(name: string): Region {
  var ts = fakeTilesetProto();
  return new Region(fakeRegionProto(name, ts), fakeTileset(ts));
}

function fakeTileset(proto: sandengine.Tileset): Tileset {
  return new Tileset(proto);
}

function fakeTilesetProto(): sandengine.Tileset {
  var proto = <sandengine.Tileset>Proto.create("Tileset");
  proto.setName("forest");
  proto.setImageResource("tilesets/forest.png");
  proto.setTileWidth(40);
  proto.setTileHeight(40);
  return proto;
}

function fakeRegionProto(name: string, ts: sandengine.Tileset): sandengine.Region {
  var proto = <sandengine.Region>Proto.create("Region");
  proto.setName(name);
  proto.setWidth(0);
  proto.setHeight(0);

  var tiles = <sandengine.TileData>Proto.create("TileData");
  tiles.setTileset(ts.getName());
  for (var x = 0; x < proto.width; x++) {
    for (var y = 0; y < proto.height; y++) {
      // TODO
    }
  }
  return proto;
}

function fakeEntityProto(id: string): sandengine.Entity {
  var proto = <sandengine.Entity>Proto.create("Entity");
  proto.setId(id);
  return proto;
}
