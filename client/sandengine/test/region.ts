/// <reference path="../lib/typings/mocha/mocha.d.ts" />
/// <reference path="../lib/typings/chai/chai.d.ts" />

/// <reference path="../proto.d.ts" />
/// <reference path="../region.ts" />
/// <reference path="../resources.ts" />
/// <reference path="../util.ts" />
/// <reference path="mocks.ts" />

var assert = chai.assert;
var resources = new ResourceManager("");

before((done) => {
  Proto.load().then(() => {
    return resources.load();
  }).then(() => {
    done();
  });
});

describe("Region", () => {
  describe("entities", () => {
    it("should return null for non-existent entity", (done) => {
      var region = fakeRegion("1");
      assert.isUndefined(region.getEntity("a"));
      assert.lengthOf(region.getEntities(), 0);
      done();
    });
    it("should get an entity after addEntity", (done) => {
      var region = fakeRegion("1");
      region.addEntity(new Entity(fakeEntityProto("a"), resources));

      var ent = region.getEntity("a");
      assert.isDefined(ent);
      assert.equal("a", ent.getId());

      var ents = region.getEntities();
      assert.lengthOf(ents, 1);
      assert.include(ents, ent);
      done();
    })
    it("should get an entity after setEntities", (done) => {
      var region = fakeRegion("1");
      region.setEntities([new Entity(fakeEntityProto("a"), resources)]);

      var ent = region.getEntity("a");
      assert.isDefined(ent);
      assert.equal("a", ent.getId());

      var ents = region.getEntities();
      assert.lengthOf(ents, 1);
      assert.include(ents, ent);
      done();
    })
  });

  describe("tileset", () => {
    it("should get the correct tile", (done) => {
      // TODO
      done();
    });

    it("should return null for out-of-bounds tiles", (done) => {
      // TODO
      done();
    });
  });
});
