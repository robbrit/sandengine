/// <reference path="../sandengine/lib/typings/es6-promise/es6-promise.d.ts" />
/// <reference path="../sandengine/lib/typings/jquery/jquery.d.ts" />

/// <reference path="../sandengine/entity.ts" />

class SandbuilderHUD {
  entity: Entity;
  el: JQuery;
  inventorySize: number;
  loaded: boolean;

  constructor() {
    this.el = jQuery(".hud");
    this.loaded = false;

    this.inventorySize = 15 * 5;
  }

  show(entity: Entity) {
    this.entity = entity;
    this.el.show();
  }

  toggleInventory() {
    this.el.find(".inventory").toggle();
  }

  hide() {
    this.el.hide();
  }

  load(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.el.load("/template/hud.tmpl.html", () => {
        this.loaded = true;
        this.bindEvents();
        this.redraw();
        resolve();
      });
    });
  }

  bindEvents() {
    this.el.find(".inventory-search").keyup(() => this.redraw());
  }

  redraw() {
    if (!this.loaded || !this.entity) {
      return;
    }

    // Draw the entity"s inventory.
    this.drawInventory();
  }

  drawInventory() {
    var items = this.entity.getPrattr<any>("items");
    var el = this.el.find(".items").empty();
    var search = this.el.find(".inventory-search").val();
    _.each(items, (amount: number, name: string) => {
      if (search != "" && name.indexOf(search) < 0) {
        return;
      }
      // TODO: Do we need a fancier templating system?
      jQuery("<div>")
        .addClass("item-row")
        .text(name + ": " + amount)
        .appendTo(el);
    });
  }
}
