/// <reference path="../sandengine/entity.ts" />
/// <reference path="../sandengine/player.ts" />
/// <reference path="../sandengine/proto.d.ts" />
/// <reference path="../sandengine/resources.ts" />

class PlayerEntity extends Entity {
  draw(ctx: CanvasRenderingContext2D, region: Region, dt: number, offset: number[]) {
    var coords = w2s(this.getLocation(), region.getTileset());

    // draw basic sprite
    super.draw(ctx, region, dt, offset);

    // and draw our name centred above our head
    ctx.font = "16px sans serif";
    var name = this.getAttr<string>("name");
    var tm = ctx.measureText(name);

    var tx = coords[0] - offset[0] - (tm.width - this.sprite.getWidth()) / 2;
    var ty = coords[1] - offset[1];

    // Draw the name above the player with a little bit of a drop-shadow.
    ctx.fillStyle = "#333333";
    ctx.fillText(name, tx + 2, ty + 2);
    ctx.fillStyle = "#00FF00";
    ctx.fillText(name, tx, ty);
  }
}

class TreeEntity extends Entity {
  // Trees get old after 7 days.
  static OldTime = 7 * 86400;

  draw(ctx: CanvasRenderingContext2D, region: Region, dt: number, offset: number[]) {
    var now = new Date().getTime() / 1000;

    if (now > this.getAttr<number>("birthtime") + TreeEntity.OldTime) {
      this.sprite.setAnimation(1);
    } else {
      this.sprite.setAnimation(0);
    }

    super.draw(ctx, region, dt, offset);
  }
}

class PineTreeEntity extends Entity {
  // Pine trees age at 7 and 14 days.
  static MediumTime = 7 * 86400;
  static OldTime = 14 * 86400;

  draw(ctx: CanvasRenderingContext2D, region: Region, dt: number, offset: number[]) {
    var now = new Date().getTime() / 1000;

    if (now > this.getAttr<number>("birthtime") + PineTreeEntity.OldTime) {
      this.sprite.setAnimation(2);
    } else if (now > this.getAttr<number>("birthtime") + PineTreeEntity.MediumTime) {
      this.sprite.setAnimation(1);
    } else {
      this.sprite.setAnimation(0);
    }

    super.draw(ctx, region, dt, offset);
  }
}
