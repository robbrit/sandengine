/// <reference path="../sandengine/entity.ts" />
/// <reference path="../sandengine/game.ts" />
/// <reference path="../sandengine/proto.d.ts" />
/// <reference path="entities.ts" />

class SandbuilderFactory implements EntityFactory {
  constructor(private game: Game) {}

  create(proto: sandengine.Entity): Entity {
    switch (proto.getEntityType()) {
    case "player":
      return new PlayerEntity(proto, this.game.getResourceManager());
    case "cotton_tree":
    case "tree":
      return new TreeEntity(proto, this.game.getResourceManager());
    case "pine_tree":
      return new PineTreeEntity(proto, this.game.getResourceManager());
    case "boulder":
    case "bush":
    case "chicken":
    case "dead_tree":
    case "fern":
    case "lilypad":
    case "loot":
      return new Entity(proto, this.game.getResourceManager());
    default:
      console.log("Unknown entity type " + proto.getEntityType());
    }
    return null;
  }
}
