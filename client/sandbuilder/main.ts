/// <reference path="../sandengine/game.ts" />
/// <reference path="entity_factory.ts" />
/// <reference path="hud.ts" />

function main() {
  var game = new Game();
  game.setEntityFactory(new SandbuilderFactory(game));
  game.getUI().setHUD(new SandbuilderHUD());
  game.run();
}
