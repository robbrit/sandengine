# Transitions

Moving between regions algorithm:
1) leave my current region, save where I am
2) send a "change region" command to client
3) client sends a "join region" to the new region
