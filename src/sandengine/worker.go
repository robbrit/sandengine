package sandengine

// A Worker continuously runs, causing some sort of effect on some region.
type Worker interface {
	// Start starts the worker with the provided RegionStore. Does not trigger
	// its own goroutine.
	Start(RegionStore) error
	// Close closes the worker.
	Close()
}

// A WorkerManager manages a number of workers.
type WorkerManager interface {
	Add(Worker)
	Start(RegionStore) error
	Close()
}

type workerManagerImpl struct {
	workers []Worker
}

func (w *workerManagerImpl) Add(worker Worker) {
	w.workers = append(w.workers, worker)
}

func (w *workerManagerImpl) Start(rs RegionStore) error {
	for _, worker := range w.workers {
		if err := worker.Start(rs); err != nil {
			// TODO: Should collect workers and return rather than failing.
			return err
		}
	}
	return nil
}

func (w *workerManagerImpl) Close() {
	for _, worker := range w.workers {
		worker.Close()
	}
}

func NewWorkerManager() WorkerManager {
	return &workerManagerImpl{}
}
