package utils

import (
	"math/rand"

	pb "sandengine/proto"
)

// Location is a position in a region.
type Location pb.Location

// ToLoc converts a *pb.Location to a *utils.Location.
func ToLoc(loc *pb.Location) *Location {
	return (*Location)(loc)
}

// ToPB converts a *utils.Location into a *pb.Location.
func (l *Location) ToPB() *pb.Location {
	return (*pb.Location)(l)
}

// Move calculates the movement from a given location in a given direction.
func Move(l *Location, d pb.Direction) *Location {
	res := *l
	switch d {
	case pb.Direction_LEFT:
		res.X--
	case pb.Direction_RIGHT:
		res.X++
	case pb.Direction_UP:
		res.Y--
	case pb.Direction_DOWN:
		res.Y++
	}
	res.Orientation = d
	return &res
}

// LocEqual checks to see if two locations are equal.
func LocEqual(l1, l2 *Location) bool {
	return l1.X == l2.X && l1.Y == l2.Y && l1.Region == l2.Region
}

// RandomDirection gives a random direction.
func RandomDirection() pb.Direction {
	return pb.Direction(rand.Intn(int(pb.Direction_UNKNOWN)))
}
