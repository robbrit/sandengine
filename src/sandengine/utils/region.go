package utils

import (
	pb "sandengine/proto"
)

// InBounds checks if a location is within the bounds of a region.
func InBounds(loc *Location, r *pb.Region) bool {
	return loc.X >= 0 && loc.X < r.Width && loc.Y >= 0 && loc.Y < r.Height
}
