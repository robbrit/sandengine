package utils

// This file contains a number of useful utilities for all across the engine.

import (
	"encoding/base64"

	"github.com/golang/protobuf/proto"
)

// ParseProto extracts a base64-encoded proto from data.
func ParseProto(data string, pb proto.Message) error {
	outBytes, err := base64.StdEncoding.DecodeString(data)
	if err != nil {
		return err
	}

	return proto.Unmarshal(outBytes, pb)
}

// SerializeProto serializes a proto into a base64-encoded byte array.
func SerializeProto(pb proto.Message) (string, error) {
	bytes, err := proto.Marshal(pb)
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(bytes), nil
}
