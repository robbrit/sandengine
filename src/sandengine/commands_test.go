package sandengine

import (
	"fmt"
	"reflect"
	"strings"
	"testing"

	"github.com/golang/protobuf/proto"

	pb "sandengine/proto"
)

func TestMapCommand(t *testing.T) {
	entityID := "asdf"

	for _, test := range []struct {
		desc       string
		cmd        proto.Message
		wantResult interface{}
		wantError  bool
	}{
		{"MoveCmd", &pb.MoveCmd{pb.Direction_UP}, &MoveCmd{entityID, pb.Direction_UP}, false},
		{"InteractCmd", &pb.InteractCmd{}, &InteractCmd{entityID}, false},
		{"error", nil, nil, true},
	} {
		var cmd *pb.Command
		if test.cmd == nil {
			cmd = &pb.Command{Name: "notarealcommand"}
		} else {
			data, err := proto.Marshal(test.cmd)
			if err != nil {
				t.Errorf("%s: got unexpected error on proto.Marshal: %s", test.desc, err)
				continue
			}
			cmd = &pb.Command{
				Name: strings.Replace(fmt.Sprintf("%T", test.cmd), "*sandengine.", "", -1),
				Data: data,
			}
		}

		got, gotErr := MapCommand(cmd, entityID)

		if test.wantError {
			if gotErr == nil {
				t.Errorf("%s: got nil error, want non-nil", test.desc)
			}
			continue
		}
		if gotErr != nil {
			t.Errorf("%s: got error %s, want nil", test.desc, gotErr)
		}

		if !reflect.DeepEqual(got, test.wantResult) {
			t.Errorf("%s: got %q, want %q", test.desc, got, test.wantResult)
		}
	}
}
