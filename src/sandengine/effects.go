package sandengine

import (
	"github.com/golang/protobuf/proto"
)

// An Effect is some sort of thing happening in a region.
type Effect interface {
	proto.Message

	// IsBroadcast determines if the effect should be broadcast to all the clients
	// in a region, or just the one it is being returned to.
	IsBroadcast() bool
	// IsExit determins if an effect is causing a client to stop listening to a
	// region.
	IsExit() bool
}
