package sandengine

import (
	"io/ioutil"

	"sandengine/utils"

	pb "sandengine/proto"
)

// TilesetStore provides an interface for loading and accessing tileset data.
type TilesetStore interface {
	// Get fetches an already loaded tileset.
	Get(name string) *pb.Tileset
	// Load loads a tileset from the disk.
	Load(path string) (*pb.Tileset, error)
}

type tilesetStore struct {
	tilesets map[string]*pb.Tileset
}

func (ts *tilesetStore) Get(name string) *pb.Tileset {
	return ts.tilesets[name]
}

func (ts *tilesetStore) Load(path string) (*pb.Tileset, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	var t pb.Tileset
	if err = utils.ParseProto(string(data), &t); err != nil {
		return nil, err
	}

	ts.tilesets[t.Name] = &t
	return ts.tilesets[t.Name], nil
}

// NewTilesetStore constructs a new TilesetStore object.
func NewTilesetStore() TilesetStore {
	return &tilesetStore{
		tilesets: map[string]*pb.Tileset{},
	}
}
