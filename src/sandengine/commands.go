package sandengine

import (
	"fmt"

	"github.com/golang/protobuf/proto"
	pb "sandengine/proto"
	"sandengine/utils"
)

type Command interface{}

type ListEntities struct {
	WithPrivate bool
}

type JoinCmd struct {
	Player *pb.Player
}

type MoveCmd struct {
	EntityID  string
	Direction pb.Direction
}

type InteractCmd struct {
	EntityID string
}

type RemoveCmd struct {
	EntityID string
}

type MakeNoise struct {
	Location *utils.Location
	Name     string
}

// MapCommand parses the proto stored in the command, and maps it to a command
// type.
func MapCommand(cmd *pb.Command, entityID string) (Command, error) {
	switch cmd.Name {
	case "MoveCmd":
		move := &pb.MoveCmd{}
		if err := proto.Unmarshal(cmd.Data, move); err != nil {
			return nil, err
		}
		return &MoveCmd{entityID, move.Direction}, nil
	case "InteractCmd":
		remove := &pb.InteractCmd{}
		if err := proto.Unmarshal(cmd.Data, remove); err != nil {
			return nil, err
		}
		return &InteractCmd{entityID}, nil
	default:
		// TODO: handle custom commands.
		return nil, fmt.Errorf("unknown command type %s", cmd.Name)
	}
}
