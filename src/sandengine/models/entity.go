package models

import (
	"sandengine"
	pb "sandengine/proto"
	"sandengine/utils"
)

type Entity struct {
	EntityID string `gorm:"primary_key"`
	Proto    string
	Privates string
	Region   string
}

// Unmarshal converts an Entity into a pb.Entity.
func (e *Entity) Unmarshal() (*pb.Entity, error) {
	var ent pb.Entity
	if err := utils.ParseProto(e.Proto, &ent); err != nil {
		return nil, err
	}
	return &ent, nil
}

// Marshal loads a pb.Entity into this Entity object.
func (e *Entity) Marshal(ent sandengine.Entity) error {
	epb := ent.Proto()
	raw, err := utils.SerializeProto(epb)
	if err != nil {
		return err
	}
	e.Proto = raw
	e.Privates = ent.Pr().MustJSON()
	e.EntityID = epb.Id
	e.Region = epb.GetLocation().Region
	return nil
}
