package models

import (
	"sandengine/utils"

	pb "sandengine/proto"
)

type Player struct {
	Proto    string
	Name     string
	Username string
}

// Unmarshal converts a Player into a pb.Player.
func (p *Player) Unmarshal() (*pb.Player, error) {
	var player pb.Player
	if err := utils.ParseProto(p.Proto, &player); err != nil {
		return nil, err
	}
	return &player, nil
}

// Marshal loads a pb.Player into this Player object.
func (p *Player) Marshal(ppb *pb.Player) error {
	raw, err := utils.SerializeProto(ppb)
	if err != nil {
		return err
	}
	p.Proto = raw
	p.Name = ppb.Name
	p.Username = ppb.Username
	return nil
}
