package mocks

import (
	"sandengine"
)

type Game struct {
	Regions  sandengine.RegionStore
	Players  sandengine.PlayerStore
	Tilesets sandengine.TilesetStore
	Entities sandengine.EntityStore
}

func (g *Game) Start() {}

func (g *Game) RegionStore() sandengine.RegionStore       { return g.Regions }
func (g *Game) PlayerStore() sandengine.PlayerStore       { return g.Players }
func (g *Game) EntityStore() sandengine.EntityStore       { return g.Entities }
func (g *Game) TilesetStore() sandengine.TilesetStore     { return g.Tilesets }
func (g *Game) EntityFactory() sandengine.EntityFactory   { return nil }
func (g *Game) PhysicsManager() sandengine.PhysicsManager { return nil }
func (g *Game) WorkerManager() sandengine.WorkerManager   { return nil }
