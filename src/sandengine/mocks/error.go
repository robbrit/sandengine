package mocks

import (
	"errors"
)

// ErrFake is the error that is always used by the mocks.
var ErrFake = errors.New("this is an error")
