package mocks

import (
	"sandengine"
	pb "sandengine/proto"
)

type tilesetStore struct {
	err     error
	tileset *pb.Tileset
}

// NewTilesetStore constructs a fake TilesetStore for testing.
func NewTilesetStore(err error, t *pb.Tileset) sandengine.TilesetStore {
	return &tilesetStore{
		err:     err,
		tileset: t,
	}
}

// NewErrTilesetStore is a shorthand for a TilesetStore that always errors.
func NewErrTilesetStore() sandengine.TilesetStore {
	return NewTilesetStore(ErrFake, nil)
}

func (ts *tilesetStore) Get(name string) *pb.Tileset {
	return ts.tileset
}

func (ts *tilesetStore) Load(path string) (*pb.Tileset, error) {
	return ts.tileset, ts.err
}
