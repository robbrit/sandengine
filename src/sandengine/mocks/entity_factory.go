package mocks

import (
	"github.com/stretchr/objx"
	"sandengine"
	"sandengine/utils"
)

type EntityFactory struct{}

func (ef *EntityFactory) Create(kind string, loc *utils.Location, props objx.Map, privs objx.Map) sandengine.Entity {
	return &Entity{"1", loc}
}

func (ef *EntityFactory) Build(id string, kind string, loc *utils.Location, props objx.Map, privs objx.Map) sandengine.Entity {
	return &Entity{id, loc}
}

func (ef *EntityFactory) NewPlayer(name string) sandengine.Entity {
	return &Entity{"1", &utils.Location{}}
}
