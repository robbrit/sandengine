package mocks

import (
	"sandengine"

	pb "sandengine/proto"
)

// playerStore is a mock implementation of sandengine.PlayerStore.
type playerStore struct {
	err    error
	player *pb.Player
}

// NewPlayerStore creates a new mock PlayerStore.
func NewPlayerStore(err error, p *pb.Player) sandengine.PlayerStore {
	return &playerStore{
		err:    err,
		player: p,
	}
}

// NewErrPlayerStore is a shorthand for a PlayerStore that always errors.
func NewErrPlayerStore() sandengine.PlayerStore {
	return NewPlayerStore(ErrFake, nil)
}

// FindPlayer finds a player for a given player name.
func (ps *playerStore) FindPlayer(string) (*pb.Player, error) {
	return ps.player, ps.err
}

// FindPlayersForUser finds all the players for a given username.
func (ps *playerStore) FindPlayersForUser(string) ([]*pb.Player, error) {
	if ps.player == nil {
		return nil, ps.err
	}
	return []*pb.Player{ps.player}, ps.err
}

// CreatePlayer creates a new player with a given name and username.
func (ps *playerStore) CreatePlayer(string, string) (*pb.Player, error) {
	return ps.player, ps.err
}

// SavePlayer persists the player to the database.
func (ps *playerStore) SavePlayer(*pb.Player) error {
	return ps.err
}
