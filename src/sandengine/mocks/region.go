package mocks

import (
	"sandengine"

	pb "sandengine/proto"
)

// A Region is a fake sandengine.Region used for testing.
type Region struct {
	data *pb.Region
}

// NewRegion gives a new fake Region.
func NewRegion(data *pb.Region) *Region {
	return &Region{
		data: data,
	}
}

// ProcessMessage calculates the impact of a certain command on a region.
func (r *Region) ProcessMessage(sandengine.Command) ([]sandengine.Effect, error) {
	return nil, nil
}

// Data gets the raw proto data for the region.
func (r *Region) Data() *pb.Region {
	return r.data
}

// Save doesn't do anything for mocks.
func (r *Region) Save() error {
	return nil
}
