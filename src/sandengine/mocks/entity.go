package mocks

import (
	"github.com/stretchr/objx"
	pb "sandengine/proto"
	"sandengine/utils"
)

type Entity struct {
	EntityID string
	Loc      *utils.Location
}

func NewEntity(id string) *Entity {
	return &Entity{
		EntityID: id,
		Loc:      &utils.Location{},
	}
}

func (e *Entity) Proto() *pb.Entity {
	pbLoc := pb.Location(*e.Loc)
	return &pb.Entity{
		Id:       e.EntityID,
		Location: &pbLoc,
	}
}

func (e *Entity) EntityType() string              { return "" }
func (e *Entity) ID() string                      { return e.EntityID }
func (e *Entity) P() objx.Map                     { return objx.Map{} }
func (e *Entity) Pr() objx.Map                    { return objx.Map{} }
func (e *Entity) Location() *utils.Location       { return e.Loc }
func (e *Entity) SetLocation(loc *utils.Location) { e.Loc = loc }
