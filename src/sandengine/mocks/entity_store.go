package mocks

import (
	"sandengine"

	pb "sandengine/proto"
)

type entityStore struct {
	err    error
	entity sandengine.Entity
}

// NewEntityStore constructs a fake EntityStore that either always errors,
// or always returns a specified entity.
func NewEntityStore(err error, e sandengine.Entity) sandengine.EntityStore {
	return &entityStore{
		err:    err,
		entity: e,
	}
}

// NewErrEntityStore is a shorthand for a EntityStore that always errors.
func NewErrEntityStore() sandengine.EntityStore {
	return NewEntityStore(ErrFake, nil)
}

// FindEntity gets an entity for a given Entity ID.
func (es *entityStore) FindEntity(string) (sandengine.Entity, error) {
	return es.entity, es.err
}

// FindEntitiesForRegion gets all the entities in a region.
func (es *entityStore) FindEntitiesForRegion(*pb.Region) ([]sandengine.Entity, error) {
	if es.entity == nil {
		return nil, es.err
	}
	return []sandengine.Entity{es.entity}, es.err
}

// CreateEntities inserts entities into the database.
func (es *entityStore) CreateEntities(...sandengine.Entity) error {
	return es.err
}

// SaveEntities updates entities in the database.
func (es *entityStore) SaveEntities(...sandengine.Entity) error {
	return es.err
}

// RemoveEntities removes entities from the database.
func (es *entityStore) RemoveEntities(...string) error {
	return es.err
}

// LoadStatic doesn't do anything, because this is a mock.
func (es *entityStore) LoadStatic(string) error {
	return es.err
}
