package mocks

import (
	"sandengine"
)

type regionStore struct {
	err    error
	region *Region
}

// NewRegionStore constructs a fake RegionStore that either always errors,
// or always returns a specified region.
func NewRegionStore(err error, r *Region) sandengine.RegionStore {
	return &regionStore{
		err:    err,
		region: r,
	}
}

// NewErrRegionStore is a shorthand for a RegionStore that always errors.
func NewErrRegionStore() sandengine.RegionStore {
	return NewRegionStore(ErrFake, nil)
}

// Send doesn't do a whole lot.
func (rs *regionStore) Send(name string, msg sandengine.Command) ([]sandengine.Effect, error) {
	if rs.err != nil {
		return nil, rs.err
	}
	if rs.region != nil {
		return rs.region.ProcessMessage(msg)
	}
	return nil, nil
}

// Get gets a requested region.
func (rs *regionStore) Get(name string) (sandengine.Region, bool) {
	return rs.region, rs.region != nil
}

func (rs *regionStore) Sub(string, sandengine.Events)   {}
func (rs *regionStore) Unsub(string, sandengine.Events) {}

// Add adds a new region to the store.
func (rs *regionStore) Add(r sandengine.Region) {}

// Start starts the regionStore's internal processing.
func (rs *regionStore) Start() error {
	return rs.err
}

// Close kills the regionStore's internal processing.
func (rs *regionStore) Close() error {
	return rs.err
}
