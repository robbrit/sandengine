package sandengine

import (
	"sandengine/utils"
)

// A PhysicsManager controls the physics of a region.
type PhysicsManager interface {
	// Collide determines what happens when two entities collide at a particular
	// location.
	Collide(Entity, Entity, *utils.Location) []Effect
	// Act determines what happens when an entity acts on another set of entities.
	Act(Entity, []Entity) []Effect
	// ActEmpty determines the effect of an entity acting on an empty space.
	ActEmpty(Entity, *utils.Location) []Effect
}
