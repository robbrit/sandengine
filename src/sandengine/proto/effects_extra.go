package sandengine

// This file adds some additional info to certain protos so that they match
// the sandengine.Effects interface.

func (*PlaySound) IsBroadcast() bool { return true }
func (*PlaySound) IsExit() bool      { return false }

func (*RemoveEntity) IsBroadcast() bool { return true }
func (*RemoveEntity) IsExit() bool      { return false }

func (*UpdateEntity) IsBroadcast() bool { return true }
func (*UpdateEntity) IsExit() bool      { return false }

func (*ChangeRegion) IsBroadcast() bool { return false }
func (*ChangeRegion) IsExit() bool      { return true }

func (*EntityList) IsBroadcast() bool { return false }
func (*EntityList) IsExit() bool      { return false }

func (*CreateEntity) IsBroadcast() bool { return false }
func (*CreateEntity) IsExit() bool      { return false }

func (*UpdatePrivate) IsBroadcast() bool { return false }
func (*UpdatePrivate) IsExit() bool      { return false }
