package sandengine

import (
	"log"
	"time"

	"github.com/tuxychandru/pubsub"
	pb "sandengine/proto"
)

const (
	// What buffer size do we use for each region's communication channel?
	RegionCommBuffer = 1000
	// What is the default save frequency for regions?
	DefaultSaveFrequency = 10 * time.Second
)

// Region represents a distinct region within the game world.
type Region interface {
	// ProcessMessage calculates the impact of a certain command on a region.
	ProcessMessage(Command) ([]Effect, error)
	// Data gets the raw proto data for the region.
	Data() *pb.Region
	// Save persists the region to disk.
	Save() error
}

// RegionStore provides an interface for storing and communicating with the
// Regions in the game world.
type RegionStore interface {
	// Send manages sending synchronized messages to a region.
	Send(name string, msg Command) ([]Effect, error)
	// Get gets a requested region.
	Get(name string) (Region, bool)
	// Sub subscribes to a region.
	Sub(name string, events Events)
	// Unsub subscribes to a region.
	Unsub(name string, events Events)
	// Add adds a new region to the store.
	Add(r Region)
	// Start starts the RegionStore's internal processing.
	Start() error
	// Close kills the RegionStore's internal processing.
	Close() error
}

// RegionStoreOptions gives a list of options for a region store.
type RegionStoreOptions struct {
	SaveFrequency  time.Duration
	PubsubCapacity int
}

// effectPair is a "tuple" of []Effect and error.
type effectPair struct {
	Effects []Effect
	Error   error
}

// A regionCall is a way to "call" a function on a region.
type regionCall struct {
	Msg      Command
	Response chan effectPair
}

// regionStore is an implementation of RegionStore.
type regionStore struct {
	opts    RegionStoreOptions
	regions map[string]Region
	comms   map[string]chan regionCall
	// The exchange for pubsub messages.
	exchange *pubsub.PubSub
}

func (rs *regionStore) Send(name string, msg Command) ([]Effect, error) {
	// Always use a channel of length 1 so that we never block a region when it
	// sends us the response.
	response := make(chan effectPair, 1)
	rs.comms[name] <- regionCall{msg, response}
	effects := <-response
	// Partition by broadcast status.
	var outEffects, broadcastEffects []Effect
	for _, effect := range effects.Effects {
		if effect.IsBroadcast() {
			broadcastEffects = append(broadcastEffects, effect)
		} else {
			outEffects = append(outEffects, effect)
		}
	}
	// Send the broadcast effects out to everyone.
	if len(broadcastEffects) > 0 {
		go rs.exchange.Pub(broadcastEffects, name)
	}
	// Everything else just goes to our caller.
	return outEffects, effects.Error
}

func (rs *regionStore) Get(name string) (Region, bool) {
	r, ok := rs.regions[name]
	return r, ok
}

// Sub subscribes an events channel to a region.
func (rs *regionStore) Sub(name string, events Events) {
	rs.exchange.AddSub(events, name)
}

// Unsub unsubscribes an events channel from a region.
func (rs *regionStore) Unsub(name string, events Events) {
	rs.exchange.Unsub(events, name)
}

func (rs *regionStore) Add(r Region) {
	name := r.Data().Name
	rs.regions[name] = r
	rs.comms[name] = make(chan regionCall, RegionCommBuffer)
}

func (rs *regionStore) startRegion(name string, r Region) {
	for msg := range rs.comms[name] {
		effects, err := r.ProcessMessage(msg.Msg)
		if err != nil {
			msg.Response <- effectPair{Error: err}
			continue
		}

		msg.Response <- effectPair{Effects: effects}
	}
}

func (rs *regionStore) Start() error {
	for name, r := range rs.regions {
		go rs.startRegion(name, r)
	}

	// Also run a goroutine for saving regions.
	tick := time.Tick(rs.opts.SaveFrequency)
	go func() {
		// TODO: Rather than calling Save directly (which could have races), send
		// a message to each region for it to save itself.
		for range tick {
			for _, r := range rs.regions {
				if err := r.Save(); err != nil {
					log.Printf("error saving region %s: %s", r.Data().Name, err)
				}
			}
		}
	}()

	return nil
}

func (rs *regionStore) Close() error {
	for _, comm := range rs.comms {
		close(comm)
	}
	return nil
}

// NewRegionStore constructs a simple RegionStore object.
func NewRegionStore(opts RegionStoreOptions) RegionStore {
	if opts.SaveFrequency == 0 {
		opts.SaveFrequency = DefaultSaveFrequency
	}
	return &regionStore{
		opts:     opts,
		regions:  map[string]Region{},
		comms:    map[string]chan regionCall{},
		exchange: pubsub.New(opts.PubsubCapacity),
	}
}
