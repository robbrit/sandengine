package builtins

import (
	"fmt"
	"log"

	"sandengine"
	"sandengine/db"
)

const (
	// DefaultDB is the game database we use by default.
	DefaultDB = "sandbuilder:sandbuilder@/sandbuilder"
)

// NewGame constructs a new game.
func NewGame(opts sandengine.GameOptions) (sandengine.Game, error) {
	if opts.Database == "" {
		opts.Database = DefaultDB
	}
	if opts.EntityFactory == nil {
		return nil, fmt.Errorf("entity factory required for creating a game")
	}

	gameDB, err := db.New(opts.Database, opts.EntityFactory)
	if err != nil {
		return nil, err
	}

	return &game{
		regions:  sandengine.NewRegionStore(opts.RegionStoreOptions),
		tilesets: sandengine.NewTilesetStore(),
		entities: gameDB,
		players:  gameDB,
		physics:  opts.PhysicsManager,
		gameDB:   gameDB,
		ef:       opts.EntityFactory,
		workers:  opts.WorkerManager,
	}, nil
}

type game struct {
	regions  sandengine.RegionStore
	tilesets sandengine.TilesetStore
	entities sandengine.EntityStore
	players  sandengine.PlayerStore
	physics  sandengine.PhysicsManager
	ef       sandengine.EntityFactory
	gameDB   sandengine.GameDB
	workers  sandengine.WorkerManager
}

func (g *game) Start() {
	g.regions.Start()
	if err := g.workers.Start(g.regions); err != nil {
		log.Printf("error starting workers: %s", err)
	}
}

func (g *game) RegionStore() sandengine.RegionStore       { return g.regions }
func (g *game) PlayerStore() sandengine.PlayerStore       { return g.players }
func (g *game) EntityStore() sandengine.EntityStore       { return g.entities }
func (g *game) TilesetStore() sandengine.TilesetStore     { return g.tilesets }
func (g *game) EntityFactory() sandengine.EntityFactory   { return g.ef }
func (g *game) PhysicsManager() sandengine.PhysicsManager { return g.physics }
func (g *game) WorkerManager() sandengine.WorkerManager   { return g.workers }
