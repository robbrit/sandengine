package regions

import (
	"testing"

	"sandengine"
	"sandengine/builtins"
	"sandengine/mocks"
	pb "sandengine/proto"
	"sandengine/utils"
)

// TODO:
// - NewTiled()
// - ProcessMessage()
// - ProcessMove()
// - ProcessInteract()
// - ProcessRemove()
// - moving:
//   - off the edge of the world
//   - onto impassable tiles
//   - collisions
// - mapEntities
//   - stop
//   - remove
//   - create
//   - softremove
//   - other

func fakeTileset() *pb.Tileset {
	return &pb.Tileset{
		Groups: []*pb.TileGroup{
			{
				Subtypes: []*pb.TileSubtype{{0, true}, {1, false}},
			},
		},
	}
}

func fakeTileData(r *pb.Region) *pb.TileData {
	td := &pb.TileData{
		Tiles: []*pb.TileData_Tile{},
	}
	for x := int32(0); x < r.Width; x++ {
		for y := int32(0); y < r.Height; y++ {
			td.Tiles = append(td.Tiles, &pb.TileData_Tile{})
		}
	}
	return td
}

func TestEdgeCollision(t *testing.T) {
	for _, test := range []struct {
		desc         string
		direction    pb.Direction
		hasNeighbour bool
		wantX        int32
		wantY        int32
	}{
		{"up - has neighbour", pb.Direction_UP, true, 0, 2},
		{"up - has no neighbour", pb.Direction_UP, false, 0, 0},
		{"left - has neighbour", pb.Direction_LEFT, true, 2, 0},
		{"left - has no neighbour", pb.Direction_LEFT, false, 0, 0},
		{"right - has neighbour", pb.Direction_RIGHT, true, 0, 0},
		{"right - has no neighbour", pb.Direction_RIGHT, false, 0, 0},
		{"down - has neighbour", pb.Direction_DOWN, true, 0, 0},
		{"down - has no neighbour", pb.Direction_DOWN, false, 0, 0},
	} {
		// Set everything up.
		// First, have the region.
		r := &pb.Region{
			Name:       "start",
			Width:      1,
			Height:     1,
			Neighbours: &pb.Region_Neighbours{},
		}

		var rs sandengine.RegionStore
		if test.hasNeighbour {
			switch test.direction {
			case pb.Direction_UP:
				r.Neighbours.Up = "end"
			case pb.Direction_DOWN:
				r.Neighbours.Down = "end"
			case pb.Direction_LEFT:
				r.Neighbours.Left = "end"
			case pb.Direction_RIGHT:
				r.Neighbours.Right = "end"
			}
			rs = mocks.NewRegionStore(nil, mocks.NewRegion(&pb.Region{
				Name:   "end",
				Width:  3,
				Height: 3,
			}))
		} else {
			rs = mocks.NewRegionStore(nil, nil)
		}

		es := mocks.NewEntityStore(nil, nil)
		b, err := NewBasic(r, es)
		if err != nil {
			t.Errorf("%s: unexpected error on NewBasic(): %s", test.desc, err)
			continue
		}

		ts := mocks.NewTilesetStore(nil, fakeTileset())

		tiled := &TiledRegion{*b, fakeTileData(r), nil, rs, ts, &mocks.EntityFactory{}}

		// Next, the entity that we're moving.
		ent := builtins.NewEntityBase("1", "testEnt", &utils.Location{
			X:      0,
			Y:      0,
			Region: r.Name,
		}, nil, nil)
		tiled.entities["1"] = ent

		// Attempt the move.
		effects, err := tiled.mapEffects(tiled.effectsForMove(ent, test.direction))
		if err != nil {
			t.Errorf("%s: unexpected error on mapEffects: %s", test.desc, err)
			continue
		}

		if test.hasNeighbour {
			// We want 2 effects: Remove and ChangeRegion.
			if len(effects) != 2 {
				t.Errorf("%s: got effects = %s, want just 2", test.desc, effects)
				continue
			}
		} else {
			// We want 1 effect: Update.
			if len(effects) != 1 {
				t.Errorf("%s: got effects = %s, want just 1", test.desc, effects)
				continue
			}
		}

		wantRegion := "start"
		if test.hasNeighbour {
			wantRegion = "end"
		}

		if ent.Location().X != test.wantX {
			t.Errorf("%s: got X %d, want %d", test.desc, ent.Location().X, test.wantX)
		}
		if ent.Location().Y != test.wantY {
			t.Errorf("%s: got Y %d, want %d", test.desc, ent.Location().Y, test.wantY)
		}
		if ent.Location().Region != wantRegion {
			t.Errorf("%s: got Region %s, want %s", test.desc, ent.Location().Region, wantRegion)
		}

		entp := ent.Proto()
		if entp.Location.X != test.wantX {
			t.Errorf("%s: got proto X %d, want %d", test.desc, entp.Location.X, test.wantX)
		}
		if entp.Location.Y != test.wantY {
			t.Errorf("%s: got proto Y %d, want %d", test.desc, entp.Location.Y, test.wantY)
		}
		if entp.Location.Region != wantRegion {
			t.Errorf("%s: got proto Region %s, want %s", test.desc, entp.Location.Region, wantRegion)
		}
		if ent.Location().Orientation != test.direction {
			t.Errorf("%s: got direction %s, want %s", test.desc, ent.Location().Orientation, test.direction)
		}
	}
}

/*func TestRegularCollision(t *testing.T) {
	for _, test := range []struct {
		desc string
	}{
		{"solid thing there"},
		{"no solid thing there"},
	} {
		t.Errorf("%s: no test written", test.desc)
	}
}*/

/*func TestTerrainCollision(t *testing.T) {
	for _, test := range []struct {
		desc string
	}{
		{"impassable tile there"},
	} {
		t.Errorf("%s: no test written", test.desc)
	}
}*/

func TestFreeMove(t *testing.T) {
	for _, test := range []struct {
		desc      string
		direction pb.Direction
		wantX     int32
		wantY     int32
	}{
		{"up", pb.Direction_UP, 4, 3},
		{"down", pb.Direction_DOWN, 4, 5},
		{"left", pb.Direction_LEFT, 3, 4},
		{"right", pb.Direction_RIGHT, 5, 4},
	} {
		// Set everything up.
		// First, have the region.
		r := &pb.Region{
			Name:       "start",
			Width:      7,
			Height:     7,
			Neighbours: &pb.Region_Neighbours{},
		}

		rs := mocks.NewRegionStore(nil, nil)
		es := mocks.NewEntityStore(nil, nil)

		b, err := NewBasic(r, es)
		if err != nil {
			t.Errorf("%s: unexpected error on NewBasic(): %s", test.desc, err)
			continue
		}

		ts := mocks.NewTilesetStore(nil, fakeTileset())
		tiled := &TiledRegion{*b, fakeTileData(r), nil, rs, ts, &mocks.EntityFactory{}}

		// Next, the entity that we're moving.
		ent := builtins.NewEntityBase("1", "testEnt", &utils.Location{
			X:      4,
			Y:      4,
			Region: r.Name,
		}, nil, nil)

		// Attempt the move.
		effects := tiled.effectsForMove(ent, test.direction)

		// We want 1 effect: Update.
		if len(effects) != 1 {
			t.Errorf("%s: got effects = %s, want just 1", test.desc, effects)
			continue
		}

		if ent.Location().X != test.wantX {
			t.Errorf("%s: got X %d, want %d", test.desc, ent.Location().X, test.wantX)
		}
		if ent.Location().Y != test.wantY {
			t.Errorf("%s: got Y %d, want %d", test.desc, ent.Location().Y, test.wantY)
		}
		if ent.Location().Region != "start" {
			t.Errorf("%s: got Region %s, want start", test.desc, ent.Location().Region)
		}
		if ent.Location().Orientation != test.direction {
			t.Errorf("%s: got direction %s, want %s", test.desc, ent.Location().Orientation, test.direction)
		}
	}
}
