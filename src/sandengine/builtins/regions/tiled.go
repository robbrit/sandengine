package regions

import (
	"fmt"
	"io/ioutil"
	"log"

	"github.com/stretchr/objx"
	"sandengine"
	"sandengine/builtins"
	pb "sandengine/proto"
	"sandengine/utils"
)

type TiledRegion struct {
	BasicRegion
	TileData      *pb.TileData
	physics       sandengine.PhysicsManager
	regions       sandengine.RegionStore
	tilesetStore  sandengine.TilesetStore
	entityFactory sandengine.EntityFactory
}

// NewTiled creates a new tiled region.
func NewTiled(t sandengine.TilesetStore, es sandengine.EntityStore, ph sandengine.PhysicsManager,
	rs sandengine.RegionStore, ef sandengine.EntityFactory, path string) (*TiledRegion, error) {

	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("I/O: %s", err)
	}

	var r pb.Region
	if err = utils.ParseProto(string(data), &r); err != nil {
		return nil, fmt.Errorf("proto: %s", err)
	}

	// Extract the tile info from the region and save it properly so we don't need to do typechecks all the time.
	ts := r.GetTiles()
	if ts == nil {
		return nil, fmt.Errorf("no tiles found: %s", path)
	}

	// Verify that the tileset we want exists.
	if t.Get(ts.Tileset) == nil {
		return nil, fmt.Errorf("no such tileset '%s'", ts.Tileset)
	}

	b, err := NewBasic(&r, es)
	if err != nil {
		return nil, err
	}

	return &TiledRegion{*b, ts, ph, rs, t, ef}, nil
}

// ProcessMessage takes a command and tries to complete it.
func (r *TiledRegion) ProcessMessage(cmd sandengine.Command) ([]sandengine.Effect, error) {
	switch cmdData := cmd.(type) {
	case *sandengine.MoveCmd:
		return r.ProcessMove(cmdData)
	case *sandengine.InteractCmd:
		return r.ProcessInteract(cmdData)
	default:
		return r.BasicRegion.ProcessMessage(cmd)
	}
}

// ProcessMove handles an attempt to move.
func (t *TiledRegion) ProcessMove(cmd *sandengine.MoveCmd) ([]sandengine.Effect, error) {
	ent, ok := t.entities[cmd.EntityID]
	if !ok {
		log.Printf("unknown entity %s", cmd.EntityID)
		return nil, fmt.Errorf("unknown entity %s", cmd.EntityID)
	}

	// See what happens when we try to move there.
	return t.mapEffects(t.effectsForMove(ent, cmd.Direction))
}

// ProcessInteract handles an attempt to interact.
func (t *TiledRegion) ProcessInteract(cmd *sandengine.InteractCmd) ([]sandengine.Effect, error) {
	ent, ok := t.entities[cmd.EntityID]
	if !ok {
		log.Printf("unknown entity %s", cmd.EntityID)
		return nil, fmt.Errorf("unknown entity %s", cmd.EntityID)
	}

	// Check to see if there are any entities in front of me.
	loc := utils.Move(ent.Location(), ent.Location().Orientation)
	entsAt := t.entitiesAt(loc)

	if len(entsAt) > 0 {
		return t.mapEffects(t.physics.Act(ent, entsAt))
	}
	return t.mapEffects(t.physics.ActEmpty(ent, loc))
}

// ProcessRemove takes a RemoveEntityCmd and processes it.
func (t *TiledRegion) ProcessRemove(cmd *sandengine.RemoveCmd) ([]sandengine.Effect, error) {
	if _, ok := t.entities[cmd.EntityID]; !ok {
		return nil, fmt.Errorf("unknown entity %s", cmd.EntityID)
	}

	delete(t.entities, cmd.EntityID)

	return []sandengine.Effect{&pb.RemoveEntity{EntityId: cmd.EntityID}}, nil
}

// effectsForMove checks to see what happens when an entity tries to move in a
// certain direction.
func (t *TiledRegion) effectsForMove(ent sandengine.Entity, d pb.Direction) []sandengine.Effect {
	newLoc := utils.Move(ent.Location(), d)

	// Check to see if we've hit a transition on the edge of the map.
	if effects := t.checkTransition(ent, newLoc, d); effects != nil {
		return effects
	}

	// Don't move off the edge of the world.
	if !utils.InBounds(newLoc, t.Data()) {
		// Can't move, just return nothing.
		return []sandengine.Effect{&builtins.Stop{ent.ID(), d}}
	}

	// Don't move into impassable tiles.
	if !t.isPassable(newLoc, ent) {
		return []sandengine.Effect{&builtins.Stop{ent.ID(), d}}
	}

	// Handle collisions.
	var effects []sandengine.Effect
	for _, ent2 := range t.entitiesAt(newLoc) {
		effects = append(effects, t.physics.Collide(ent, ent2, newLoc)...)
		if len(effects) > 1 {
			log.Println(ent)
		}
	}
	if effects != nil {
		return effects
	}

	// No effects so far? Then just move.
	ent.SetLocation(newLoc)
	return []sandengine.Effect{&pb.UpdateEntity{Entity: ent.Proto()}}
}

// checkTransition checks to see if we are moving onto the edge of the region
// and should transition to a different one.
func (t *TiledRegion) checkTransition(ent sandengine.Entity, loc *utils.Location, d pb.Direction) []sandengine.Effect {
	data := t.Data()
	neighbours := data.GetNeighbours()
	if neighbours == nil {
		// No neighbours? This is an invalid region, log it.
		log.Printf("error: region %s has nil neighbours", t.Data().Name)
		return []sandengine.Effect{&builtins.Stop{ent.ID(), d}}
	}

	// Update the entity if we need to transition somewhere else.
	var newLoc *utils.Location

	if loc.X < 0 {
		if neighbours.Left == "" {
			return []sandengine.Effect{&builtins.Stop{ent.ID(), d}}
		}
		target, ok := t.regions.Get(neighbours.Left)
		if !ok {
			// This should never happen since region configuration is checked at
			// startup.
			log.Printf("error: region '%s' left of %s does not exist", neighbours.Left, data.Name)
			return nil
		}
		targetp := target.Data()
		// We need to transition left.
		newLoc = &utils.Location{
			X:           targetp.Width - 1,
			Y:           loc.Y,
			Region:      neighbours.Left,
			Orientation: pb.Direction_LEFT,
		}
	}

	if loc.X >= data.Width {
		if neighbours.Right == "" {
			return []sandengine.Effect{&builtins.Stop{ent.ID(), d}}
		}
		// We need to transition right.
		newLoc = &utils.Location{
			X:           0,
			Y:           loc.Y,
			Region:      neighbours.Right,
			Orientation: pb.Direction_RIGHT,
		}
	}

	if loc.Y < 0 {
		if neighbours.Up == "" {
			return []sandengine.Effect{&builtins.Stop{ent.ID(), d}}
		}
		target, ok := t.regions.Get(neighbours.Up)
		if !ok {
			// This should never happen since region configuration is checked at
			// startup.
			log.Printf("error: region '%s' up from %s does not exist", neighbours.Up, data.Name)
			return nil
		}
		targetp := target.Data()
		// We need to transition up.
		newLoc = &utils.Location{
			X:           loc.X,
			Y:           targetp.Height - 1,
			Region:      neighbours.Up,
			Orientation: pb.Direction_UP,
		}
	}

	if loc.Y >= data.Height {
		if neighbours.Down == "" {
			return []sandengine.Effect{&builtins.Stop{ent.ID(), d}}
		}
		// We need to transition down.
		newLoc = &utils.Location{
			X:           loc.X,
			Y:           0,
			Region:      neighbours.Down,
			Orientation: pb.Direction_DOWN,
		}
	}

	if newLoc != nil {
		// Remove us from this region, and signal to the outside to add us to a
		// different one.
		ent.SetLocation(newLoc)
		t.entityStore.SaveEntities(ent)
		return []sandengine.Effect{
			// Remove the entity.
			&builtins.SoftRemove{EntityID: ent.ID()},
			&pb.ChangeRegion{},
		}
	}

	return nil
}

// mapEffects checks two things: if there is any processing needed for each
// effect on the region, and if the effect should be sent back to whoever
// requested the action. Returns true if it should be returned to the caller.
func (t *TiledRegion) mapEffects(effects []sandengine.Effect) ([]sandengine.Effect, error) {
	var outEffects []sandengine.Effect

	for _, effect := range effects {
		switch effect := effect.(type) {
		case *builtins.Stop:
			// On a stop, update the entity anyway but keep them in the same place.
			ent, ok := t.entities[effect.EntityID]
			if !ok {
				log.Printf("region %s: attempted to stop non-existent entity %s", t.Data().Name, effect.EntityID)
				continue
			}
			loc := ent.Location()
			loc.Orientation = effect.Direction
			ent.SetLocation(loc)
			outEffects = append(outEffects, &pb.UpdateEntity{Entity: ent.Proto()})
		case *pb.RemoveEntity:
			// Remove this entity from the region, update the DB.
			delete(t.entities, effect.EntityId)
			if err := t.entityStore.RemoveEntities(effect.EntityId); err != nil {
				log.Printf("region %s: error removing entity %s: %s", t.Data().Name, effect.EntityId, err)
			}
			outEffects = append(outEffects, effect)
		case *builtins.SoftRemove:
			// Convert it to a regular remove so the client handles things properly.
			outEffects = append(outEffects, &pb.RemoveEntity{EntityId: effect.EntityID})
		case *pb.CreateEntity:
			// Create a new entity with the given arguments.
			props, err := objx.FromJSON(effect.Properties)
			if err != nil {
				log.Printf("region %s: invalid properties string '%s' on create %s", t.Data().Name, effect.Properties, effect.EntityType)
				continue
			}
			privs, err := objx.FromJSON(effect.Privates)
			if err != nil {
				log.Printf("region %s: invalid privates string '%s' on create %s", t.Data().Name, effect.Privates, effect.EntityType)
				continue
			}
			ent := t.entityFactory.Create(effect.EntityType, utils.ToLoc(effect.Location), props, privs)
			if err := t.entityStore.CreateEntities(ent); err != nil {
				log.Printf("region %s: error creating entity %v: %s", t.Data().Name, ent, err)
				continue
			}
			t.entities[ent.ID()] = ent
			// Convert the create to an update - everybody else should be handling
			// updates as "upserts".
			outEffects = append(outEffects, &pb.UpdateEntity{Entity: ent.Proto()})
		default:
			// Nothing to do, so just pass it through.
			outEffects = append(outEffects, effect)
		}
	}
	return outEffects, nil
}

// isPassable checks to see if a particular location is passable for a given
// entity.
func (t *TiledRegion) isPassable(loc *utils.Location, ent sandengine.Entity) bool {
	tile := t.tileAt(loc)
	ts := t.tilesetStore.Get(t.TileData.Tileset)

	return ts.Groups[tile.GroupNum].Subtypes[tile.SubtypeNum].Passable
}

// tileAt gets the tile at a requested location.
func (t *TiledRegion) tileAt(loc *utils.Location) *pb.TileData_Tile {
	if !utils.InBounds(loc, t.Data()) {
		return nil
	}

	d := t.Data()
	return t.TileData.Tiles[loc.X+loc.Y*d.Width]
}
