package regions

import (
	"errors"
	"fmt"
	"reflect"
	"sort"
	"testing"

	"github.com/stretchr/objx"
	"sandengine"
	"sandengine/builtins"
	"sandengine/mocks"
	pb "sandengine/proto"
	"sandengine/utils"
)

// TODO:
// - Save()

func TestCreate(t *testing.T) {
	r := &pb.Region{Name: "2"}
	loc := &utils.Location{X: 1, Y: 10, Region: r.Name}
	ent := builtins.NewEntityBase("", "", loc, objx.Map{}, objx.Map{})

	for _, test := range []struct {
		desc    string
		err     string
		wantErr bool
	}{
		{"error case", "blah blah", true},
		{"regular case", "", false},
	} {
		var err error
		if test.err != "" {
			err = errors.New(test.err)
		}
		b, err := NewBasic(r, mocks.NewEntityStore(err, ent))

		if test.wantErr {
			if err == nil {
				t.Errorf("%s: got nil error, want one", test.desc)
			}
			continue
		}

		if err != nil {
			t.Errorf("%s: got error %s, want none", test.desc, err)
			continue
		}

		// Verify that the entity we added is there.
		gotEnts := b.entitiesAt(loc)
		wantEnts := []sandengine.Entity{ent}
		if !reflect.DeepEqual(gotEnts, wantEnts) {
			t.Errorf("%s: got entities %s at %s, want %s", test.desc, gotEnts, loc, wantEnts)
		}
	}
}

func TestProcessJoin(t *testing.T) {
	r := &pb.Region{Name: "2"}
	loc := &utils.Location{X: 1, Y: 10, Region: r.Name}
	ent := builtins.NewEntityBase("", "", loc, objx.Map{}, objx.Map{})

	for _, test := range []struct {
		desc    string
		err     string
		exists  bool
		wantErr bool
	}{
		{"good case", "", false, false},
		{"error in find", "omg", false, true},
		{"entity already in region", "", true, false},
	} {
		var err error
		if test.err != "" {
			err = errors.New(test.err)
		}
		b := &BasicRegion{r, map[string]sandengine.Entity{}, mocks.NewEntityStore(err, ent)}
		if test.exists {
			b.entities[ent.ID()] = ent
		}

		got, err := b.ProcessJoin(&sandengine.JoinCmd{
			Player: &pb.Player{EntityId: ent.ID()},
		})

		if test.wantErr {
			if err == nil {
				t.Errorf("%s: got nil error, want one", test.desc)
			}
			continue
		}
		if err != nil {
			t.Errorf("%s: got error %s, want none", test.desc, err)
			continue
		}

		// Want an UpdateEntity and an EntityList.
		var gotTypes []string
		for _, effect := range got {
			gotTypes = append(gotTypes, fmt.Sprintf("%T", effect))
		}
		sort.Strings(gotTypes)
		wantTypes := []string{"*sandengine.EntityList", "*sandengine.UpdateEntity"}
		if !reflect.DeepEqual(gotTypes, wantTypes) {
			t.Errorf("%s: got effect types %v, want types %s", test.desc, gotTypes, wantTypes)
		}

		// Verify that the entity we added is there.
		gotEnt, ok := b.entities[ent.ID()]
		if !ok {
			t.Errorf("%s: got no entity %s, want one", test.desc, ent.ID())
			continue
		}
		if gotEnt != ent {
			t.Errorf("%s: got entity %v, want %v", test.desc, gotEnt, ent)
		}
	}
}
