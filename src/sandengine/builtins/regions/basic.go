package regions

import (
	"fmt"

	"sandengine"
	pb "sandengine/proto"
	"sandengine/utils"
)

type BasicRegion struct {
	proto       *pb.Region                   // Static details of the region.
	entities    map[string]sandengine.Entity // List of entities in the region.
	entityStore sandengine.EntityStore       // Where all the entities are stored.
}

// NewBasic creates a new basic region.
func NewBasic(r *pb.Region, es sandengine.EntityStore) (*BasicRegion, error) {
	b := &BasicRegion{
		proto:       r,
		entities:    map[string]sandengine.Entity{},
		entityStore: es,
	}

	// Grab all the entities that are in this region.
	ents, err := es.FindEntitiesForRegion(r)
	if err != nil {
		return nil, fmt.Errorf("findEntities: %s", err)
	}
	for _, ent := range ents {
		// Don't place any players, those only get added when the player logs in.
		// TODO: Come up with a less hard-coded way of doing this.
		if ent.EntityType() != "player" {
			b.entities[ent.ID()] = ent
		}
	}

	return b, nil
}

// Data gets the proto for the region.
func (b *BasicRegion) Data() *pb.Region {
	return b.proto
}

// Save saves the region to the database.
func (b *BasicRegion) Save() error {
	var ents []sandengine.Entity
	for _, ent := range b.entities {
		if !ent.Pr().Get("static").Bool(false) {
			ents = append(ents, ent)
		}
	}
	if ents == nil {
		return nil
	}
	return b.entityStore.SaveEntities(ents...)
}

// ProcessJoin takes a JoinCmd and processes the effects of it.
func (b *BasicRegion) ProcessJoin(cmd *sandengine.JoinCmd) ([]sandengine.Effect, error) {
	newEnt, ok := b.entities[cmd.Player.EntityId]
	if !ok {
		// Entity isn't in the region, add it.
		var err error
		if newEnt, err = b.entityStore.FindEntity(cmd.Player.EntityId); err != nil {
			return nil, err
		}
		b.entities[newEnt.ID()] = newEnt
	}

	// The EntityList is for the client joining - tell them who is already here.
	list := &pb.EntityList{}
	for _, ent := range b.entities {
		list.Entities = append(list.Entities, ent.Proto())
	}
	// The update is for everyone else - tell them who is joining.
	update := &pb.UpdateEntity{Entity: newEnt.Proto()}
	updatePrivs := &pb.UpdatePrivate{
		EntityId: newEnt.ID(),
		Privates: newEnt.Pr().MustJSON(),
	}
	return []sandengine.Effect{list, update, updatePrivs}, nil
}

// ListEntities gets all the entities in the region.
func (b *BasicRegion) ListEntities(cmd *sandengine.ListEntities) []sandengine.Effect {
	list := &pb.EntityList{}
	for _, ent := range b.entities {
		pr := ent.Proto()
		if cmd.WithPrivate {
			pr.Privates = ent.Pr().MustJSON()
		}
		list.Entities = append(list.Entities, pr)
	}
	return []sandengine.Effect{list}
}

// entitiesAt gets the entity at a location, if any.
func (b *BasicRegion) entitiesAt(loc *utils.Location) []sandengine.Entity {
	// TODO: should probably have a better data structure for location querying.
	var ents []sandengine.Entity
	for _, ent := range b.entities {
		if utils.LocEqual(ent.Location(), loc) {
			ents = append(ents, ent)
		}
	}
	return ents
}

// MakeNoise makes noise.
func (b *BasicRegion) MakeNoise(cmd *sandengine.MakeNoise) []sandengine.Effect {
	return []sandengine.Effect{
		&pb.PlaySound{
			Sound:    cmd.Name,
			Location: (*pb.Location)(cmd.Location),
		},
	}
}

// ProcessMessage takes a command and tries to complete it.
func (b *BasicRegion) ProcessMessage(cmd sandengine.Command) ([]sandengine.Effect, error) {
	switch cmdData := cmd.(type) {
	case *sandengine.JoinCmd:
		return b.ProcessJoin(cmdData)
	case *sandengine.ListEntities:
		return b.ListEntities(cmdData), nil
	case *sandengine.MakeNoise:
		return b.MakeNoise(cmdData), nil
	default:
		return nil, fmt.Errorf("Unknown command type %T", cmd)
	}
}
