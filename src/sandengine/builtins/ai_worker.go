package builtins

import (
	"log"
	"time"

	"github.com/stretchr/objx"
	"sandengine"
	pb "sandengine/proto"
	"sandengine/utils"
)

const (
	delayTime = 5 * time.Millisecond
	// TODO: make buffer size configurable
	bufferSize = 10
)

type Behaviour func(sandengine.Entity, *pb.Region) []sandengine.Command

type behaviourInfo struct {
	Action   Behaviour
	Cooldown time.Duration
}

type entityInfo struct {
	Entity     sandengine.Entity
	LastUpdate time.Time
}

// An AIWorker controls all the AI entities in a set of regions.
type AIWorker struct {
	events     sandengine.Events
	store      sandengine.RegionStore
	regions    []string
	entities   map[string]entityInfo
	factory    sandengine.EntityFactory
	frequency  time.Duration
	behaviours map[string]behaviourInfo
}

func NewAIWorker(regions []string, ef sandengine.EntityFactory, frequency time.Duration) *AIWorker {
	return &AIWorker{
		events:     make(sandengine.Events, bufferSize),
		regions:    regions,
		entities:   map[string]entityInfo{},
		factory:    ef,
		frequency:  frequency,
		behaviours: map[string]behaviourInfo{},
	}
}

func (ai *AIWorker) RegisterBehaviour(entType string, cooldown time.Duration, behaviour Behaviour) {
	ai.behaviours[entType] = behaviourInfo{behaviour, cooldown}
}

func (ai *AIWorker) Start(rs sandengine.RegionStore) error {
	ai.store = rs

	// List all the entities, figure out which ones are AIs.
	for _, region := range ai.regions {
		effs, err := ai.store.Send(region, &sandengine.ListEntities{true})
		if err != nil {
			return err
		}

		for _, rawEffect := range effs {
			effect, ok := rawEffect.(*pb.EntityList)
			if !ok {
				continue
			}

			for _, ent := range effect.Entities {
				privs := objx.MustFromJSON(ent.Privates)
				// TODO: allow filtering on entities so that one worker isn't managing
				// all the workers.
				if !privs.Get("ai").Bool() {
					continue
				}

				props := objx.MustFromJSON(ent.Properties)
				loc := (*utils.Location)(ent.Location)
				ai.entities[ent.Id] = entityInfo{
					Entity: ai.factory.Build(ent.Id, ent.EntityType, loc, props, privs),
				}
			}
		}
	}

	// TODO: Start a timer, periodically make the AIs do things.
	ticker := time.NewTicker(ai.frequency)

	rs.Sub("global", ai.events)
	for _, region := range ai.regions {
		rs.Sub(region, ai.events)
	}

	// TODO: Subscribe to the region, and handle AI events.
	for {
		select {
		case <-ticker.C:
			ai.handleBehaviours()
		case rawMsg := <-ai.events:
			switch msg := rawMsg.(type) {
			case []sandengine.Effect:
				for _, effect := range msg {
					ai.processEffect(effect)
				}
			}
		default:
			time.Sleep(delayTime)
		}
	}

	return nil
}

func (ai *AIWorker) handleBehaviours() {
	for _, info := range ai.entities {
		now := time.Now()
		action, ok := ai.behaviours[info.Entity.EntityType()]
		if !ok {
			// No behaviour defined for this entity type.
			continue
		}
		if info.LastUpdate.Add(action.Cooldown).After(now) {
			// Too soon.
			continue
		}

		r, ok := ai.store.Get(info.Entity.Location().Region)
		if !ok {
			// This should never happen, but check anyway.
			log.Printf("Error: entity %s is in unknown region %s", info.Entity.ID(), info.Entity.Location().Region)
			continue
		}

		for _, cmd := range action.Action(info.Entity, r.Data()) {
			effects, err := ai.store.Send(info.Entity.Location().Region, cmd)
			if err != nil {
				log.Printf("Error triggering behaviour for %s (%s): %s", info.Entity.ID(), info.Entity.EntityType(), err)
				continue
			}
			for _, effect := range effects {
				ai.processEffect(effect)
			}
		}

		info.LastUpdate = now
	}
}

func (ai *AIWorker) processEffect(rawEffect sandengine.Effect) {
	switch effect := rawEffect.(type) {
	case *pb.UpdateEntity:
		// Is it an entity we are managing?
		ent, ok := ai.entities[effect.Entity.Id]
		if !ok {
			// TODO: We should probably care about other entities.
			return
		}

		ent.Entity.P().MergeHere(objx.MustFromJSON(effect.Entity.Properties))
		ent.Entity.SetLocation((*utils.Location)(effect.Entity.Location))
		if effect.Entity.Privates != "" {
			ent.Entity.Pr().MergeHere(objx.MustFromJSON(effect.Entity.Privates))
		}
	case *pb.UpdatePrivate:
		// Is it an entity we are managing?
		ent, ok := ai.entities[effect.EntityId]
		if !ok {
			// TODO: We should probably care about other entities.
			return
		}

		ent.Entity.Pr().MergeHere(objx.MustFromJSON(effect.Privates))
	default:
		// TODO: Handle CreateEntity.
		// TODO: Handle RemoveEntity.
		// TODO: Handle ChangeRegion.
	}
}

func (ai *AIWorker) Close() {
	for _, region := range ai.regions {
		ai.store.Unsub(region, ai.events)
	}
	// This should be the last topic, so the pubsub system will automatically
	// close the channel.
	ai.store.Unsub("global", ai.events)
}
