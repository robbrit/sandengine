package builtins

import (
	"testing"

	"sandengine"
	pb "sandengine/proto"
)

func TestIsBroadcast(t *testing.T) {
	for _, test := range []struct {
		desc          string
		effect        sandengine.Effect
		wantBroadcast bool
	}{
		{"good value", &pb.UpdateEntity{}, true},
		{"bad value", &pb.EntityList{}, false},
	} {
		if got := test.effect.IsBroadcast(); got != test.wantBroadcast {
			t.Errorf("%s: got %t, want %t", test.desc, got, test.wantBroadcast)
		}
	}
}

func TestIsExit(t *testing.T) {
	for _, test := range []struct {
		desc     string
		effect   sandengine.Effect
		wantExit bool
	}{
		{"good value", &pb.ChangeRegion{}, true},
		{"bad value", &pb.EntityList{}, false},
	} {
		if got := test.effect.IsExit(); got != test.wantExit {
			t.Errorf("%s: got %t, want %t", test.desc, got, test.wantExit)
		}
	}
}
