package builtins

import (
	"sandengine"
	"sandengine/utils"

	pb "sandengine/proto"
)

// A CollisionHandler defines the effects of a collision between two entities.
type CollisionHandler func(sandengine.Entity, sandengine.Entity, *utils.Location) []sandengine.Effect

// An ActionHandler defines the effects of an entity acting on a set of
// entities that are all on one space.
type ActionHandler func(sandengine.Entity, sandengine.Entity) []sandengine.Effect

// An EmptyActionHandler defines the effects of an entity acting on a space.
type EmptyActionHandler func(sandengine.Entity, *utils.Location) []sandengine.Effect

// A PhysicsManager controls the physics of a region.
type PhysicsManager struct {
	collisions       map[string]map[string]CollisionHandler
	defaultCollision CollisionHandler
	// TODO: do we need a default action handler?
	actions            map[string]map[string]ActionHandler
	emptyActions       map[string]EmptyActionHandler
	defaultEmptyAction EmptyActionHandler
}

// NewPhysicsManager constructs a new PhysicsManager object.
func NewPhysicsManager() *PhysicsManager {
	return &PhysicsManager{
		collisions:         map[string]map[string]CollisionHandler{},
		defaultCollision:   defaultCollision,
		actions:            map[string]map[string]ActionHandler{},
		emptyActions:       map[string]EmptyActionHandler{},
		defaultEmptyAction: defaultEmptyAction,
	}
}

// Collide determines what happens when two entities collide.
func (pm *PhysicsManager) Collide(e1, e2 sandengine.Entity, loc *utils.Location) []sandengine.Effect {
	t1 := e1.Proto().EntityType
	t2 := e2.Proto().EntityType

	if t1 > t2 {
		t1, t2 = t2, t1
		e1, e2 = e2, e1
	}

	row, ok := pm.collisions[t1]
	if !ok {
		return pm.defaultCollision(e1, e2, loc)
	}

	handler, ok := row[t2]
	if !ok {
		return pm.defaultCollision(e1, e2, loc)
	}

	return handler(e1, e2, loc)
}

// Act determines what happens when an entity acts on another entity.
func (pm *PhysicsManager) Act(actor sandengine.Entity, targets []sandengine.Entity) []sandengine.Effect {
	row, ok := pm.actions[actor.EntityType()]
	if !ok {
		return nil
	}

	var effects []sandengine.Effect
	for _, target := range targets {
		handler, ok := row[target.EntityType()]
		if !ok {
			continue
		}
		effects = append(effects, handler(actor, target)...)
	}
	return effects
}

// ActEmpty determines what happens when an entity acts on an empty space.
func (pm *PhysicsManager) ActEmpty(actor sandengine.Entity, target *utils.Location) []sandengine.Effect {
	handler, ok := pm.emptyActions[actor.EntityType()]
	if !ok {
		return pm.defaultEmptyAction(actor, target)
	}
	return handler(actor, target)
}

// SetDefault sets the default collision behaviour.
func (pm *PhysicsManager) SetDefault(handler CollisionHandler) {
	pm.defaultCollision = handler
}

// RegisterCollision sets the collision behaviour between two Entity types.
func (pm *PhysicsManager) RegisterCollision(t1, t2 string, handler CollisionHandler) {
	if t1 > t2 {
		t1, t2 = t2, t1
	}
	if _, ok := pm.collisions[t1]; !ok {
		pm.collisions[t1] = map[string]CollisionHandler{}
	}
	pm.collisions[t1][t2] = handler
}

// RegisterAction defines what happens when an entity acts on another entity.
func (pm *PhysicsManager) RegisterAction(e1, e2 string, handler ActionHandler) {
	if _, ok := pm.actions[e1]; !ok {
		pm.actions[e1] = map[string]ActionHandler{}
	}
	pm.actions[e1][e2] = handler
}

// RegisterEmptyAction defines what happens when an entity acts on an empty space.
func (pm *PhysicsManager) RegisterEmptyAction(ent string, handler EmptyActionHandler) {
	pm.emptyActions[ent] = handler
}

// defaultCollision is just what happens when two entities collide. If they are
// both solid then the effect is Stop, otherwise nothing happens.
func defaultCollision(e1, e2 sandengine.Entity, loc *utils.Location) []sandengine.Effect {
	if e1.Pr().Get("solid").Bool(false) && e2.Pr().Get("solid").Bool(false) {
		return []sandengine.Effect{&Stop{e1.ID(), e1.Location().Orientation}}
	}
	return nil
}

// defaultEmptyAction is what happens when an entity attempts to interact but
// the space in front of them is empty.
func defaultEmptyAction(ent sandengine.Entity, loc *utils.Location) []sandengine.Effect {
	var pbLoc pb.Location = pb.Location(*ent.Location())
	return []sandengine.Effect{
		&pb.PlaySound{
			Sound:    "swing",
			Location: &pbLoc,
		},
	}
}

// CombineActions combines multiple actions, merging all their effects together.
func CombineActions(actions ...ActionHandler) ActionHandler {
	return func(e1, e2 sandengine.Entity) []sandengine.Effect {
		var effects []sandengine.Effect
		for _, action := range actions {
			effects = append(effects, action(e1, e2)...)
		}
		return effects
	}
}
