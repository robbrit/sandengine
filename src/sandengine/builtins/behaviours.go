package builtins

import (
	"math/rand"

	"sandengine"
	pb "sandengine/proto"
	"sandengine/utils"
)

// NullBehaviour is just the entity doing nothing.
func NullBehaviour(sandengine.Entity) []sandengine.Command {
	return nil
}

// Wander just makes the entity attempt to wander around. It will not wander
// out of the region that it is in.
func Wander(probability float64) Behaviour {
	return func(ent sandengine.Entity, r *pb.Region) []sandengine.Command {
		// Choose a random direction.
		if rand.Float64() >= probability {
			return nil
		}

		var dir pb.Direction
		var good bool
		for i := 0; i < 10; i++ {
			dir = utils.RandomDirection()
			newLoc := utils.Move(ent.Location(), dir)
			// Don't move off the edge of the world.
			if utils.InBounds(newLoc, r) {
				good = true
				break
			}
			// TODO: Should also check for collisions.
			// TODO: Don't wander into portals.
		}
		if !good {
			// We couldn't find a direction to move in.
			return nil
		}
		return []sandengine.Command{
			&sandengine.MoveCmd{
				EntityID:  ent.ID(),
				Direction: dir,
			},
		}
	}
}

// A pre-defined behaviour that causes an entity to make a sound every now and
// then.
func Talk(sound string, probability float64) Behaviour {
	return func(ent sandengine.Entity, r *pb.Region) []sandengine.Command {
		if rand.Float64() >= probability {
			return nil
		}
		return []sandengine.Command{
			&sandengine.MakeNoise{
				Location: ent.Location(),
				Name:     sound,
			},
		}
	}
}

// Merge combines several behaviours.
func Merge(behaviours ...Behaviour) Behaviour {
	return func(ent sandengine.Entity, r *pb.Region) []sandengine.Command {
		var cmds []sandengine.Command
		for _, b := range behaviours {
			cmds = append(cmds, b(ent, r)...)
		}
		return cmds
	}
}
