package builtins

import (
	"testing"

	"github.com/stretchr/objx"
	"sandengine/utils"
)

func TestProto(t *testing.T) {
	loc := &utils.Location{X: 5, Y: 7, Region: "asdf"}
	eb := &EntityBase{
		loc:        loc,
		props:      objx.Map{},
		privates:   objx.Map{},
		entityType: "quack",
		id:         "sausage",
	}

	got := eb.Proto()

	gotLoc := utils.ToLoc(got.Location)
	if *gotLoc != *loc {
		t.Errorf("got loc %s, want %s", gotLoc, loc)
	}
	if got.EntityType != eb.entityType {
		t.Errorf("got type %s, want %s", got.EntityType, eb.entityType)
	}
	if got.Id != eb.id {
		t.Errorf("got id %s, want %s", got.Id, eb.id)
	}
}
