package builtins

import (
	"github.com/satori/go.uuid"
	"github.com/stretchr/objx"
	pb "sandengine/proto"
	"sandengine/utils"
)

// NewId gets a new ID for an entity.
func NewId() string {
	return uuid.NewV4().String()
}

// EntityBase is a basic implementation of the sandengine.Entity interface.
type EntityBase struct {
	loc        *utils.Location
	props      objx.Map
	privates   objx.Map
	entityType string
	id         string
}

// NewEntityBase creates a new EntityBase object.
func NewEntityBase(id, entityType string, loc *utils.Location, props objx.Map, privs objx.Map) *EntityBase {
	if props == nil {
		props = objx.Map{}
	}
	if privs == nil {
		privs = objx.Map{}
	}
	return &EntityBase{
		id:         id,
		loc:        loc,
		props:      props,
		privates:   privs,
		entityType: entityType,
	}
}

func (eb *EntityBase) Proto() *pb.Entity {
	return &pb.Entity{
		Id:         eb.id,
		EntityType: eb.entityType,
		Location:   eb.loc.ToPB(),
		Properties: eb.props.MustJSON(),
		// Don't include private properties, those can be added manually by the
		// caller.
	}
}

func (eb *EntityBase) P() objx.Map               { return eb.props }
func (eb *EntityBase) Pr() objx.Map              { return eb.privates }
func (eb *EntityBase) Location() *utils.Location { return eb.loc }
func (eb *EntityBase) ID() string                { return eb.id }
func (eb *EntityBase) EntityType() string        { return eb.entityType }

func (eb *EntityBase) SetLocation(loc *utils.Location) {
	eb.loc = loc
}
