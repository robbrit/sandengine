package builtins

import (
	"fmt"

	pb "sandengine/proto"
)

// Stop is an effect that stops an entity from moving.
type Stop struct {
	EntityID  string
	Direction pb.Direction
}

// Have Stop satisfy the sandengine.Effect interface.
func (s *Stop) Reset()            {}
func (s *Stop) String() string    { return fmt.Sprintf("Stop{%s, %d}", s.EntityID, s.Direction) }
func (s *Stop) ProtoMessage()     {}
func (s *Stop) IsBroadcast() bool { return false }
func (s *Stop) IsExit() bool      { return false }

// SoftRemove is an effect that removes an entity from a region but not from
// the database.
type SoftRemove struct {
	EntityID string
}

// Have SoftRemove satisfy the sandengine.effect interface.
func (s *SoftRemove) Reset()            {}
func (s *SoftRemove) String() string    { return fmt.Sprintf("SoftRemove{%s}", s.EntityID) }
func (s *SoftRemove) ProtoMessage()     {}
func (s *SoftRemove) IsBroadcast() bool { return false }
func (s *SoftRemove) IsExit() bool      { return false }
