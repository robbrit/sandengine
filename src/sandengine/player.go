package sandengine

import (
	pb "sandengine/proto"
)

// PlayerStore defines how various things will load Player data from a database.
type PlayerStore interface {
	// FindPlayer finds a player for a given player name.
	FindPlayer(string) (*pb.Player, error)
	// FindPlayersForUser finds all the players for a given username.
	FindPlayersForUser(string) ([]*pb.Player, error)
	// CreatePlayer creates a new player with a given name and username.
	CreatePlayer(string, string) (*pb.Player, error)
	// SavePlayer persists the player to the database.
	SavePlayer(*pb.Player) error
}
