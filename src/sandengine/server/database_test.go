package server

import (
	"testing"

	"sandengine/server/models"
)

func fakeDB(t *testing.T) *authDB {
	db, err := NewDB(":memory:")
	if err != nil {
		t.Fatalf("Failed to initialize test DB: %s", err)
	}
	db.SaveUser(models.NewUser("bob", "asdfasdf", "bob@sandbuilder.com"))
	return db
}

func TestUsernameExists(t *testing.T) {
	for _, test := range []struct {
		desc      string
		username  string
		wantExist bool
	}{
		{"good user", "bob", true},
		{"unknown user", "santa", false},
	} {
		db := fakeDB(t)
		got := db.UsernameExists(test.username)
		if got != test.wantExist {
			t.Errorf("%s: UsernameExists(%s): got %t, want %t", test.desc, test.username, got, test.wantExist)
		}
	}
}

func TestEmailExists(t *testing.T) {
	for _, test := range []struct {
		desc      string
		email     string
		wantExist bool
	}{
		{"good email", "bob@sandbuilder.com", true},
		{"unknown email", "santa@sandbuilder.com", false},
	} {
		db := fakeDB(t)
		got := db.EmailExists(test.email)
		if got != test.wantExist {
			t.Errorf("%s: EmailExists(%s): got %t, want %t", test.desc, test.email, got, test.wantExist)
		}
	}
}

func TestFetchUser(t *testing.T) {
	for _, test := range []struct {
		desc     string
		username string
		wantOK   bool
	}{
		{"good user", "bob", true},
		{"unknown user", "santa", false},
	} {
		db := fakeDB(t)
		got, gotOK := db.FetchUser(test.username)
		if gotOK != test.wantOK {
			t.Errorf("%s: FetchUser(%s): got %t, want %t", test.desc, test.username, gotOK, test.wantOK)
			continue
		}

		if !test.wantOK {
			continue
		}

		if got == nil {
			t.Errorf("%s: FetchUser(%s): got nil, want non-nil", test.desc, test.username)
			continue
		}
		if got.Username != test.username {
			t.Errorf("%s: FetchUser(%s): got username %s, want %s", test.desc, test.username, got.Username, test.username)
		}
	}
}

func TestAuthenticateUser(t *testing.T) {
	for _, test := range []struct {
		desc     string
		username string
		password string
		wantOK   bool
	}{
		{"good user", "bob", "asdfasdf", true},
		{"wrong password", "bob", "notrightpassword", false},
		{"unknown user", "santa", "asdfasdf", false},
	} {
		db := fakeDB(t)
		got, gotOK := db.AuthenticateUser(test.username, test.password)
		if gotOK != test.wantOK {
			t.Errorf("%s: AuthenticateUser(%s, %s): got %t, want %t", test.desc, test.username, test.password, gotOK, test.wantOK)
			continue
		}

		if !test.wantOK {
			continue
		}

		if got == nil {
			t.Errorf("%s: AuthenticateUser(%s, %s): got nil, want non-nil", test.desc, test.username, test.password)
			continue
		}
		if got.Username != test.username {
			t.Errorf("%s: AuthenticateUser(%s, %s): got username %s, want %s", test.desc, test.username, test.password, got.Username, test.username)
		}
	}
}
