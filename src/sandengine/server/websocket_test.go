package server

import (
	"testing"

	"github.com/stretchr/objx"
	"github.com/tuxychandru/pubsub"
	"sandengine"
	"sandengine/builtins"
	"sandengine/mocks"
	pb "sandengine/proto"
	"sandengine/utils"
)

func TestHandleJoin(t *testing.T) {
	ts := &pb.Tileset{Name: "forest"}
	rpb := &pb.Region{
		Name: "salad",
		Data: &pb.Region_Tiles{Tiles: &pb.TileData{Tileset: ts.Name}},
	}
	r := mocks.NewRegion(rpb)
	loc := &utils.Location{X: 1, Y: 10, Region: rpb.Name}
	ent := builtins.NewEntityBase("", "", loc, objx.Map{}, objx.Map{})
	goodPlayers := mocks.NewPlayerStore(nil, &pb.Player{Name: "jim", Username: "bob"})
	goodRegions := mocks.NewRegionStore(nil, r)
	goodTilesets := mocks.NewTilesetStore(nil, ts)
	goodEntities := mocks.NewEntityStore(nil, ent)

	for _, test := range []struct {
		desc          string
		username      string
		players       sandengine.PlayerStore
		regions       sandengine.RegionStore
		tilesets      sandengine.TilesetStore
		entities      sandengine.EntityStore
		wantClientErr bool
		wantErr       bool
	}{
		{"success", "bob", goodPlayers, goodRegions, goodTilesets, goodEntities, false, false},
		{"player load error", "bob", mocks.NewErrPlayerStore(), goodRegions, goodTilesets, goodEntities, false, true},
		{"nil player", "bob", mocks.NewPlayerStore(nil, nil), goodRegions, goodTilesets, goodEntities, true, false},
		{"not-owned player", "jill", goodPlayers, goodRegions, goodTilesets, goodEntities, true, false},
		{"entity load error", "bob", goodPlayers, goodRegions, goodTilesets, mocks.NewErrEntityStore(), false, true},
		{"nil entity", "bob", goodPlayers, goodRegions, goodTilesets, mocks.NewEntityStore(nil, nil), false, true},
		{"region load error", "bob", goodPlayers, mocks.NewErrRegionStore(), goodTilesets, goodEntities, false, true},
		{"tileset load error", "bob", goodPlayers, goodRegions, mocks.NewErrTilesetStore(), goodEntities, false, true},
		{"nil tileset", "bob", goodPlayers, goodRegions, mocks.NewTilesetStore(nil, nil), goodEntities, false, true},
	} {
		ss := &socketState{
			game: &mocks.Game{
				Regions:  test.regions,
				Players:  test.players,
				Tilesets: test.tilesets,
				Entities: test.entities,
			},
			exchange: pubsub.New(1),
		}
		u := &pb.User{Username: test.username}
		req := &pb.JoinGameRequest{
			PlayerName: "jim",
		}

		got, gotClientErr, gotErr := ss.handleJoinGame(req, u)
		if test.wantErr {
			if gotErr == nil {
				t.Errorf("%s: got nil error, want one", test.desc)
			}
			continue
		}
		if gotErr != nil {
			t.Errorf("%s: got unexpected error %s, want nil", test.desc, gotErr)
		}

		if test.wantClientErr {
			if gotClientErr == nil {
				t.Errorf("%s: got nil client error, want one", test.desc)
			}
			continue
		}
		if gotClientErr != nil {
			t.Errorf("%s: got unexpected client error %s, want nil", test.desc, gotClientErr)
		}

		if got.Region != rpb {
			t.Errorf("%s: got region %q, want %q", test.desc, got.Region, rpb)
		}
		if got.Tileset != ts {
			t.Errorf("%s: got tileset %q, want %q", test.desc, got.Tileset, ts)
		}
	}
}
