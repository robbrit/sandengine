package server

import (
	"net/http"
	"testing"

	"sandengine/server/mocks"
	"sandengine/server/models"

	pb "sandengine/proto"
)

func TestValidAuthHandler(t *testing.T) {
	for _, test := range []struct {
		desc    string
		authKey string
		wantOK  bool
	}{
		{"valid auth", mocks.GoodSessionKey, true},
		{"invalid auth", "something else", false},
		{"no auth", "", false},
	} {
		h := testHTTP()

		req := pb.SimpleRequest{}
		req.AuthKey = test.authKey

		var resp pb.ValidAuthResponse
		code := h.fakeRequest("GET", "/valid_auth", h.validAuthHandler, &req, &resp)

		if code != http.StatusOK {
			t.Errorf("%s: got code %d, want 200", test.desc, code)
		}

		if test.wantOK != resp.Ok {
			t.Errorf("%s: got OK = %t, want %t", test.desc, resp.Ok, test.wantOK)
		}
	}
}

func TestLoginHandler(t *testing.T) {
	db := mocks.NewDB()
	u := models.NewUser("taco", "passwd", "email@sandbuilder.com")
	db.SaveUser(u)

	for _, test := range []struct {
		desc        string
		username    string
		password    string
		wantSuccess bool
		wantStatus  int
	}{
		{"good credentials", "taco", "passwd", true, http.StatusOK},
		{"wrong credentials", "taco", "wat", false, http.StatusNotFound},
		{"unknown user", "else", "passwd", false, http.StatusNotFound},
	} {
		h := testHTTP()
		h.UserStore = db

		var req pb.AuthRequest
		req.Username = test.username
		req.Password = test.password

		var resp pb.AuthResponse
		code := h.fakeRequest("POST", "/login", h.loginHandler, &req, &resp)

		if code != test.wantStatus {
			t.Errorf("%s: got code %d, want %d", test.desc, code, test.wantStatus)
		}

		if test.wantSuccess {
			if len(resp.AuthKey) == 0 {
				t.Errorf("%s: got no auth key, expected one", test.desc)
			}
			if len(resp.Errors) > 0 {
				t.Errorf("%s: got errors %s, want none", test.desc, resp.Errors)
			}
		} else {
			if len(resp.AuthKey) != 0 {
				t.Errorf("%s: got auth key %s, want none", test.desc, resp.AuthKey)
			}
			if len(resp.Errors) == 0 {
				t.Errorf("%s: got no errors, want some", test.desc)
			}
		}
	}
}

func TestSignupHandler(t *testing.T) {
	u := models.NewUser("taco", "passwd", "email@sandbuilder.com")

	for _, test := range []struct {
		desc        string
		username    string
		email       string
		password    string
		wantSuccess bool
	}{
		{"good signup", "candle", "candle@sandbuilder.com", "asdfasdf", true},
		{"no email", "candle", "", "asdfasdf", false},
		{"no username", "", "candle@sandbuilder.com", "asdfasdf", false},
		{"no password", "candle", "candle@sandbuilder.com", "", false},
		{"invalid email", "candle", "notanemail", "asdfasdf", false},
		{"email exists", "candle", "email@sandbuilder.com", "asdfasdf", false},
		{"username exists", "taco", "candle@sandbuilder.com", "asdfasdf", false},
		{"password too short", "taco", "candle@sandbuilder.com", "asdf", false},
	} {
		db := mocks.NewDB()
		db.SaveUser(u)

		h := testHTTP()
		h.UserStore = db

		var req pb.SignupRequest
		req.Username = test.username
		req.Email = test.email
		req.Password = test.password

		var resp pb.SignupResponse
		code := h.fakeRequest("POST", "/signup", h.signupHandler, &req, &resp)

		if code != http.StatusOK {
			t.Errorf("%s: got code %d, want %d", test.desc, code, http.StatusOK)
		}

		if test.wantSuccess {
			if len(resp.Errors) > 0 {
				t.Errorf("%s: got errors %s, want none", test.desc, resp.Errors)
			}
		} else {
			if len(resp.Errors) == 0 {
				t.Errorf("%s: got no errors, want some", test.desc)
			}
		}
	}
}
