package models

import (
	"crypto/rand"
	"crypto/sha1"
	"encoding/base64"

	"github.com/golang/protobuf/proto"

	pb "sandengine/proto"
)

const hashSecret = "S4usages!"

type User struct {
	Proto    []byte
	Username string
	Email    string
}

// Unmarshal converts a User into a pb.User.
func (u *User) Unmarshal() (*pb.User, error) {
	var user pb.User
	if err := proto.Unmarshal(u.Proto, &user); err != nil {
		return nil, err
	}
	return &user, nil
}

// Marshal loads a pb.User into this User object.
func (u *User) Marshal(upb *pb.User) error {
	raw, err := proto.Marshal(upb)
	if err != nil {
		return err
	}
	u.Proto = raw
	u.Username = upb.Username
	u.Email = upb.Email
	return nil
}

// CheckPassword verifies that the provided password is correct.
func CheckPassword(u *pb.User, p string) bool {
	return hashPassword(u, p) == u.HashedPassword
}

// NewUser creates a new user from a username, password, and email.
func NewUser(username, password, email string) *pb.User {
	saltBytes := make([]byte, 30)

	rand.Read(saltBytes)

	user := &pb.User{
		Username: username,
		Email:    email,
		Salt:     base64.URLEncoding.EncodeToString(saltBytes),
	}

	user.HashedPassword = hashPassword(user, password)

	return user
}

func hashString(hash, salt string) string {
	bv := []byte(hash + hashSecret + salt)
	hasher := sha1.New()
	hasher.Write(bv)

	return base64.URLEncoding.EncodeToString(hasher.Sum(nil))
}

func hashPassword(u *pb.User, p string) string {
	return hashString(p, u.Salt)
}
