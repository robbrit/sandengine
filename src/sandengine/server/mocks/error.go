package mocks

import (
	"fmt"
)

var ErrFake = fmt.Errorf("fake error")
