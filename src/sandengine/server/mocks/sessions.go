package mocks

import (
	pb "sandengine/proto"
)

type SessionStore interface {
	FetchUser(string) (*pb.User, error)
	RegisterUser(*pb.User) string
	ClearKey(string)
}

var GoodSessionKey = "goodkey"

type mockSession struct {
	user *pb.User
}

func (m *mockSession) FetchUser(key string) (*pb.User, error) {
	if key == "error" {
		return nil, ErrFake
	}
	if key == GoodSessionKey {
		return m.user, nil
	}
	return nil, nil
}

func (m *mockSession) RegisterUser(user *pb.User) string {
	m.user = user
	return GoodSessionKey
}

func (m *mockSession) ClearKey(key string) {
	m.user = nil
}

func NewSession() *mockSession {
	return &mockSession{
		// Default to having a user.
		user: &pb.User{},
	}
}
