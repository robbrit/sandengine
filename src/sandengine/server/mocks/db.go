package mocks

import (
	"sandengine/server/models"

	pb "sandengine/proto"
)

type MockDB struct {
	users map[string]*pb.User
}

func NewDB() *MockDB {
	return &MockDB{
		users: map[string]*pb.User{},
	}
}

/////////////////////////////////////////
// Implementation of UserStore methods //
/////////////////////////////////////////

func (m *MockDB) UsernameExists(username string) bool {
	_, ok := m.users[username]
	return ok
}

func (m *MockDB) EmailExists(email string) bool {
	for _, u := range m.users {
		if u.Email == email {
			return true
		}
	}
	return false
}

func (m *MockDB) AuthenticateUser(username, password string) (*pb.User, bool) {
	u, ok := m.users[username]
	if !ok || !models.CheckPassword(u, password) {
		return nil, false
	}
	return u, true
}

func (m *MockDB) SaveUser(user *pb.User) error {
	m.users[user.Username] = user
	return nil
}

func (m *MockDB) FetchUser(username string) (*pb.User, bool) {
	u, ok := m.users[username]
	return u, ok
}
