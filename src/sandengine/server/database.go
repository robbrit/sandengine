package server

import (
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3"

	"sandengine/server/models"

	pb "sandengine/proto"
)

type UserStore interface {
	UsernameExists(string) bool
	EmailExists(string) bool
	AuthenticateUser(string, string) (*pb.User, bool)
	SaveUser(*pb.User) error
	FetchUser(string) (*pb.User, bool)
}

type authDB struct {
	db gorm.DB
}

func (db *authDB) UsernameExists(username string) bool {
	var users int
	db.db.Model(models.User{}).Where("username = ?", username).Count(&users)

	return users > 0
}

func (db *authDB) EmailExists(email string) bool {
	var users int
	db.db.Model(models.User{}).Where("email = ?", email).Count(&users)

	return users > 0
}

func (db *authDB) FetchUser(username string) (*pb.User, bool) {
	var users []models.User
	db.db.Where("username = ?", username).Find(&users)

	if len(users) == 0 {
		return nil, false
	}

	u, err := users[0].Unmarshal()
	if err != nil {
		log.Printf("Error unmarshalling proto: %s", err)
		return nil, false
	}
	return u, true
}

func (db *authDB) AuthenticateUser(username, password string) (*pb.User, bool) {
	user, ok := db.FetchUser(username)

	if !ok || !models.CheckPassword(user, password) {
		// User doesn't exist
		return nil, false
	}
	return user, true
}

func (db *authDB) SaveUser(user *pb.User) error {
	var u models.User
	if err := u.Marshal(user); err != nil {
		return err
	}
	db.db.Save(u)
	return nil
}

func NewDB(path string) (*authDB, error) {
	var wrapper authDB

	// DB initialization
	db, err := gorm.Open("sqlite3", path)
	if err != nil {
		return nil, err
	}

	db.AutoMigrate(&models.User{})

	wrapper.db = db

	return &wrapper, nil
}
