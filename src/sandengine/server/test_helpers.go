package server

import (
	"encoding/base64"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/proto"

	"sandengine/server/mocks"
)

func testHTTP() *HTTPServer {
	h := NewHTTP()
	h.Sessions = mocks.NewSession()
	return h
}

func (h *HTTPServer) fakeRequest(method, path string, handler gin.HandlerFunc, args, resp proto.Message) int {
	req, _ := http.NewRequest(method, path, nil)
	w := httptest.NewRecorder()

	if args != nil {
		b, _ := proto.Marshal(args)
		data := base64.StdEncoding.EncodeToString(b)
		req.Body = ioutil.NopCloser(strings.NewReader(data))
		req.ContentLength = int64(len(data))
	}

	gin.SetMode(gin.ReleaseMode) // This will disable logging.
	r := gin.New()
	if method == "GET" {
		r.GET(path, handler)
	} else if method == "POST" {
		r.POST(path, handler)
	}

	r.ServeHTTP(w, req)

	b, err := base64.StdEncoding.DecodeString(string(w.Body.Bytes()))
	if err != nil {
		log.Fatalf("error on test unmarshal: %s", err)
	}

	if err := proto.Unmarshal(b, resp); err != nil {
		log.Fatalf("error on test unmarshal: %s", err)
	}

	return w.Code
}
