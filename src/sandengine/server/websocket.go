package server

import (
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/proto"
	"github.com/gorilla/websocket"
	"github.com/tuxychandru/pubsub"

	"sandengine"
	pb "sandengine/proto"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type timeFetcher func() time.Time

const (
	// TODO: These constants should be configurable.
	// PubsubCapacity is how many messages can be buffered in the pubsub exchange.
	PubsubCapacity = 100
	// SocketPubsubCapacity is how many messages each socket connection can buffer.
	SocketPubsubCapacity = 10
	// Cooldown is how long a client must wait until they send another message.
	Cooldown = 100 * time.Millisecond
)

// A wrapper around a websocket connection
type socketState struct {
	// Our websocket
	socket *websocket.Conn
	// Whether we have identified or not
	identified bool
	// The user that we identified as
	user *pb.User
	// Our session store.
	sessions SessionStore
	// The exchange for pubsub messages.
	exchange *pubsub.PubSub
	// The channel used for reading game event pubsub messages.
	eventsReader sandengine.Events
	// The primary Game.
	game sandengine.Game
	// Which region are we in?
	region string
	// Which entity ID is our player?
	entityID string
	// When was the last message sent?
	lastMessage time.Time
	// How does this socket manage time?
	timeFetcher timeFetcher
}

// SocketServer handles websocket connections.
type SocketServer struct {
	sessions SessionStore
	// The exchange for pubsub messages.
	exchange *pubsub.PubSub
	// The primary Game.
	game sandengine.Game
}

// Process a single message from a websocket
func (ss *socketState) process(data *pb.ClientRequest) error {
	resp := pb.ServerMessage{}

	// Validate auth key.
	user, err := ss.sessions.FetchUser(data.AuthKey)
	if err != nil {
		return err
	} else if user == nil {
		resp.Errors = append(resp.Errors, "invalid auth key")
		return ss.writeProto(&resp)
	}

	switch req := extractRequest(data).(type) {
	case *pb.IdentifyRequest:
		msg := ss.handleIdentify(req, user)
		resp.Msg = &pb.ServerMessage_Identify{Identify: msg}
	case *pb.JoinGameRequest:
		msg, clientErr, err := ss.handleJoinGame(req, user)
		if err != nil {
			return err
		}
		if clientErr != nil {
			resp.Errors = append(resp.Errors, clientErr.Error())
		} else {
			resp.Msg = &pb.ServerMessage_Join{Join: msg}
		}
	case *pb.Command:
		// Check their cooldown.
		now := ss.timeFetcher()
		if ss.lastMessage.Add(Cooldown).After(now) {
			resp.Errors = append(resp.Errors, "rate-limit")
			return nil
		}
		ss.lastMessage = now

		msg, err := ss.handleRegionCmd(req)
		if err != nil {
			return err
		}
		resp.Msg = &pb.ServerMessage_Effects{
			Effects: serializeEffects(ss.scanMessages(msg), nil),
		}
	default:
		resp.Errors = append(resp.Errors, fmt.Sprintf("unknown request type %T", req))
	}
	return ss.writeProto(&resp)
}

// handleIdentify handles when a client sends an IdentifyRequest.
func (ss *socketState) handleIdentify(req *pb.IdentifyRequest, user *pb.User) *pb.IdentifyResponse {
	msgResponse := &pb.IdentifyResponse{}

	log.Printf("Identified as %s.", user.Username)
	ss.identified = true
	ss.user = user

	// Start listening for messages from the server side.
	go ss.awaitServerMessages()

	return msgResponse
}

// scanMessages scans a list of messages to see if there is any server-side
// processing that needs to be done.
func (ss *socketState) scanMessages(effects []sandengine.Effect) []sandengine.Effect {
	for _, effect := range effects {
		if effect.IsExit() {
			// Stop listening to the region.
			ss.game.RegionStore().Unsub(ss.region, ss.eventsReader)
			ss.region = ""
		}
	}
	return effects
}

// handleJoinGame handles when we receive join game request.
func (ss *socketState) handleJoinGame(req *pb.JoinGameRequest, user *pb.User) (*pb.JoinGameResponse, error, error) {
	// Verify that the player exists and they own it.
	player, err := ss.game.PlayerStore().FindPlayer(req.PlayerName)
	if err != nil {
		return nil, nil, err
	} else if player == nil {
		return nil, fmt.Errorf("no such player '%s'", req.PlayerName), nil
	} else if player.Username != user.Username {
		return nil, fmt.Errorf("you don't own player '%s'", req.PlayerName), nil
	}

	ent, err := ss.game.EntityStore().FindEntity(player.EntityId)
	if err != nil {
		// This is not a client error, we should always have a player entity for
		// every player.
		return nil, nil, err
	}
	if ent == nil {
		return nil, nil, fmt.Errorf("no entity exists for player %s (entity ID %s)", player.Name, player.EntityId)
	}

	// Get the region and tileset.
	region, ok := ss.game.RegionStore().Get(ent.Location().Region)
	if !ok {
		return nil, nil, fmt.Errorf("player %s is in unknown region %s", player.Name, ent.Location().Region)
	}
	tilesetName := region.Data().GetTiles().Tileset
	tileset := ss.game.TilesetStore().Get(tilesetName)
	if tileset == nil {
		return nil, nil, fmt.Errorf("region %s has unknown tileset %s", region.Data().Name, tilesetName)
	}

	// Subscribe to the messages for this region.
	ss.game.RegionStore().Sub(ent.Location().Region, ss.eventsReader)

	ss.region = ent.Location().Region
	ss.entityID = ent.ID()

	// Send a join message to the region.
	effects, err := ss.game.RegionStore().Send(ent.Location().Region, &sandengine.JoinCmd{player})
	return &pb.JoinGameResponse{
		Region:  region.Data(),
		Tileset: tileset,
		Effects: serializeEffects(effects, err),
	}, nil, nil
}

// handleRegionCmd handles when we receive a command for a region.
func (ss *socketState) handleRegionCmd(req *pb.Command) ([]sandengine.Effect, error) {
	if ss.region == "" {
		// Client sent us a command, but we're not in a region. No effect.
		return nil, nil
	}

	// Send the message to the game and it can figure out what to do with it.
	cmd, err := sandengine.MapCommand(req, ss.entityID)
	if err != nil {
		return nil, err
	}
	return ss.game.RegionStore().Send(ss.region, cmd)
}

// writeProto writes a proto to the socket.
func (ss *socketState) writeProto(msg proto.Message) error {
	bytes, err := proto.Marshal(msg)
	if err != nil {
		return err
	}

	return ss.socket.WriteMessage(websocket.TextMessage, []byte(base64.StdEncoding.EncodeToString(bytes)))
}

// listen reads messages from a client.
func (ss *socketState) listen() {
	log.Println("Websocket connected.")
	for {
		_, data, err := ss.socket.ReadMessage()
		if err != nil {
			switch err.(type) {
			case *websocket.CloseError:
				// This happens if the socket is closed while we're waiting, nothing
				// to do here but break.
			default:
				log.Printf("error on ReadMessage: %s", err)
			}
			break
		}

		// Decode the message.
		s, err := base64.StdEncoding.DecodeString(string(data))
		if err != nil {
			log.Printf("error decoding string (%s): %s", data, err)
			continue
		}
		msg := pb.ClientRequest{}
		if err = proto.Unmarshal(s, &msg); err != nil {
			log.Printf("error unmarshaling proto: %s", err)
			// The proto is probably invalid, so tell them that.
			resp := pb.ServerMessage{}
			resp.Errors = append(resp.Errors, err.Error())
			if err = ss.writeProto(&resp); err != nil {
				log.Printf("error responding to client: %s", err)
			}
			continue
		}

		// Figure out the result of this message.
		if err = ss.process(&msg); err != nil {
			log.Printf("error on process: %s", err)
			resp := pb.ServerMessage{}
			resp.Errors = append(resp.Errors, err.Error())
			if err = ss.writeProto(&resp); err != nil {
				log.Printf("error responding to client: %s", err)
			}
			continue
		}
	}
	log.Printf("%s disconnected", ss.user.Username)
	if err := ss.disconnect(); err != nil {
		log.Println("Error while disconnecting: %s", err)
	}
}

// Handle disconnecting from the game.
func (ss *socketState) disconnect() error {
	if !ss.identified {
		return nil
	}

	// Unsubscribe from all my topics.
	ss.game.RegionStore().Unsub("global", ss.eventsReader)
	if ss.region != "" {
		ss.game.RegionStore().Unsub(ss.region, ss.eventsReader)
	}

	// Remove the entity from the region.
	effects, err := ss.game.RegionStore().Send(ss.region, &sandengine.RemoveCmd{ss.entityID})
	if err != nil {
		return err
	}
	if outEffects := ss.scanMessages(effects); outEffects != nil {
		// There shouldn't be any effects left.
		return fmt.Errorf("disconnect resulted in non-broadcast messages: %q", outEffects)
	}

	return nil
}

// awaitServerMessages listens for messages coming from the game (as opposed to
// the user) and processes them.
func (ss *socketState) awaitServerMessages() {
	for rawMsg := range ss.eventsReader {
		switch effects := rawMsg.(type) {
		case []sandengine.Effect:
			if err := ss.writeProto(&pb.ServerMessage{
				Msg: &pb.ServerMessage_Effects{Effects: serializeEffects(effects, nil)},
			}); err != nil {
				log.Println("error writing proto: %s", err)
			}
		default:
			log.Println("unknown event type %T: %v", rawMsg, rawMsg)
		}
	}
}

func (s *SocketServer) websocketHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("Error on websocket connect:", err)
		return
	}

	ss := socketState{
		sessions:     s.sessions,
		socket:       conn,
		exchange:     s.exchange,
		eventsReader: make(sandengine.Events, SocketPubsubCapacity),
		game:         s.game,
		timeFetcher:  time.Now,
	}
	// Subscribe to the global messages channel.
	// For now we don't actually receive any messages on this topic, but there is
	// a quirk with pubsub that closes a channel if you unsubscribe from
	// everything.
	ss.game.RegionStore().Sub("global", ss.eventsReader)

	ss.listen()
}

// extractRequest gets the proper proto out of a ClientRequest.
func extractRequest(client *pb.ClientRequest) interface{} {
	if msg := client.GetIdentify(); msg != nil {
		return msg
	}
	if msg := client.GetJoin(); msg != nil {
		return msg
	}
	if msg := client.GetCommand(); msg != nil {
		return msg
	}
	log.Printf("Unknown msg: %s", client)
	return nil
}

// serializeEffects groups a bunch of effects into a *pb.Effects bundle.
func serializeEffects(effects []sandengine.Effect, err error) *pb.Effects {
	if err != nil {
		return &pb.Effects{Error: err.Error()}
	}

	resp := &pb.Effects{}
	protos := []*pb.Effect{}
	for _, effect := range effects {
		data, err := proto.Marshal(effect)
		if err != nil {
			resp.Error = err.Error()
			return resp
		}
		protos = append(protos, &pb.Effect{
			Name: strings.Replace(fmt.Sprintf("%T", effect), "*sandengine.", "", 1),
			Data: data,
		})
	}
	resp.Effects = protos
	return resp
}

// NewSocketServer constructs a new websocket manager.
func NewSocketServer() *SocketServer {
	return &SocketServer{
		exchange: pubsub.New(PubsubCapacity),
	}
}

// Start actually starts running the socket server.
func (s *SocketServer) Start(sessions SessionStore, game sandengine.Game, g *gin.Engine) {
	s.sessions = sessions
	s.game = game

	g.GET("/ws", func(c *gin.Context) {
		s.websocketHandler(c.Writer, c.Request)
	})
}
