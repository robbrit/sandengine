package server

import (
	"fmt"
	"log"

	"sandengine"
)

const (
	DefaultHTTPPort     = 8080
	DefaultStaticRoot   = "static"
	DefaultTemplateRoot = "templates"
	DefaultAuthDB       = "sandengine-auth.db"
	DefaultAssetsFile   = "static/assets.json"
)

type Server interface {
	// Start starts the server with a given set of options.
	Start(ServerOptions) error
}

type allServers struct {
	HTTP    *HTTPServer
	Sockets *SocketServer
	Game    sandengine.Game
}

type ServerOptions struct {
	// What is the port that the HTTP server should listen on?
	HTTPPort int
	// Where should I load all my static assets from?
	StaticRoot string
	// Where should I load all my templates from?
	TemplateRoot string
	// Are we running in debug mode?
	Debug bool
	// What should we show at the top of the page?
	Title string
	// What should we use as a session secret?
	SessionSecret string
	// What SQLite database should we use for authentication info?
	AuthDatabase string
	// What SQLite database should we use for game data?
	GameDatabase string
	// What file contains all our assets?
	AssetsFile string
	// Which file to use for HTTPS certificate?
	HTTPSCert string
	// Which file to use for HTTPS key?
	HTTPSKey string

	// Options specifically for sessions.
	SessionOptions SessionOptions
}

func New(g sandengine.Game) *allServers {
	return &allServers{
		HTTP:    NewHTTP(),
		Sockets: NewSocketServer(),
		Game:    g,
	}
}

func (a *allServers) Start(opts ServerOptions) error {
	if opts.Debug {
		log.Println("Running in debug mode.")
	}

	// These arguments are required, if not passed then fail horribly.
	if opts.SessionSecret == "" {
		return fmt.Errorf("no session secret provided")
	}

	// These arguments have defaults.
	if opts.AuthDatabase == "" {
		opts.AuthDatabase = DefaultAuthDB
	}

	authDB, err := NewDB(opts.AuthDatabase)
	if err != nil {
		return err
	}

	sess := NewSessionStore(authDB, opts.SessionOptions)

	go func() {
		if opts.HTTPPort == 0 {
			opts.HTTPPort = DefaultHTTPPort
		}
		if opts.StaticRoot == "" {
			opts.StaticRoot = DefaultStaticRoot
		}
		if opts.TemplateRoot == "" {
			opts.TemplateRoot = DefaultTemplateRoot
		}
		if opts.AssetsFile == "" {
			opts.AssetsFile = DefaultAssetsFile
		}

		log.Fatal(a.HTTP.Start(a.Game, authDB, sess, a.Sockets, opts))
	}()

	a.Game.Start()

	return nil
}
