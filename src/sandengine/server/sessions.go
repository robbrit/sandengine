package server

import (
	"log"
	"time"

	"github.com/bradfitz/gomemcache/memcache"
	"github.com/satori/go.uuid"

	pb "sandengine/proto"
)

var _ = log.Println

const (
	DefaultSessionServer  = "localhost:11211"
	DefaultSessionPrefix  = "sandengine-"
	DefaultSessionTimeout = 30 * time.Minute
)

type SessionOptions struct {
	Server  string
	Prefix  string
	Timeout time.Duration
}

type SessionStore interface {
	FetchUser(string) (*pb.User, error)
	RegisterUser(*pb.User) string
	ClearKey(string)
}

type sessions struct {
	cache *memcache.Client
	users map[string]*pb.User
	db    UserStore
	opts  SessionOptions
}

// NewSessionStore creates a new SessionStore object.
func NewSessionStore(db UserStore, opts SessionOptions) *sessions {
	if opts.Server == "" {
		opts.Server = DefaultSessionServer
	}
	if opts.Prefix == "" {
		opts.Prefix = DefaultSessionPrefix
	}
	if opts.Timeout == 0 {
		opts.Timeout = DefaultSessionTimeout
	}

	return &sessions{
		cache: memcache.New(opts.Server),
		users: make(map[string]*pb.User),
		db:    db,
		opts:  opts,
	}
}

// FetchUser grabs a user from an auth key. Returns nil if no user is registered with that auth key.
func (s *sessions) FetchUser(auth string) (*pb.User, error) {
	if auth == "" {
		return nil, nil
	}

	username, err := s.cache.Get(auth)
	if err != nil {
		if err == memcache.ErrCacheMiss {
			// In the case that the user doesn't exist, don't return an error.
			return nil, nil
		}
		return nil, err
	}

	// touch it so that we don't expire
	s.cache.Touch(auth, int32(s.opts.Timeout/time.Second))

	user, ok := s.users[string(username.Value)]
	if !ok {
		// This will have happened if we restarted the server, so re-fetch the user
		user, ok = s.db.FetchUser(string(username.Value))
		if !ok {
			// This shouldn't happen unless the user was deleted or something
			return nil, nil
		}

		// and cache it so that next time we are ok
		s.users[user.Username] = user
	}

	return user, nil
}

// RegisterUser binds an auth key to a user.
func (s *sessions) RegisterUser(user *pb.User) string {
	auth := s.newSessionKey()

	s.cache.Set(&memcache.Item{
		Key:        auth,
		Value:      []byte(user.Username),
		Expiration: int32(s.opts.Timeout / time.Second),
	})

	s.users[user.Username] = user

	return auth
}

// ClearKey clears a specified key.
func (s *sessions) ClearKey(key string) {
	s.cache.Delete(key)
}

// newSessionKey generators a new session key.
func (s *sessions) newSessionKey() string {
	return s.opts.Prefix + uuid.NewV4().String()
}
