package server

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
)

// TestInvalidProtos just sends an invalid proto value to a bunch of routes
// to verify that they fail when we send bad data to them.
func TestInvalidProtos(t *testing.T) {
	h := testHTTP()

	for _, test := range []struct {
		path    string
		method  string
		handler gin.HandlerFunc
	}{
		{"/r", "GET", h.regionHandler},
		{"/t", "GET", h.tilesetHandler},
		{"/valid_auth", "GET", h.validAuthHandler},
		{"/login", "POST", h.loginHandler},
		{"/signup", "POST", h.signupHandler},
		{"/players", "GET", h.playersHandler},
		{"/players", "POST", h.createPlayerHander},
	} {
		req, _ := http.NewRequest(test.method, test.path, nil)
		w := httptest.NewRecorder()

		data := "this should probably not match a proto"
		req.Body = ioutil.NopCloser(strings.NewReader(data))
		req.ContentLength = int64(len(data))

		gin.SetMode(gin.ReleaseMode) // This will disable logging.
		r := gin.New()
		if test.method == "GET" {
			r.GET(test.path, test.handler)
		} else if test.method == "POST" {
			r.POST(test.path, test.handler)
		}

		r.ServeHTTP(w, req)

		if w.Code != http.StatusBadRequest {
			t.Errorf("%s: got code %d, want %d", test.path, w.Code, http.StatusBadRequest)
		}
	}
}
