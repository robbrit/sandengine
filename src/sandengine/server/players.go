package server

// This file contains all the routes related to player management.

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	pb "sandengine/proto"
)

func (h *HTTPServer) playersHandler(c *gin.Context) {
	req := pb.SimpleRequest{}
	resp := pb.PlayerListResponse{}

	if err := readProto(c, &req); err != nil {
		resp.Errors = append(resp.Errors, err.Error())
		c.String(http.StatusBadRequest, "expecting SimpleRequest proto")
		return
	}

	u, err := h.Sessions.FetchUser(req.AuthKey)
	if err != nil {
		log.Printf("Error on fetching session: %s", err)
		writeProto(c, http.StatusOK, &resp)
		return
	}

	players, err := h.Game.PlayerStore().FindPlayersForUser(u.Username)
	if err != nil {
		log.Printf("Error fetching players: %s", err)
		resp.Errors = append(resp.Errors, err.Error())
		writeProto(c, http.StatusOK, &resp)
		return
	}

	resp.Players = players

	writeProto(c, http.StatusOK, &resp)
}

func (h *HTTPServer) createPlayerHander(c *gin.Context) {
	req := pb.CreatePlayerRequest{}
	resp := pb.CreatePlayerResponse{}

	// Verify that they sent a valid request.
	if err := readProto(c, &req); err != nil {
		resp.Errors = append(resp.Errors, err.Error())
		c.String(http.StatusBadRequest, "expecting CreatePlayerRequest proto")
		return
	}

	// Ensure the user exists and is logged in.
	u, err := h.Sessions.FetchUser(req.AuthKey)
	if err != nil {
		log.Printf("Error on fetching session: %s", err)
		writeProto(c, http.StatusOK, &resp)
		return
	}
	if u == nil {
		resp.Errors = append(resp.Errors, "invalid auth key")
		writeProto(c, http.StatusUnauthorized, &resp)
		return
	}

	// Ensure the player does not yet exist.
	p, err := h.Game.PlayerStore().FindPlayer(req.Name)
	if err != nil {
		log.Printf("Error validating player: %s", err)
		resp.Errors = append(resp.Errors, err.Error())
		writeProto(c, http.StatusInternalServerError, &resp)
		return
	}
	if p != nil {
		resp.Errors = append(resp.Errors, fmt.Sprintf("player '%s' already exists", req.Name))
		writeProto(c, http.StatusOK, &resp)
		return
	}

	// Create the player object.
	p, err = h.Game.PlayerStore().CreatePlayer(req.Name, u.Username)
	if err != nil {
		log.Printf("Error creating player: %s", err)
		resp.Errors = append(resp.Errors, err.Error())
		writeProto(c, http.StatusOK, &resp)
		return
	}

	// Create an associated entity.
	ent := h.Game.EntityFactory().NewPlayer(p.Name)
	if err = h.Game.EntityStore().SaveEntities(ent); err != nil {
		log.Printf("Error saving player entity to DB: %s", err)
		resp.Errors = append(resp.Errors, err.Error())
		writeProto(c, http.StatusInternalServerError, &resp)
		return
	}

	p.EntityId = ent.ID()
	if err = h.Game.PlayerStore().SavePlayer(p); err != nil {
		log.Printf("Error saving player to DB: %s", err)
		resp.Errors = append(resp.Errors, err.Error())
		writeProto(c, http.StatusInternalServerError, &resp)
		return
	}

	resp.Player = p

	writeProto(c, http.StatusOK, &resp)
}
