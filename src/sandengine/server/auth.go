package server

// This file handles all the authentication-related routes.

import (
	"fmt"
	"log"
	"net/http"

	"github.com/asaskevich/govalidator"
	"github.com/gin-gonic/gin"

	"sandengine/server/models"

	pb "sandengine/proto"
)

const (
	// minPasswordLen is how long a password must be.
	minPasswordLen = 8
)

// validAuthHandler handles the /valid_auth route.
func (h *HTTPServer) validAuthHandler(c *gin.Context) {
	req := pb.SimpleRequest{}
	response := pb.ValidAuthResponse{}

	if err := readProto(c, &req); err != nil {
		// TODO use errors in the response proto
		c.String(http.StatusBadRequest, "expecting SimpleRequest proto")
		return
	}

	u, err := h.Sessions.FetchUser(req.AuthKey)
	if err != nil {
		log.Printf("Error on fetching session: %s", err)
	} else {
		response.Ok = u != nil
	}

	writeProto(c, http.StatusOK, &response)
}

// loginHandler handles the /login route.
func (h *HTTPServer) loginHandler(c *gin.Context) {
	var request pb.AuthRequest
	var response pb.AuthResponse

	if err := readProto(c, &request); err != nil {
		log.Printf("Error reading request: %s", err)
		response.Errors = append(response.Errors, err.Error())
		writeProto(c, http.StatusBadRequest, &response)
		return
	}

	username := request.Username
	password := request.Password

	user, ok := h.UserStore.AuthenticateUser(username, password)
	if !ok {
		response.Errors = append(response.Errors, "not found")
		writeProto(c, http.StatusNotFound, &response)
		return
	}

	// User exists, so create a new session and send the info back to the client.
	response.AuthKey = h.Sessions.RegisterUser(user)

	writeProto(c, http.StatusOK, &response)
}

// signupHandler handles the /signup route.
func (h *HTTPServer) signupHandler(c *gin.Context) {
	var request pb.SignupRequest
	var response pb.SignupResponse

	if err := readProto(c, &request); err != nil {
		log.Printf("Error reading request: %s", err)
		response.Errors = append(response.Errors, err.Error())
		writeProto(c, http.StatusBadRequest, &response)
		return
	}

	username := request.Username
	password := request.Password
	email := request.Email

	if username == "" {
		response.Errors = append(response.Errors, "need a username")
	}
	if h.UserStore.UsernameExists(username) {
		response.Errors = append(response.Errors, "username is taken")
	}
	if email == "" || !govalidator.IsEmail(email) {
		response.Errors = append(response.Errors, "email is invalid")
	}
	if h.UserStore.EmailExists(email) {
		response.Errors = append(response.Errors, "email is taken")
	}
	if len(password) < minPasswordLen {
		response.Errors = append(response.Errors, fmt.Sprintf("password must be at least %d characters", minPasswordLen))
	}

	if len(response.Errors) > 0 {
		writeProto(c, http.StatusOK, &response)
		return
	}

	// We got this far, we're good to go.
	user := models.NewUser(username, password, email)
	h.UserStore.SaveUser(user)

	// Log them in automatically.
	response.AuthKey = h.Sessions.RegisterUser(user)

	writeProto(c, http.StatusOK, &response)
}
