package server

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/proto"
	"sandengine"
	"sandengine/utils"
)

type HTTPServer struct {
	Game      sandengine.Game
	Sessions  SessionStore
	UserStore UserStore
	DebugMode bool
}

// writeProto writes a proto and an HTTP status code to gin Context.
func writeProto(c *gin.Context, s int, p proto.Message) {
	str, err := utils.SerializeProto(p)
	if err != nil {
		log.Printf("Error marshaling proto: %s", err)
		c.String(http.StatusInternalServerError, "")
	} else {
		c.String(s, str)
	}
}

// readProto reads a proto from an HTTP request.
func readProto(c *gin.Context, p proto.Message) error {
	var data string

	if c.Request.ContentLength == 0 {
		// No body, check the URL instead.
		data = c.Request.URL.RawQuery
	} else {
		raw, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			return err
		}
		data = string(raw)
	}

	return utils.ParseProto(data, p)
}

func NewHTTP() *HTTPServer {
	return &HTTPServer{}
}

func (h *HTTPServer) Start(g sandengine.Game, store UserStore, sess SessionStore, sock *SocketServer, opts ServerOptions) error {

	r := gin.Default()

	h.Game = g
	h.UserStore = store
	h.Sessions = sess

	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	// Verifies whether a certain auth credential is valid.
	r.GET("/valid_auth", h.validAuthHandler)
	// Attempts to log a user in.
	r.POST("/login", h.loginHandler)
	// Attempts to sign up.
	r.POST("/signup", h.signupHandler)

	// Gets the list of players for a user.
	r.GET("/players", h.playersHandler)
	// Creates a new player.
	r.POST("/players", h.createPlayerHander)

	// Fetches a region based on the name.
	r.GET("/r/:name", h.regionHandler)
	// Fetches a tileset based on the name.
	r.GET("/t/:name", h.tilesetHandler)

	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl.html", gin.H{
			"title":   opts.Title,
			"isDebug": opts.Debug,
		})
	})

	r.GET("/template/:name", func(c *gin.Context) {
		c.HTML(http.StatusOK, c.Param("name"), gin.H{
			"isDebug": opts.Debug,
		})
	})

	if opts.Debug {
		r.GET("/test", func(c *gin.Context) {
			c.HTML(http.StatusOK, "test.tmpl.html", gin.H{})
		})
	}

	// Fetches the compiled assets (images, sounds, object data, etc.)
	assets, err := ioutil.ReadFile(opts.AssetsFile)
	if err != nil {
		return err
	}
	r.GET("/assets", func(c *gin.Context) {
		c.Data(http.StatusOK, "text/json", assets)
	})

	r.StaticFS("/static", http.Dir(opts.StaticRoot))
	r.LoadHTMLGlob(fmt.Sprintf("%s/*.tmpl.html", opts.TemplateRoot))

	sock.Start(sess, g, r)
	if opts.HTTPSCert == "" || opts.HTTPSKey == "" {
		log.Println("Running as insecure server")
		return r.Run(fmt.Sprintf(":%d", opts.HTTPPort))
	}
	return r.RunTLS(fmt.Sprintf(":%d", opts.HTTPPort), opts.HTTPSCert, opts.HTTPSKey)
}
