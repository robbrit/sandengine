package server

import (
	"net/http"
	"testing"

	"sandengine/mocks"

	pb "sandengine/proto"
)

func TestRegionHandler(t *testing.T) {
	for _, test := range []struct {
		desc       string
		hasRegion  bool
		wantStatus int
	}{
		{"good region", true, http.StatusOK},
		{"unknown region", false, http.StatusNotFound},
	} {
		game := &mocks.Game{}
		if test.hasRegion {
			game.Regions = mocks.NewRegionStore(nil, mocks.NewRegion(&pb.Region{}))
		} else {
			game.Regions = mocks.NewRegionStore(nil, nil)
		}
		h := testHTTP()
		h.Game = game

		var req pb.SimpleRequest
		var resp pb.RegionResponse
		code := h.fakeRequest("GET", "/r/1", h.regionHandler, &req, &resp)

		if code != test.wantStatus {
			t.Errorf("%s: got code %d, want %d", test.desc, code, test.wantStatus)
		}
	}
}

func TestTilesetHandler(t *testing.T) {
	for _, test := range []struct {
		desc       string
		hasTileset bool
		wantStatus int
	}{
		{"good tileset", true, http.StatusOK},
		{"unknown tileset", false, http.StatusNotFound},
	} {
		game := &mocks.Game{}
		if test.hasTileset {
			game.Tilesets = mocks.NewTilesetStore(nil, &pb.Tileset{})
		} else {
			game.Tilesets = mocks.NewTilesetStore(nil, nil)
		}
		h := testHTTP()
		h.Game = game

		var req pb.SimpleRequest
		var resp pb.TilesetResponse
		code := h.fakeRequest("GET", "/t/1", h.tilesetHandler, &req, &resp)

		if code != test.wantStatus {
			t.Errorf("%s: got code %d, want %d", test.desc, code, test.wantStatus)
		}
	}
}
