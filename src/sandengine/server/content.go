package server

// This file handles all the region-related routes.

import (
	"net/http"

	"github.com/gin-gonic/gin"

	pb "sandengine/proto"
)

// regionHandler handles the /r/:name route.
func (h *HTTPServer) regionHandler(c *gin.Context) {
	req := pb.SimpleRequest{}
	resp := pb.RegionResponse{}

	if err := readProto(c, &req); err != nil {
		resp.Errors = append(resp.Errors, err.Error())
		writeProto(c, http.StatusBadRequest, &resp)
		return
	}

	region, ok := h.Game.RegionStore().Get(c.Param("name"))

	if !ok {
		writeProto(c, http.StatusNotFound, &resp)
	} else {
		resp.Region = region.Data()
		writeProto(c, http.StatusOK, &resp)
	}
}

// tilesetHandler handles the /t/:name route.
func (h *HTTPServer) tilesetHandler(c *gin.Context) {
	req := pb.SimpleRequest{}
	resp := pb.TilesetResponse{}

	if err := readProto(c, &req); err != nil {
		resp.Errors = append(resp.Errors, err.Error())
		writeProto(c, http.StatusBadRequest, &resp)
		return
	}

	resp.Tileset = h.Game.TilesetStore().Get(c.Param("name"))

	if resp.Tileset == nil {
		writeProto(c, http.StatusNotFound, &resp)
	} else {
		writeProto(c, http.StatusOK, &resp)
	}
}
