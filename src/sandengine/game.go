package sandengine

// A Game manages all the different regions of the game, and all the players.
type Game interface {
	Start()
	RegionStore() RegionStore
	PlayerStore() PlayerStore
	TilesetStore() TilesetStore
	EntityStore() EntityStore
	EntityFactory() EntityFactory
	PhysicsManager() PhysicsManager
	WorkerManager() WorkerManager
}

// GameOptions is a list of options to be used when creating a new game.
type GameOptions struct {
	// What DB do we use?
	Database string
	// Are we running in debug mode?
	Debug bool
	// What EntityFactory should we use for creating entities?
	EntityFactory EntityFactory
	// How are we applying physics in this world?
	PhysicsManager PhysicsManager
	// What RegionStoreOptions should we use?
	RegionStoreOptions RegionStoreOptions
	// How are we managing workers?
	WorkerManager WorkerManager
}
