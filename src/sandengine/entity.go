package sandengine

import (
	"github.com/stretchr/objx"
	pb "sandengine/proto"
	"sandengine/utils"
)

// An EntityFactory takes a proto and creates Entity objects from it.
type EntityFactory interface {
	// Create builds a new Entity from the given arguments.
	Create(entityType string, loc *utils.Location, props, privs objx.Map) Entity
	// Build instantiates an Entity from existing data.
	Build(id, entityType string, loc *utils.Location, props, privs objx.Map) Entity
	// NewPlayer creates a new player entity.
	NewPlayer(name string) Entity
}

// An EntityStore is where we permanently store Entity objects.
type EntityStore interface {
	// FindEntity gets an entity for a given Entity ID.
	FindEntity(string) (Entity, error)
	// FindEntitiesForRegion gets all the entities in a region.
	FindEntitiesForRegion(*pb.Region) ([]Entity, error)
	// CreateEntities inserts a number of entities into the database.
	CreateEntities(...Entity) error
	// SaveEntities updates a number of entities to the database.
	SaveEntities(...Entity) error
	// RemoveEntities removes some entities from the database.
	RemoveEntities(...string) error
	// LoadStatic loads a number of static entities into the database.
	LoadStatic(string) error
}

// An Entity is an entity within the system.
type Entity interface {
	Proto() *pb.Entity
	EntityType() string
	ID() string
	// P gets the public attributes for an entity.
	P() objx.Map
	// Pr gets the private attributes for an entity.
	Pr() objx.Map
	Location() *utils.Location
	SetLocation(*utils.Location)
}
