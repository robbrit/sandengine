package sandengine

// GameDB is a database that stores all the data related to game logic.
type GameDB interface {
	EntityStore
	PlayerStore
}
