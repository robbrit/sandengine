package db

import (
	"reflect"
	"sort"
	"testing"

	"sandengine/mocks"
	"sandengine/models"
	pb "sandengine/proto"
	"sandengine/utils"
)

func resetDB() (*gameDB, error) {
	dbi, err := New("sandbuilder:sandbuilder@/sandbuilder_test", &mocks.EntityFactory{})
	if err != nil {
		return nil, err
	}
	db := dbi.(*gameDB)
	db.db.Exec(`TRUNCATE TABLE players`)
	db.db.Exec(`TRUNCATE TABLE entities`)
	return db, nil
}

/*
func TestCreateEntities(t *testing.T) {
	for _, test := range []struct {
		desc string
	}{
		{"good create"},
		{"insert duplicate"},
		{"insert lots"},
	} {
	}
*/

func TestSaveEntities(t *testing.T) {
	for _, test := range []struct {
		desc       string
		existing   *models.Entity
		update     *mocks.Entity
		wantResult bool
	}{
		{"save non-existent", nil, mocks.NewEntity("1"), false},
		{"good", &models.Entity{"1", "", "", "2"}, &mocks.Entity{
			"1", &utils.Location{
				X:      2,
				Y:      7,
				Region: "3",
			},
		}, true},
	} {
		db, err := resetDB()
		if err != nil {
			t.Fatalf("%s: unexpected error connecting to DB: %s", test.desc, err)
		}
		if test.existing != nil {
			db.db.Create(test.existing)
		}

		// Now do the actual test.
		gotErr := db.SaveEntities(test.update)
		if gotErr != nil {
			t.Errorf("%s: got error %s, want nil", test.desc, gotErr)
			continue
		}

		// Verify that it actually updated.
		ents := []models.Entity{}
		if err := db.db.Where("entity_id = ?", test.update.EntityID).Find(&ents).Error; err != nil {
			t.Errorf("%s: unexpected error on Where(): %s", test.desc, err)
			continue
		}

		if !test.wantResult {
			if len(ents) > 0 {
				t.Errorf("%s: got entities %s, want none", test.desc, ents)
			}
			continue
		}
		if len(ents) != 1 {
			t.Errorf("%s: got entities %s, want just one", test.desc, ents)
			continue
		}

		epb, err := ents[0].Unmarshal()
		if err != nil {
			t.Errorf("%s: unexpected error on Unmarshal(): %s", test.desc, err)
			continue
		}

		if epb.Location.X != test.update.Loc.X {
			t.Errorf("%s: got location x %d, want %d", test.desc, epb.Location.X, test.update.Loc.X)
		}
		if epb.Location.Y != test.update.Loc.Y {
			t.Errorf("%s: got location y %d, want %d", test.desc, epb.Location.Y, test.update.Loc.Y)
		}
		if epb.Location.Region != test.update.Loc.Region {
			t.Errorf("%s: got location region %d, want %d", test.desc, epb.Location.Region, test.update.Loc.Region)
		}
	}
}

func TestFindEntity(t *testing.T) {
	db, err := resetDB()
	if err != nil {
		t.Fatalf("unexpected error connecting to DB: %s", err)
	}
	db.CreateEntities(mocks.NewEntity("17"))

	for _, test := range []struct {
		desc    string
		id      string
		wantNil bool
	}{
		{"good case", "17", false},
		{"non-existent entity", "92", true},
	} {
		got, err := db.FindEntity(test.id)
		if err != nil {
			t.Errorf("%s: unexpected error on FindEntity(%s): %s", test.desc, test.id, err)
			continue
		}

		if test.wantNil {
			if got != nil {
				t.Errorf("%s: got %s, want nil", test.desc, got)
			}
			continue
		}
		if got == nil {
			t.Errorf("%s: got nil, want non-nil", test.desc)
			continue
		}
		if got.ID() != test.id {
			t.Errorf("%s: got entity %s, want %s", test.desc, got.ID(), test.id)
		}
	}
}

func TestFindEntitiesForRegion(t *testing.T) {
	db, err := resetDB()
	if err != nil {
		t.Fatalf("unexpected error connecting to DB: %s", err)
	}
	db.CreateEntities(&mocks.Entity{"17", &utils.Location{Region: "2"}},
		&mocks.Entity{"11", &utils.Location{Region: "4"}})

	for _, test := range []struct {
		desc    string
		region  string
		wantIDs []string
	}{
		{"good region", "2", []string{"17"}},
		{"other region", "4", []string{"11"}},
		{"unknown region", "3", nil},
	} {
		got, err := db.FindEntitiesForRegion(&pb.Region{Name: test.region})
		if err != nil {
			t.Errorf("%s: unexpected error on FindEntitiesForRegion(%s): %s", test.desc, test.region, err)
			continue
		}

		var gotIDs []string
		for _, ent := range got {
			gotIDs = append(gotIDs, ent.ID())
		}
		sort.Strings(gotIDs)

		if !reflect.DeepEqual(gotIDs, test.wantIDs) {
			t.Errorf("%s: got IDs %s, want %s", test.desc, gotIDs, test.wantIDs)
		}
	}
}

func TestRemoveEntities(t *testing.T) {
	db, err := resetDB()
	if err != nil {
		t.Fatalf("unexpected error connecting to DB: %s", err)
	}
	db.CreateEntities(mocks.NewEntity("17"))

	for _, test := range []struct {
		desc string
		id   string
	}{
		{"good case", "17"},
		{"non-existent entity", "92"},
	} {
		err := db.RemoveEntities(test.id)
		if err != nil {
			// Delete never gives an error.
			t.Errorf("%s: unexpected error on RemoveEntities(%s): %s", test.desc, test.id, err)
			continue
		}

		// Ensure that we don't find the entity.
		ent, err := db.FindEntity(test.id)
		if err != nil {
			t.Errorf("%s: unexpected error on FindEntity(%s): %s", test.desc, test.id, err)
			continue
		}
		if ent != nil {
			t.Errorf("%s: got entity %v, want nil", test.desc, ent)
			continue
		}
	}
}

/*func TestLoadStatic(t *testing.T) {
	for _, test := range []struct {
		desc string
	}{
		{"good file"},
		{"invalid file"},
		{"non-existent file"},
	} {
	}
}

func TestFindPlayer(t *testing.T) {
	for _, test := range []struct {
		desc string
	}{
		{"good player"},
		{"unknown player"},
	} {
	}
}

func TestFindPlayersForUser(t *testing.T) {
	for _, test := range []struct {
		desc string
	}{
		{"good user"},
		{"unknown user"},
	} {
	}
}

func TestCreatePlayer(t *testing.T) {
	for _, test := range []struct {
		desc string
	}{
		{"good create"},
		{"player already exists"},
	} {
	}
}

func TestSavePlayer(t *testing.T) {
	for _, test := range []struct {
		desc string
	}{
		{"good save"},
	} {
	}
}*/
