package db

import (
	"fmt"
	"io/ioutil"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/objx"
	"sandengine"
	"sandengine/models"
	pb "sandengine/proto"
	"sandengine/utils"
)

type gameDB struct {
	db      *gorm.DB
	factory sandengine.EntityFactory
}

func (g *gameDB) CreateEntities(ents ...sandengine.Entity) error {
	// Gorm doesn't have bulk insert, so construct a raw query.
	numFields := 4
	// There is a maximum number of variables we can insert, so do this in
	// batches - 199 is the maximum batch size that doesn't make gorm fall over.
	batchSize := 199

	for current := 0; current < len(ents); current += batchSize {
		size := len(ents) - current
		if size > batchSize {
			size = batchSize
		}

		insertStr := "(" + strings.Repeat("?, ", numFields-1) + "?)"
		sql := "INSERT INTO entities (proto, entity_id, region, privates) VALUES " +
			strings.Repeat(insertStr+", ", size-1) + insertStr

		values := make([]interface{}, size*numFields)
		for i := 0; i < size; i++ {
			var ent models.Entity
			if err := ent.Marshal(ents[i+current]); err != nil {
				return err
			}
			values[i*numFields] = ent.Proto
			values[i*numFields+1] = ent.EntityID
			values[i*numFields+2] = ent.Region
			values[i*numFields+3] = ent.Privates
		}

		if err := g.db.Exec(sql, values...).Error; err != nil {
			return err
		}
	}

	return nil
}

func (g *gameDB) SaveEntities(ents ...sandengine.Entity) error {
	// TODO: Can we do batch updates somehow?
	for _, ent := range ents {
		var model models.Entity
		if err := model.Marshal(ent); err != nil {
			return err
		}
		if err := g.db.Save(&model).Error; err != nil {
			return err
		}
	}

	return nil
}

func (g *gameDB) RemoveEntities(ids ...string) error {
	return g.db.Where(ids).Delete(models.Entity{}).Error
}

func (g *gameDB) FindEntity(id string) (sandengine.Entity, error) {
	var entities []models.Entity
	g.db.Where("entity_id = ?", id).Find(&entities)

	if len(entities) == 0 {
		return nil, nil
	}

	return g.modelToEntity(entities[0])
}

func (g *gameDB) FindEntitiesForRegion(r *pb.Region) ([]sandengine.Entity, error) {
	var entities []models.Entity
	g.db.Where("region = ?", r.Name).Find(&entities)

	var ents []sandengine.Entity
	for _, ent := range entities {
		ent, err := g.modelToEntity(ent)
		if err != nil {
			return nil, err
		}
		ents = append(ents, ent)
	}
	return ents, nil
}

func (g *gameDB) LoadStatic(staticFile string) error {
	raw, err := ioutil.ReadFile(staticFile)
	if err != nil {
		return err
	}

	var pbEnts pb.StaticEntities
	if err := utils.ParseProto(string(raw), &pbEnts); err != nil {
		return err
	}

	for _, r := range pbEnts.Regions {
		var ents []sandengine.Entity
		for _, p := range r.StaticEntities {
			props, err := objx.FromJSON(p.Properties)
			if err != nil {
				return fmt.Errorf("json parse props %s: %s", p, err)
			}
			privs, err := objx.FromJSON(p.Privates)
			if err != nil {
				return fmt.Errorf("json parse privs %s: %s", p, err)
			}
			ent := g.factory.Create(p.EntityType, utils.ToLoc(p.Location), props, privs)
			ent.Pr().Set("static", true)
			ents = append(ents, ent)
		}
		if err := g.CreateEntities(ents...); err != nil {
			return err
		}
	}
	return nil
}

// FindPlayer finds a player for a given player name.
func (g *gameDB) FindPlayer(name string) (*pb.Player, error) {
	var players []models.Player
	g.db.Where("name = ?", name).Find(&players)

	if len(players) == 0 {
		return nil, nil
	}

	p, err := players[0].Unmarshal()
	if err != nil {
		return nil, err
	}
	return p, nil
}

// FindPlayersForUser finds all the players for a given username.
func (g *gameDB) FindPlayersForUser(username string) ([]*pb.Player, error) {
	var players []models.Player
	g.db.Where("username = ?", username).Find(&players)

	var pbs []*pb.Player

	for _, player := range players {
		p, err := player.Unmarshal()
		if err != nil {
			return nil, err
		}
		pbs = append(pbs, p)
	}
	return pbs, nil
}

// CreatePlayer creates a new player with a given name and username.
func (g *gameDB) CreatePlayer(name, username string) (*pb.Player, error) {
	// Assumption: username is valid.
	player, err := g.FindPlayer(name)
	if err != nil {
		return nil, err
	}
	if player != nil {
		return nil, fmt.Errorf("player with name '%s' already exists")
	}
	return &pb.Player{
		Name:     name,
		Username: username,
	}, nil
}

// SavePlayer persists the player to the database.
func (g *gameDB) SavePlayer(ppb *pb.Player) error {
	var p models.Player
	if err := p.Marshal(ppb); err != nil {
		return err
	}
	if err := g.db.Save(p).Error; err != nil {
		return err
	}
	return nil
}

func (g *gameDB) modelToEntity(ent models.Entity) (sandengine.Entity, error) {
	p, err := ent.Unmarshal()
	if err != nil {
		return nil, err
	}
	props := objx.Map{}
	privs := objx.Map{}
	if p.Properties != "" {
		props, err = objx.FromJSON(p.Properties)
		if err != nil {
			return nil, err
		}
	}
	if ent.Privates != "" {
		privs, err = objx.FromJSON(ent.Privates)
		if err != nil {
			return nil, err
		}
	}
	return g.factory.Build(p.Id, p.EntityType, utils.ToLoc(p.Location), props, privs), nil
}

// New constructs a new game database from an Sqlite3 path.
func New(path string, factory sandengine.EntityFactory) (sandengine.GameDB, error) {
	var gdb gameDB

	// DB initialization
	db, err := gorm.Open("mysql", path)
	if err != nil {
		return nil, err
	}

	db.AutoMigrate(&models.Entity{})
	db.AutoMigrate(&models.Player{})

	gdb.db = &db
	gdb.factory = factory

	return &gdb, nil
}
