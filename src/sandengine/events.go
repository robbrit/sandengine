package sandengine

// Events is a channel that notes certain effects happening in a region.
type Events chan interface{}

// TODO: How to get this to typecheck?
