package main

import (
	"log"
	"time"

	"github.com/stretchr/objx"
	se "sandengine"
	"sandengine/builtins"
	"sandengine/utils"
)

const (
	boulderHealth = 7
	chickenCD     = 1 * time.Second
	chickenWander = 0.15
	treeHealth    = 5
)

var (
	// For now, loot is a simple number.
	boulderLoot = objx.Map{"stone": 5}
	treeLoot    = objx.Map{"wood": 5}
)

var entities = map[string]struct {
	solid  bool
	health int
	loot   objx.Map
}{
	"boulder":     {true, boulderHealth, boulderLoot},
	"bush":        {false, 0, nil},
	"chicken":     {true, 0, nil},
	"cotton_tree": {true, treeHealth, treeLoot},
	"dead_tree":   {true, treeHealth, treeLoot},
	"fern":        {false, 0, nil},
	"lilypad":     {false, 0, nil},
	"loot":        {false, 0, nil},
	"player":      {true, 0, nil},
	"pine_tree":   {true, treeHealth, treeLoot},
	"tree":        {true, treeHealth, treeLoot},
}

type EntityFactory struct{}

func (ef *EntityFactory) Create(entityType string, loc *utils.Location, props objx.Map, privs objx.Map) se.Entity {
	data, ok := entities[entityType]
	if !ok {
		log.Printf("Unknown entity type %s", entityType)
		return nil
	}

	privs.Set("solid", data.solid)
	if data.loot != nil {
		privs.Set("loot", data.loot)
	}

	if data.health > 0 {
		props.Set("health", data.health)
		props.Set("maxHealth", data.health)
	}
	return ef.Build(builtins.NewId(), entityType, loc, props, privs)
}

func (ef *EntityFactory) Build(id, entityType string, loc *utils.Location, props objx.Map, privs objx.Map) se.Entity {
	_, ok := entities[entityType]
	if !ok {
		log.Printf("Unknown entity type %s", entityType)
		return nil
	}
	return builtins.NewEntityBase(id, entityType, loc, props, privs)
}

func (ef *EntityFactory) NewPlayer(name string) se.Entity {
	props := objx.Map{}
	props.Set("name", name)
	props.Set("activeItem", "axe")
	privs := objx.Map{}
	privs.Set("items", objx.Map{})
	loc := &utils.Location{
		X:      10,
		Y:      10,
		Region: "1",
	}
	return ef.Create("player", loc, props, privs)
}

func bindAI(worker *builtins.AIWorker) {
	worker.RegisterBehaviour(
		"chicken",
		chickenCD,
		builtins.Merge(
			builtins.Talk("chicken", 0.02),
			builtins.Wander(chickenWander)))
}
