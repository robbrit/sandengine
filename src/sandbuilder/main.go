package main

import (
	"flag"
	"fmt"
	"log"
	"time"

	"sandengine"
	"sandengine/builtins"
	"sandengine/builtins/regions"
	pb "sandengine/proto"
	"sandengine/server"
	"sandengine/server/models"
)

var (
	debug         = flag.Bool("debug", true, "Whether to run in debug mode or not.")
	port          = flag.Int("port", 8080, "Which port to run the HTTP server on.")
	sessionSecret = flag.String("session_secret", "fixme", "Session secret.")
	httpsCert     = flag.String("https_cert", "", "Certificate file for HTTPS")
	httpsKey      = flag.String("https_key", "", "Key file for HTTPS")
)

// loadData loads up all our data from files.
func loadData(g sandengine.Game) error {
	_, err := g.TilesetStore().Load("data/tilesets/forest.tileset")
	if err != nil {
		return fmt.Errorf("tileset error: %s", err)
	}

	names := []string{"1", "2"}

	for _, name := range names {
		r, err := regions.NewTiled(g.TilesetStore(), g.EntityStore(), g.PhysicsManager(),
			g.RegionStore(), g.EntityFactory(), fmt.Sprintf("data/regions/%s.region", name))
		if err != nil {
			return fmt.Errorf("region error: %s", err)
		}

		g.RegionStore().Add(r)
	}

	ai := builtins.NewAIWorker(names, g.EntityFactory(), 100*time.Millisecond)
	bindAI(ai)
	g.WorkerManager().Add(ai)

	return nil
}

// loadTestData creates some users and players that can be used for testing.
func loadTestData(g sandengine.Game) error {
	// Load users.
	authDB, err := server.NewDB(server.DefaultAuthDB)
	if err != nil {
		return err
	}

	for _, playerName := range []string{"bob", "jill"} {
		user := models.NewUser(playerName, "asdfasdf", playerName+"@sandbuilder.com")
		if err := authDB.SaveUser(user); err != nil {
			return err
		}

		// Load player entities.
		ent := g.EntityFactory().NewPlayer(playerName)
		if err := g.EntityStore().CreateEntities(ent); err != nil {
			return err
		}

		log.Printf("Created entity for %s with ID %s: %s", playerName, ent.ID(), ent)

		// Load players.
		if err := g.PlayerStore().SavePlayer(&pb.Player{
			Name:     playerName,
			Username: playerName,
			EntityId: ent.ID(),
		}); err != nil {
			return err
		}
	}
	return nil
}

func main() {
	flag.Parse()

	log.SetFlags(log.Flags() | log.Lshortfile)

	// Construct the game.
	g, err := builtins.NewGame(sandengine.GameOptions{
		Debug:          *debug,
		EntityFactory:  &EntityFactory{},
		PhysicsManager: NewPhysics(),
		WorkerManager:  sandengine.NewWorkerManager(),
	})
	if err != nil {
		log.Fatalf("Unable to initialize game: %s", err)
	}

	switch flag.Arg(0) {
	case "test_data":
		log.Println("Loading test data...")
		if err := loadTestData(g); err != nil {
			log.Fatalf("Error loading test data: %s", err)
		}
		return
	case "load_static":
		log.Println("Loading static data...")
		if err := g.EntityStore().LoadStatic("data/regions/static"); err != nil {
			log.Fatalf("Error loading test data: %s", err)
		}
		return
	}

	if err = loadData(g); err != nil {
		log.Fatalf("Unable to load data: %s", err)
	}

	// Construct the server.
	if err = server.New(g).Start(server.ServerOptions{
		HTTPPort:      *port,
		Debug:         *debug,
		Title:         "Sandbuilder",
		SessionSecret: *sessionSecret,
		HTTPSCert:     *httpsCert,
		HTTPSKey:      *httpsKey,
	}); err != nil {
		log.Fatalf("Unable to initialize server: %s", err)
	}

	select {}
}
