package main

import (
	"fmt"
	"log"
	"time"

	"github.com/stretchr/objx"
	"sandengine"
	"sandengine/builtins"
	pb "sandengine/proto"
	"sandengine/utils"
)

const (
	// How much damage do things do by default?
	defaultDamage = 1
	// What's the default attack rate limit?
	defaultAttackRate = 500
)

// throttle throttles an action.
func throttle(rateParam, lastActionParam string, defaultRate int, onSuccess builtins.ActionHandler) builtins.ActionHandler {
	return func(e1, e2 sandengine.Entity) []sandengine.Effect {
		last := e1.Pr().Get(lastActionParam).Int(0)
		rate := e1.Pr().Get(rateParam).Int(defaultRate)
		now := time.Now().Unix()
		if int64(last+rate) > now {
			return nil
		}
		e1.Pr().Set(lastActionParam, now)
		return onSuccess(e1, e2)
	}
}

// soundPlayer gives back an ActionHandler that plays a sound at an entity's
// location.
func soundPlayer(sound string) builtins.ActionHandler {
	return func(e1, e2 sandengine.Entity) []sandengine.Effect {
		var loc pb.Location = pb.Location(*e1.Location())
		return []sandengine.Effect{
			&pb.PlaySound{
				Sound:    sound,
				Location: &loc,
			},
		}
	}
}

// Damage gives back an ActionHandler that damages an entity and calls some
// functions based on the result.
func damage(onHit, onDead builtins.ActionHandler) builtins.ActionHandler {
	return func(e1, e2 sandengine.Entity) []sandengine.Effect {
		// e1 is hitting e2.
		if !e2.P().Has("health") {
			return onHit(e1, e2)
		}

		// Subtract health.
		var health = e2.P().Get("health").Int(0)
		health -= e1.Pr().Get("damage").Int(defaultDamage)

		if health <= 0 {
			e2.P().Set("health", 0)
			return onDead(e1, e2)
		}
		e2.P().Set("health", health)
		return onHit(e1, e2)
	}
}

// destroy removes an entity from the game.
func destroy(e1, e2 sandengine.Entity) []sandengine.Effect {
	return []sandengine.Effect{&pb.RemoveEntity{e2.ID()}}
}

// createLoot creates some loot at specific spot.
func createLoot(e1, e2 sandengine.Entity) []sandengine.Effect {
	if !e2.Pr().Has("loot") {
		return nil
	}

	return []sandengine.Effect{
		&pb.CreateEntity{
			EntityType: "loot",
			Location:   e2.Location().ToPB(),
			Properties: "{}",
			Privates: objx.Map{
				"loot": e2.Pr().Get("loot").MSI(),
			}.MustJSON(),
		},
	}
}

// loadLoot loads some loot into a player's inventory.
func loadLoot(e1, e2 sandengine.Entity, loc *utils.Location) []sandengine.Effect {
	// Figure out which one is the loot.
	var loot, player sandengine.Entity
	if e1.EntityType() == "loot" {
		loot, player = e1, e2
	} else {
		player, loot = e1, e2
	}

	for key, rawAmount := range loot.Pr().Get("loot").MSI() {
		amount, ok := rawAmount.(float64)
		if !ok {
			log.Printf("non-float loot: loot[%s] = %v (%T)", key, rawAmount, rawAmount)
			continue
		}
		playerKey := fmt.Sprintf("items.%s", key)
		current := player.Pr().Get(playerKey).Float64()
		player.Pr().Set(playerKey, current+amount)
	}

	return []sandengine.Effect{
		&pb.RemoveEntity{
			EntityId: loot.ID(),
		},
		&pb.UpdatePrivate{
			EntityId: player.ID(),
			Privates: player.Pr().MustJSON(),
		},
	}
}

func NewPhysics() sandengine.PhysicsManager {
	pm := builtins.NewPhysicsManager()

	for _, tree := range []string{"tree", "dead_tree", "pine_tree", "cotton_tree"} {
		pm.RegisterAction("player", tree,
			throttle("attackRate", "lastAttack", defaultAttackRate,
				damage(
					soundPlayer("woodchop"),
					builtins.CombineActions(soundPlayer("treefall"), destroy, createLoot),
				)))
	}

	for _, rock := range []string{"boulder"} {
		pm.RegisterAction("player", rock,
			throttle("attackRate", "lastAttack", defaultAttackRate,
				damage(
					soundPlayer("metalchop"),
					builtins.CombineActions(soundPlayer("boulderfall"), destroy, createLoot),
				)))
	}

	pm.RegisterCollision("player", "loot", loadLoot)

	return pm
}
